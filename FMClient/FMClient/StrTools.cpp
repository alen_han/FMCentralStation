// StrTools.cpp

#include "stdafx.h"
#include "FM_STRUCTS.h"
#include "FMClient.h"
#include "StrTools.h"


CString GetFileName(CString& strFullPath)
{
	CString s;
	int nPos = 0;
	int nLeft = 0;
	while(1){
		nLeft = strFullPath.Find('\\', nPos);
		if( nLeft < 0 )
			break;
		nPos = nLeft+1;
	}
	s = strFullPath.Mid(nPos);
	return s;
}
CString GetFilePath(CString& strFullPath)
{
	CString s;
	int nPos = 0;
	int nLeft = 0;
	while(1){
		nLeft = strFullPath.Find('\\', nPos);
		if( nLeft < 0 )
			break;
		nPos = nLeft+1;
	}
	s = strFullPath.Left(nPos);
	return s;
}
CString TrimAll(const CString& rs)
{
	CString s(rs);
	s.TrimLeft();
	s.TrimRight();
	return s;
}
int Split(CStringArray& rAr, LPCTSTR pszSrc, LPCTSTR pszDelimiter)
{
	int n = (int)_tcslen(pszDelimiter);
	rAr.RemoveAll();
	if( n<=0 ){
		rAr.Add(TrimAll(CString(pszSrc)));
		return (int)rAr.GetSize();
	}
	CString s(pszSrc);
	while( s.GetLength() ){
		int iPos = s.Find(pszDelimiter);
		if( iPos<0 ){
			rAr.Add(TrimAll(s));
			break;
		}
		rAr.Add(TrimAll(s.Left(iPos)));
		s = s.Mid(iPos + n);
	}
	return (int)rAr.GetSize();
}
void Process(CMapStringToString& rDst, CStringArray& rAr)
{
	int i=0;
	int nFlds = (int)rAr.GetSize();
	for( i=0; i<nFlds; ++i ){
		CString s = rAr[i];
		int iPos = s.Find(_T("="));
		if( iPos<=0 ) continue;
		CString n = s.Left(iPos);
		CString v = s.Mid(iPos+1);
		rDst[n] = v;
	}
}
bool ParseRect(CRect& rRc, CString& rs)
{
	CStringArray sAr;
	int n = Split(sAr, rs, _T(","));
	if( n!=4 ) return false;
	rRc.left   = _ttoi(sAr[0]);
	rRc.top    = _ttoi(sAr[1]);
	rRc.right  = _ttoi(sAr[2]);
	rRc.bottom = _ttoi(sAr[3]);
	return true;
}
bool IncreaseStringNum(CString& rs)
{
	int n = _ttoi(rs);
	if (0 == n) return false;
	rs.Format(_T("%d"), n + 1);
	return true;
}
bool IncreaseStringNumWithout13and4(CString& rs)
{
	ULONGLONG n;
	ULONGLONG i = 10;
	ULONGLONG j = 1;
	try {
		_stscanf(rs.GetString(), _T("%I64u"), &n);
		n += 1;
		//修改13
		if (n % 100 == 13) {
			n += 2;
		}
		//跳过所有带4的数字
		while (j < n) {
			if (n % i / j == 4) {
				n += j;
			}
			i *= 10;
			j *= 10;
		}
		rs.Format(_T("%I64u"), n);
	}
	catch (CNotSupportedException e) {
		e.Delete();
		return false;
	}
	return true;
}
bool IsPositiveNum(CString& rs)
{
	if (_T("0") == rs) return true;
	if (0 < _ttoi(rs)) return true;
	return false;
}
CString AtoW(BYTE* asc_str, int len)
{
	WCHAR* wc = new WCHAR[len + 1];
	MultiByteToWideChar(CP_OEMCP, NULL, (char*)asc_str, len, wc, len + 1);
	CString ret_str(wc);
	delete[] wc;
	return ret_str;
}
int GetAge(CTime& tmBirthday)
{
	CTime tmNow = CTime::GetCurrentTime();
	int nAge = tmNow.GetYear() - tmBirthday.GetYear();
	if (tmNow.GetMonth() < tmBirthday.GetMonth()) {
		return (nAge - 1);
	}
	if (tmNow.GetMonth() == tmBirthday.GetMonth()) {
		if (tmNow.GetDay() < tmBirthday.GetDay()) {
			return (nAge - 1);
		}
	}
	return nAge;
}
CTime StringToTime(CString s)
{
	int nYear, nMonth, nDate, nHour, nMin, nSec;
	_stscanf(s, _T("%d-%d-%d %d:%d:%d"), &nYear, &nMonth, &nDate, &nHour, &nMin, &nSec);
	return CTime(nYear, nMonth, nDate, nHour, nMin, nSec);
}
//将n转化成绘制数字的专用字符串
CString itoNumString2(int n)
{
	CString s;
	if (INVALID_VALUE == n) {
		s = _T("--");
		return s;
	}
	s.Format(_T("%d"), n);
	while (2 > s.GetLength()) {
		s = _T(" ") + s;
	}
	return s;
}
CString itoNumString3(int n)
{
	CString s;
	if (INVALID_VALUE == n) {
		s = _T("---");
		return s;
	}
	s.Format(_T("%d"), n);
	while (3 > s.GetLength()) {
		s = _T(" ") + s;
	}
	return s;
}
//将n转化为绘制血压的专用字符串
CString itoNibpString(int unit, int n)
{
	CString s;
	if (INVALID_VALUE == n) {
		s = _T("--- ");
		return s;
	}
	if (1 == unit) {
		//转化为千帕
		float kpa = 0.1333f * n;
		s.Format(_T("%.1f"), kpa);
		while (s.GetLength() < 4) {
			s = _T(" ") + s;
		}
		return s;
	}
	else {
		s.Format(_T("%d"), n);
		while (s.GetLength() < 3) {
			s = _T(" ") + s;
		}
		s += _T(" ");
		return s;
	}
}
//将n转化成绘制温度的专用字符串
CString itoTempString(int unit, int n)
{
	CString s;
	if (INVALID_VALUE == n) {
		s = _T("---.-");
		return s;
	}
	if (1 == unit) {
		//转换为华氏度
		n = n * 9 / 5 + 320;
	}

	float f = (float)n / 10.0f;
	s.Format(_T("%.1f"), f);
	while (s.GetLength() < 5) {
		s = _T(" ") + s;
	}
	return s;
}
//将n转化成绘制图标的专用字符串
CString itoIconString(int n)
{
	CString s;
	s.Format(_T("%d"), (n % 10));
	return s;
}

CIniInt::CIniInt(CString sFileContent)
{
	int i;
	CStringArray ar;
	CStringArray pair;
	int nLineCount = Split(ar, sFileContent, _T("\r\n"));
	for (i=0; i<nLineCount; i++) {
		int n = Split(pair, ar[i], _T("="));
		if (2 != n) {
			//CString sErr;
			//sErr.Format(_T("CIniInt错误发生在第%d行！\n%s\n"), i+1, ar[i]);
			//theApp.WriteLog(sErr);
			continue;
		}
		CString sKey = TrimAll(pair[0]);
		int nValue = _ttoi(TrimAll(pair[1]));
		m_mapDict[sKey] = nValue;
	}
}

CIniInt::~CIniInt()
{
	m_mapDict.RemoveAll();
}

int CIniInt::Get(LPCTSTR sKey)
{
	int n;
	try {
		n = m_mapDict[sKey];
	}
	catch (CException* pe) {
		//已知问题：这个异常报不出来
		pe->Delete();
		return -1;
	}
	return n;
}

CIniString::CIniString(CString sFileContent)
{
	int i;
	CStringArray ar;
	CStringArray pair;
	int nLineCount = Split(ar, sFileContent, _T("\r\n"));
	for (i=0; i<nLineCount; i++) {
		int n = Split(pair, ar[i], _T("="));
		if (2 != n) {
			//CString sErr;
			//sErr.Format(_T("CIniString错误发生在第%d行！\n%s\n"), i+1, ar[i]);
			//theApp.WriteLog(sErr);
			continue;
		}
		CString sKey = TrimAll(pair[0]);
		CString sValue = TrimAll(pair[1]);
		m_mapDict[sKey] = sValue;
	}
}

CIniString::~CIniString()
{
	m_mapDict.RemoveAll();
}

CString CIniString::Get(LPCTSTR sKey)
{
	CString s = _T("");
	try {
		s = m_mapDict[sKey];
	}
	catch (CException* pe) {
		//已知问题：这个异常报不出来
		pe->Delete();
		return s;
	}
	return s;
}
