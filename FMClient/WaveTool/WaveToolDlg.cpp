
// WaveToolDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "WaveTool.h"
#include "WaveToolDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CWaveToolDlg 对话框



CWaveToolDlg::CWaveToolDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CWaveToolDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CWaveToolDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CWaveToolDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDCANCEL, &CWaveToolDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDOK, &CWaveToolDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CWaveToolDlg 消息处理程序

BOOL CWaveToolDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CWaveToolDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CWaveToolDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CWaveToolDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CWaveToolDlg::OnBnClickedCancel()
{
	// TODO: 在此添加控件通知处理程序代码
	CDialogEx::OnCancel();
}

static TCHAR l_szBmpFileFilter[] = _T("BMP Files (*.bmp)|*.bmp||");
static TCHAR l_szTxtFileFilter[] = _T("TXT Files (*.txt)|*.txt||");
void CWaveToolDlg::OnBnClickedOk()
{
	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, l_szBmpFileFilter, this);
	if (IDOK != dlg.DoModal()) {
		return;
	}
	
	CString sFile = dlg.GetPathName();
	HBITMAP hBitmap = (HBITMAP)LoadImage(NULL, sFile, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION|LR_DEFAULTSIZE|LR_LOADFROMFILE);

	CBitmap bmp;
	bmp.Attach(hBitmap);

	CClientDC dc(this);
	CDC memDC;
	memDC.CreateCompatibleDC(&dc);
	memDC.SelectObject(&bmp);

	BITMAP bm;
	bmp.GetBitmap(&bm);
	if (0 == bm.bmWidth || 0 == bm.bmHeight) {
		MessageBox(_T("输入文件 打开出错！"));
		return;
	}

	// 绘制，看看
	dc.BitBlt(0, 0, bm.bmWidth, min(256, bm.bmHeight), &memDC, 0, 0, SRCCOPY);

	CFile file;
	CString sOutputFileName = _T("array.txt");
	CFileDialog dlg2(FALSE, NULL, sOutputFileName, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, l_szTxtFileFilter, this);
	if (IDOK != dlg2.DoModal()) {
		return;
	}

	CString sOutputFile = dlg2.GetPathName();
	if (! file.Open(sOutputFile, CFile::modeCreate | CFile::modeReadWrite)) {
		MessageBox(_T("写入文件 出错！"));
		return;
	}
	char head[] = "BYTE buffer = {\r\n\t";
	file.Write(head, strlen(head));

	int size = bm.bmWidth;
	int i, y;
	for (i=0; i<size; i++) {
		for (y=0; y<256; y++) {
			COLORREF cr = memDC.GetPixel(i, y);
			if (cr != RGB(255,255,255)) {
				// 如果不是白色，那么就算碰到了曲线
				break;
			}
		}
		BYTE value = 0xFF - (BYTE)y;

		char txtValue[255];
		if (i != size-1) {
			if (i%16 == 15) {
				sprintf(txtValue, "0x%02X,\r\n\t", value);
			}
			else {
				sprintf(txtValue, "0x%02X, ", value);
			}
		}
		else {
			sprintf(txtValue, "0x%02X };", value);
		}
		file.Write(txtValue, strlen(txtValue));
	}
	file.Close();
	MessageBox(_T("写入成功！"));

	return;

//	CDialogEx::OnOK();
}
