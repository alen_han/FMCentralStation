#include "stdafx.h"
#include "ReviewTendTable.h"
#include "ReviewDlg.h"

static const int top_margin = 6;
static const int bot_margin = 6;
static const int lft_margin = 90;
static const int rht_margin = 91;
static const int grid_gap_x = 140;
static const int grid_gap_y = 18;

static const COLORREF cr_bkgd = RGB(0,0,0);
static const COLORREF cr_grid = RGB(0,0,0);
static const COLORREF cr_title_bar = RGB(192,128,32);
static const COLORREF cr_title_text = RGB(255,255,255);
static const COLORREF cr_normal = RGB(225,225,255);
static const COLORREF cr_alarm = RGB(255,166,92);
static const COLORREF cr_time = RGB(0,0,0);
static const COLORREF cr_hr   = RGB(0,128,0);
static const COLORREF cr_sys  = RGB(0,0,192);
static const COLORREF cr_mea  = RGB(0,64,128);
static const COLORREF cr_dia  = RGB(128,0,64);
static const COLORREF cr_rr   = RGB(92,92,0);
static const COLORREF cr_spo2 = RGB(128,0,128);
static const COLORREF cr_t1   = RGB(0,128,192);
static const COLORREF cr_t2   = RGB(192,0,0);


CReviewTendTable::CReviewTendTable(CReviewDlg* pdlg)
	: CReviewTool(pdlg)
{
	// 创建默认字体
	m_fontNum.CreateFont(12, 0, 0, 0, FW_SEMIBOLD, FALSE, FALSE, 0, ANSI_CHARSET,
		OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_SWISS, _T("宋体"));
	m_pOldFont = NULL;
}

CReviewTendTable::~CReviewTendTable(void)
{
	m_pOldFont = NULL;
}

int CReviewTendTable::GetPageSpan()
{
	CRect rc = m_pReviewDlg->GetTableRect();
	int lines = (rc.Height() - top_margin - bot_margin) / grid_gap_y - 1;//减去title占用的一行
	return lines;
}

void CReviewTendTable::EnterReview()
{
	m_pReviewDlg->m_staSelectNibpUnit.ShowWindow(SW_SHOW);
	m_pReviewDlg->m_cmbSelectNibpUnit.ShowWindow(SW_SHOW);
	m_pReviewDlg->m_staSelectTempUnit.ShowWindow(SW_SHOW);
	m_pReviewDlg->m_cmbSelectTempUnit.ShowWindow(SW_SHOW);
}

void CReviewTendTable::LeaveReview()
{
	m_pReviewDlg->m_staSelectNibpUnit.ShowWindow(SW_HIDE);
	m_pReviewDlg->m_cmbSelectNibpUnit.ShowWindow(SW_HIDE);
	m_pReviewDlg->m_staSelectTempUnit.ShowWindow(SW_HIDE);
	m_pReviewDlg->m_cmbSelectTempUnit.ShowWindow(SW_HIDE);
}

void CReviewTendTable::Draw(CDC* pdc)
{
	m_pReviewDlg->InitBkgd(pdc, cr_bkgd);

	CRect rc = m_pReviewDlg->GetTableRect();
	rc.OffsetRect(-rc.left, -rc.top);

	m_pOldFont = pdc->SelectObject(&m_fontNum);
	pdc->SetBkMode(TRANSPARENT);
	DrawGrid(pdc, rc);
	DrawColumnName(pdc, rc);
	DrawData(pdc, rc);
	pdc->SelectObject(m_pOldFont);
}

void CReviewTendTable::DrawGrid(CDC* pdc, CRect& rc)
{
	const int grid_gap = 2;

	CPen gridPen(PS_SOLID, 1, cr_grid);
	CPen* pOldPen = pdc->SelectObject(&gridPen);

	CBrush brTitleBar(cr_title_bar);
	CRect rcTitle(rc.left + lft_margin, rc.top + top_margin,
		rc.left + lft_margin + grid_gap_x * 9, rc.top + top_margin + grid_gap_y);
	pdc->FillRect(&rcTitle, &brTitleBar);
	CBrush brTableArea(cr_normal);
	CRect rcBody(rc.left + lft_margin, rc.top + top_margin + grid_gap_y,
		rc.right - rht_margin, rc.bottom - bot_margin);
	pdc->FillRect(&rcBody, &brTableArea);

	int x, y;
	for (x=rc.left + lft_margin; x<rc.right; x+= grid_gap_x) {
		pdc->MoveTo(x, rc.top + top_margin);
		pdc->LineTo(x, rc.bottom - bot_margin);
	}
	for (y=rc.top; y<rc.bottom; y+= grid_gap_y) {
		pdc->MoveTo(rc.left + lft_margin, y + top_margin);
		pdc->LineTo(rc.right - rht_margin, y + top_margin);
	}

	pdc->SelectObject(pOldPen);
}

void CReviewTendTable::DrawColumnName(CDC* pdc, CRect& rc)
{
	int x = rc.left + lft_margin + 8;
	int y = rc.top + top_margin + 3;
	CString sNibpUnit = CFMDict::NIBPUnit_itos(m_pReviewDlg->GetNIBPUnitOpt());
	CString sTempUnit = CFMDict::TEMPUnit_itos(m_pReviewDlg->GetTEMPUnitOpt());
	pdc->SetTextColor(cr_title_text);
	CString sName;

	pdc->TextOut(x, y, _T("监护时间"));

	x += grid_gap_x;
	pdc->TextOut(x, y, _T("心率"));

	x += grid_gap_x;
	sName.Format(_T("收缩压(%s)"), sNibpUnit);
	pdc->TextOut(x, y, sName);

	x += grid_gap_x;
	sName.Format(_T("平均压(%s)"), sNibpUnit);
	pdc->TextOut(x, y, sName);

	x += grid_gap_x;
	sName.Format(_T("舒张压(%s)"), sNibpUnit);
	pdc->TextOut(x, y, sName);

	x += grid_gap_x;
	pdc->TextOut(x, y, _T("呼吸率"));

	x += grid_gap_x;
	pdc->TextOut(x, y, _T("血氧饱和度"));

	x += grid_gap_x;
	sName.Format(_T("体温1(%s)"), sTempUnit);
	pdc->TextOut(x, y, sName);

	x += grid_gap_x;
	sName.Format(_T("体温2(%s)"), sTempUnit);
	pdc->TextOut(x, y, sName);
}

void CReviewTendTable::DrawData(CDC* pdc, CRect& rc)
{
	int nValidLen;
	CTime tmStart = m_pReviewDlg->GetCurrentDataTime(&nValidLen);
	CLIENT_DATA* pData = m_pReviewDlg->GetCurrentBufferPointer(&nValidLen);
	if (! pData) {
		return;
	}

	int x = rc.left + lft_margin;
	int y = rc.top + top_margin + grid_gap_y;
	const int text_x_offset = 18;
	const int text_y_offset = 3;
	int nibpUnit = m_pReviewDlg->GetNIBPUnitOpt();
	int tempUnit = m_pReviewDlg->GetTEMPUnitOpt();
	CString sValue;

	int i;
	int lines = (rc.Height() - top_margin - bot_margin) / grid_gap_y - 1;//减去title占用的一行
	int count = min(lines, nValidLen);
	for (i=0; i<count; i++) {
		int curX = x;
		CTimeSpan ts(0, 0, 0, i);
		CTime tmCur = tmStart + ts;
		sValue = tmCur.Format(_T("%H:%M:%S"));
		pdc->SetTextColor(cr_time);
		pdc->TextOut(curX + text_x_offset, y + text_y_offset, sValue);
		curX += grid_gap_x;

		ECG* pe;
		RESP* pr;
		SPO2* ps;
		NIBP* pn;
		TEMP* pt;
		FETUS* pf;
		CStructTools::ParseClientData(&(pData[i]), &pe, &pr, &ps, &pn, &pt, &pf);

		if (pe->hr < pe->hr_low || pe->hr > pe->hr_high) {
			DrawAlarmGrid(pdc, curX, y);
		}
		sValue = itoNumString3(CHECK_HR(pe->hr));
		pdc->SetTextColor(cr_hr);
		pdc->TextOut(curX + text_x_offset, y + text_y_offset, sValue);
		curX += grid_gap_x;

		if (pn->sys < pn->sys_low || pn->sys > pn->sys_high) {
			DrawAlarmGrid(pdc, curX, y);
		}
		sValue = itoNibpString(nibpUnit, CHECK_NIBP(pn->sys));
		pdc->SetTextColor(cr_sys);
		pdc->TextOut(curX + text_x_offset, y + text_y_offset, sValue);
		curX += grid_gap_x;

		if (pn->mea < pn->mea_low || pn->mea > pn->mea_high) {
			DrawAlarmGrid(pdc, curX, y);
		}
		sValue = itoNibpString(nibpUnit, CHECK_NIBP(pn->mea));
		pdc->SetTextColor(cr_mea);
		pdc->TextOut(curX + text_x_offset, y + text_y_offset, sValue);
		curX += grid_gap_x;

		if (pn->dia < pn->dia_low || pn->dia > pn->dia_high) {
			DrawAlarmGrid(pdc, curX, y);
		}
		sValue = itoNibpString(nibpUnit, CHECK_NIBP(pn->dia));
		pdc->SetTextColor(cr_dia);
		pdc->TextOut(curX + text_x_offset, y + text_y_offset, sValue);
		curX += grid_gap_x;

		if (pr->rr < pr->rr_low || pr->rr > pr->rr_high) {
			DrawAlarmGrid(pdc, curX, y);
		}
		sValue = itoNumString3(CHECK_RR(pr->rr));
		pdc->SetTextColor(cr_rr);
		pdc->TextOut(curX + text_x_offset, y + text_y_offset, sValue);
		curX += grid_gap_x;

		if (ps->spo2 < ps->spo2_low || ps->spo2 > ps->spo2_high) {
			DrawAlarmGrid(pdc, curX, y);
		}
		sValue = itoNumString3(CHECK_SPO2(ps->spo2));
		pdc->SetTextColor(cr_spo2);
		pdc->TextOut(curX + text_x_offset, y + text_y_offset, sValue);
		curX += grid_gap_x;

		if (pt->t1 < pt->t1_low || pt->t1 > pt->t1_high) {
			DrawAlarmGrid(pdc, curX, y);
		}
		sValue = itoTempString(tempUnit, CHECK_TEMP(pt->t1));
		pdc->SetTextColor(cr_t1);
		pdc->TextOut(curX + text_x_offset, y + text_y_offset, sValue);
		curX += grid_gap_x;

		if (pt->t2 < pt->t2_low || pt->t2 > pt->t2_high) {
			DrawAlarmGrid(pdc, curX, y);
		}
		sValue = itoTempString(tempUnit, CHECK_TEMP(pt->t2));
		pdc->SetTextColor(cr_t2);
		pdc->TextOut(curX + text_x_offset, y + text_y_offset, sValue);
		curX += grid_gap_x;

		y += grid_gap_y;
	}
}

void CReviewTendTable::DrawAlarmGrid(CDC* pdc, int x, int y)
{
	CBrush br(cr_alarm);
	CRect rc(x + 1, y + 1, x + grid_gap_x - 1, y + grid_gap_y - 1);
	pdc->FillRect(&rc, &br);
}
