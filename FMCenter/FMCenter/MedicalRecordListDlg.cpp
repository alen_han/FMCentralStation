// MedicalRecordListDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "FMCenter.h"
#include "MedicalRecordListDlg.h"
#include "MedicalRecordEditDlg.h"
#include "afxdialogex.h"


#define MEDICAL_LIST_LINAGE                      (40) //定义每页显示的行数
#define MEDICAL_LIST_COLUMN_COUNT                (7)
#define MEDICAL_LIST_INDEX_OF_MEDICAL_NUM        (0)
#define MEDICAL_LIST_INDEX_OF_PATIENT_NAME       (1)
#define MEDICAL_LIST_INDEX_OF_ADMISSION_DATE     (6)


//column上面显示的文字
static _TCHAR* l_arColumnLabel[MEDICAL_LIST_COLUMN_COUNT] =
{
	_T("病历号"),
	_T("姓名"),
	_T("出生日期"),
	_T("性别"),
	_T("科室"),
	_T("主治医师"),
	_T("住院时间")
};
//column上文字的显示方式（靠左）
static int l_arColumnFmt[MEDICAL_LIST_COLUMN_COUNT] = 
{
	LVCFMT_LEFT,
	LVCFMT_LEFT,
	LVCFMT_LEFT,
	LVCFMT_LEFT,
	LVCFMT_LEFT,
	LVCFMT_LEFT,
	LVCFMT_LEFT
};
//column的宽度
static int l_arColumnWidth[MEDICAL_LIST_COLUMN_COUNT] = 
{
	70,
	90,
	90,
	40,
	90,
	90,
	120
};


// CMedicalRecordListDlg 对话框

IMPLEMENT_DYNAMIC(CMedicalRecordListDlg, CDialogEx)

CMedicalRecordListDlg::CMedicalRecordListDlg(CWnd* pParent, int nType)
	: CDialogEx(CMedicalRecordListDlg::IDD, pParent)
	, m_db(theApp.m_db)
	, m_dbLock(theApp.m_csDBLock)
	, m_nType(nType)
{
	m_nRecordCount = 0;
	m_nPageStartIndex = 0;
	m_nListValidCount = 0;
	m_nListCurrentIndex = -1;
}

CMedicalRecordListDlg::~CMedicalRecordListDlg()
{
}

void CMedicalRecordListDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LST_RECORD, m_lstRecord);
	DDX_Control(pDX, IDC_BTN_FIRST, m_btnFirst);
	DDX_Control(pDX, IDC_BTN_PREV, m_btnPrev);
	DDX_Control(pDX, IDC_BTN_NEXT, m_btnNext);
	DDX_Control(pDX, IDC_BTN_LAST, m_btnLast);
	DDX_Control(pDX, IDC_BTN_EDIT, m_btnEdit);
	DDX_Control(pDX, IDC_BTN_DELETE, m_btnDelete);
	DDX_Control(pDX, IDC_EDT_MEDICAL_NUM, m_edtMedicalNum);
	DDX_Control(pDX, IDC_EDT_PATIENT_NAME, m_edtPatientName);
	DDX_Control(pDX, IDC_EDT_PATIENT_TYPE, m_edtPatientType);
	DDX_Control(pDX, IDC_EDT_GENDER, m_edtGender);
	DDX_Control(pDX, IDC_EDT_BLOODTYPE, m_edtBloodtype);
	DDX_Control(pDX, IDC_EDT_HEIGHT, m_edtHeight);
	DDX_Control(pDX, IDC_EDT_WEIGHT, m_edtWeight);
	DDX_Control(pDX, IDC_EDT_BIRTHDAY, m_edtBirthday);
	DDX_Control(pDX, IDC_EDT_ADDRESS, m_edtAddress);
	DDX_Control(pDX, IDC_EDIT_CREATE_DATE, m_edtCreateDate);
	DDX_Control(pDX, IDC_EDT_CLINIC, m_edtClinic);
	DDX_Control(pDX, IDC_EDT_DOCTOR, m_edtDoctor);
	DDX_Control(pDX, IDC_EDT_PHONE_NUMBER, m_edtPhoneNumber);
	DDX_Control(pDX, IDC_EDT_PAGE_INFO, m_edtPageInfo);
	DDX_Control(pDX, IDC_EDIT_ADMIN_NAME, m_edtAdminName);
	DDX_Control(pDX, IDC_EDT_ADMISSION_DATE, m_edtAdmissionDate);
	DDX_Control(pDX, IDC_BTN_SELECT, m_btnSelect);
	DDX_Control(pDX, IDC_EDT_SEARCH_MEDICAL_NUM, m_edtSearchMedicalNum);
	DDX_Control(pDX, IDC_EDT_SEARCH_PATIENT_NAME, m_edtSearchPatientName);
	DDX_Control(pDX, IDC_CMB_SEARCH_GENDER, m_cmbSearchGender);
	DDX_Control(pDX, IDC_EDT_SEARCH_ADDRESS, m_edtSearchAddress);
	DDX_Control(pDX, IDC_EDT_SEARCH_PHONE_NUMBER, m_edtSearchPhoneNumber);
	DDX_Control(pDX, IDC_EDT_SEARCH_CLINIC, m_edtSearchClinic);
	DDX_Control(pDX, IDC_EDT_SEARCH_DOCTOR, m_edtSearchDoctor);
	DDX_Control(pDX, IDC_DATE_SEARCH_ADMISSION, m_datSearchAdmission);
	DDX_Control(pDX, IDC_BTN_NEW, m_btnNew);
	DDX_Control(pDX, IDC_CMB_SEARCH_PATIENT_TYPE, m_cmbSearchPatientType);
}


BEGIN_MESSAGE_MAP(CMedicalRecordListDlg, CDialogEx)
	ON_BN_CLICKED(IDC_BTN_FIRST, &CMedicalRecordListDlg::OnBnClickedBtnFirst)
	ON_BN_CLICKED(IDC_BTN_PREV, &CMedicalRecordListDlg::OnBnClickedBtnPrev)
	ON_BN_CLICKED(IDC_BTN_NEXT, &CMedicalRecordListDlg::OnBnClickedBtnNext)
	ON_BN_CLICKED(IDC_BTN_LAST, &CMedicalRecordListDlg::OnBnClickedBtnLast)
	ON_BN_CLICKED(IDC_BTN_EDIT, &CMedicalRecordListDlg::OnBnClickedBtnEdit)
	ON_BN_CLICKED(IDC_BTN_NEW, &CMedicalRecordListDlg::OnBnClickedBtnNew)
	ON_BN_CLICKED(IDC_BTN_DELETE, &CMedicalRecordListDlg::OnBnClickedBtnDelete)
	ON_NOTIFY(NM_CLICK, IDC_LST_RECORD, &CMedicalRecordListDlg::OnNMClickLstRecord)
	ON_NOTIFY(NM_DBLCLK, IDC_LST_RECORD, &CMedicalRecordListDlg::OnNMDbClickLstRecord)
	ON_BN_CLICKED(IDC_BTN_SELECT, &CMedicalRecordListDlg::OnBnClickedBtnSelect)
	ON_BN_CLICKED(IDC_BTN_SEARCH_BY_MEDICL_NUM, &CMedicalRecordListDlg::OnBnClickedBtnSearchByMediclNum)
	ON_BN_CLICKED(IDC_BTN_SEARCH_BY_NAME, &CMedicalRecordListDlg::OnBnClickedBtnSearchByName)
	ON_BN_CLICKED(IDC_BTN_SEARCH_BY_GENDER, &CMedicalRecordListDlg::OnBnClickedBtnSearchByGender)
	ON_BN_CLICKED(IDC_BTN_SEARCH_BY_ADDRESS, &CMedicalRecordListDlg::OnBnClickedBtnSearchByAddress)
	ON_BN_CLICKED(IDC_BTN_SEARCH_BY_PHONE_NUMBER, &CMedicalRecordListDlg::OnBnClickedBtnSearchByPhoneNumber)
	ON_BN_CLICKED(IDC_BTN_SEARCH_BY_CLINIC, &CMedicalRecordListDlg::OnBnClickedBtnSearchByClinic)
	ON_BN_CLICKED(IDC_BTN_SEARCH_BY_DOCTOR, &CMedicalRecordListDlg::OnBnClickedBtnSearchByDoctor)
	ON_BN_CLICKED(IDC_BTN_SEARCH_BY_ADMISSION, &CMedicalRecordListDlg::OnBnClickedBtnSearchByAdmission)
	ON_BN_CLICKED(IDC_BTN_SEARCH_BY_ALL, &CMedicalRecordListDlg::OnBnClickedBtnSearchAll)
	ON_BN_CLICKED(IDC_BTN_SEARCH_BY_TYPE, &CMedicalRecordListDlg::OnBnClickedBtnSearchByType)
	ON_BN_CLICKED(IDCANCEL, &CMedicalRecordListDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


// CMedicalRecordListDlg 消息处理程序


void CMedicalRecordListDlg::OnBnClickedBtnFirst()
{
	m_nPageStartIndex = 0;
	UpdateRecordList();
	SetNaviButtonState();
}


void CMedicalRecordListDlg::OnBnClickedBtnPrev()
{
	if (m_nPageStartIndex > 0) {
		m_nPageStartIndex -= MEDICAL_LIST_LINAGE;
	}
	UpdateRecordList();
	SetNaviButtonState();
}


void CMedicalRecordListDlg::OnBnClickedBtnNext()
{
	if ((m_nPageStartIndex + MEDICAL_LIST_LINAGE) < m_nRecordCount) {
		m_nPageStartIndex += MEDICAL_LIST_LINAGE;
	}
	UpdateRecordList();
	SetNaviButtonState();
}


void CMedicalRecordListDlg::OnBnClickedBtnLast()
{
	m_nPageStartIndex = ((m_nRecordCount - 1) / MEDICAL_LIST_LINAGE) * MEDICAL_LIST_LINAGE;
	UpdateRecordList();
	SetNaviButtonState();
}

// 编辑病历
void CMedicalRecordListDlg::OnBnClickedBtnEdit()
{
	CString sMedNo = m_lstRecord.GetItemText(m_nListCurrentIndex, MEDICAL_LIST_INDEX_OF_MEDICAL_NUM);
	CMedicalRecordEditDlg dlg(this, TYPE_OP_EDIT, sMedNo);
	dlg.DoModal();
}

// 新建病历
void CMedicalRecordListDlg::OnBnClickedBtnNew()
{
	CMedicalRecordEditDlg dlg(this, TYPE_OP_NEW, _T(""));
	dlg.DoModal();
}

// 删除病历
void CMedicalRecordListDlg::OnBnClickedBtnDelete()
{
	if (-1 == m_nListCurrentIndex) {
		return;
	}

	CString sMedNo = m_lstRecord.GetItemText(m_nListCurrentIndex, MEDICAL_LIST_INDEX_OF_MEDICAL_NUM);
	CString sTime = m_lstRecord.GetItemText(m_nListCurrentIndex, MEDICAL_LIST_INDEX_OF_ADMISSION_DATE);
	CString sName = m_lstRecord.GetItemText(m_nListCurrentIndex, MEDICAL_LIST_INDEX_OF_PATIENT_NAME);
	CString sMsgInfo;
	sMsgInfo.Format(_T("准备删除的病历：\n\n病历号：%s\n住院时间：%s\n姓名：%s\n\n您确认要删除该病历吗？"), 
		sMedNo, sTime, sName);
	if (IDOK != MessageBox(sMsgInfo, _T("确认删除操作"), MB_ICONEXCLAMATION | MB_OKCANCEL)) {
		return;
	}

	//执行删除操作
	CString sql;
	sql.Format(
		_T("DELETE FROM medical_record WHERE medical_num = '%s'"), sMedNo);

	//执行SQL语句
	CSmartLock lock(m_dbLock);
	try {
		m_db.ExecuteSQL(sql);
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CMedicalRecordListDlg::OnBnClickedBtnDelete第1个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		MessageBox(_T("数据库操作失败！"), _T("提示"), MB_ICONERROR | MB_OK);
		return;
	}

	//记录条计数减一
	DecreaseRecordCounter();
	UpdateRecordList(TRUE);
	SetNaviButtonState();
}

BOOL CMedicalRecordListDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 初始化LIST的表头
	int i;
	m_lstRecord.RemoveAllGroups();
	m_lstRecord.ModifyStyle(0, LVS_REPORT | LVS_SHOWSELALWAYS, 0);
	m_lstRecord.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	//设定一个用于存取column的结构lvc
	LVCOLUMN lvc;
	//设定存取模式
	lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
	//用InSertColumn函数向窗口中插入柱
	for (i=0 ; i<MEDICAL_LIST_COLUMN_COUNT; i++) {
		lvc.iSubItem = i;
		lvc.pszText = l_arColumnLabel[i];
		lvc.cx = l_arColumnWidth[i];
		lvc.fmt = l_arColumnFmt[i];
		m_lstRecord.InsertColumn(i,&lvc);
	}

	// 设置选项
	m_cmbSearchGender.ResetContent();
	for (i=0; i<CFMDict::GetGenderCount(); i++) {
		m_cmbSearchGender.AddString(CFMDict::Gender_itos(i));
	}
	m_cmbSearchPatientType.ResetContent();
	for (i=0; i<CFMDict::GetPatientTypeCount(); i++) {
		m_cmbSearchPatientType.AddString(CFMDict::PatientType_itos(i));
	}
	CTime tmNow = CTime::GetCurrentTime();
	m_datSearchAdmission.SetTime(&tmNow);
	m_datSearchAdmission.SetFormat(_T("yyyy-MM-dd"));

	// 设置输入框长度限制
	m_edtSearchAddress.SetLimitText(64);
	m_edtSearchClinic.SetLimitText(24);
	m_edtSearchDoctor.SetLimitText(24);
	m_edtSearchMedicalNum.SetLimitText(24);
	m_edtSearchPatientName.SetLimitText(24);
	m_edtSearchPhoneNumber.SetLimitText(24);

	switch (m_nType) {

	case TYPE_OP_NORMAL:
		m_btnSelect.ShowWindow(SW_HIDE);
		break;

	case TYPE_OP_SELECT:
		m_btnEdit.ShowWindow(SW_HIDE);
		m_btnDelete.ShowWindow(SW_HIDE);
		m_btnNew.ShowWindow(SW_HIDE);
		break;
	}

	m_nPageStartIndex = 0;
	m_nListValidCount = 0;
	UpdateRecordList();
	SetNaviButtonState();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void CMedicalRecordListDlg::SetNaviButtonState()
{
	if (-1 == m_nListCurrentIndex) {
		m_btnEdit.EnableWindow(FALSE);
		m_btnDelete.EnableWindow(FALSE);
	}
	else {
		m_btnEdit.EnableWindow(TRUE);
		m_btnDelete.EnableWindow(TRUE);
	}

	if (m_nRecordCount < MEDICAL_LIST_LINAGE) {
		//内容不够一页
		m_btnPrev.EnableWindow(FALSE);
		m_btnFirst.EnableWindow(FALSE);
		m_btnNext.EnableWindow(FALSE);
		m_btnLast.EnableWindow(FALSE);
	}

	if (m_nPageStartIndex > 0) {
		m_btnPrev.EnableWindow(TRUE);
		m_btnFirst.EnableWindow(TRUE);
	}
	else {
		m_btnPrev.EnableWindow(FALSE);
		m_btnFirst.EnableWindow(FALSE);
	}

	if ((m_nPageStartIndex + MEDICAL_LIST_LINAGE) < m_nRecordCount) {
		m_btnNext.EnableWindow(TRUE);
		m_btnLast.EnableWindow(TRUE);
	}
	else {
		m_btnNext.EnableWindow(FALSE);
		m_btnLast.EnableWindow(FALSE);
	}
}

void CMedicalRecordListDlg::InitRecordCounter(CString& sWhereStatement)
{
	m_nRecordCount = 0;
	CRecordset rs(&m_db);
	CString sql;
	if (sWhereStatement.IsEmpty()) {
		sql.Format(
			_T("SELECT COUNT(*) AS count FROM medical_record"));
	}
	else {
		sql.Format(
			_T("SELECT COUNT(*) AS count FROM medical_record WHERE %s"),
			sWhereStatement);
	}
	OutputDebugString(sql + _T("\n"));

	{
		// 执行SQL语句
		CSmartLock lock(m_dbLock);
		try {
			rs.Open(AFX_DB_USE_DEFAULT_TYPE, sql);
		}
		catch (CDBException* pe) {
			_TCHAR err[255];
			pe->GetErrorMessage(err, 255);
			theApp.WriteLog(_T("异常位于：CMedicalRecordListDlg::InitRecordCounter第1个catch\n"));
			theApp.WriteLog(err);
			theApp.WriteLog(_T("\n"));
			pe->Delete();
			return;
		}
	}

	// 读出结果
	try {
		rs.MoveFirst();
		if (!rs.IsEOF()) {
			CDBVariant var;
			rs.GetFieldValue(_T("count"), var);
			m_nRecordCount = var.m_lVal;
		}
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CMedicalRecordListDlg::InitRecordCounter第2个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		return;
	}
}

void CMedicalRecordListDlg::OnNMClickLstRecord(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	*pResult = 0;

	// 得到选中的行数
	int nIndex = pNMItemActivate->iItem;
    if (nIndex < 0 || nIndex >= m_nListValidCount) {
		m_nListCurrentIndex = -1;
		SetNaviButtonState();
        return;
    }
	m_nListCurrentIndex = nIndex;
	SetNaviButtonState();

	CString sMedicalNum = m_lstRecord.GetItemText(nIndex, MEDICAL_LIST_INDEX_OF_MEDICAL_NUM);
	UpdateDetialPanel(sMedicalNum);
}

void CMedicalRecordListDlg::UpdateDetialPanel(CString& sMedicalNum)
{
	//读出medno，并查询它对应的记录集
	CRecordset rs(&m_db);
	CString sql;
	sql.Format(
		_T("SELECT medical_num,patient_name,patient_type,gender,bloodtype,height,weight,birthday,address,phone_number,clinic,doctor,admission_date,create_date,operator ")
		_T("FROM medical_record WHERE medical_num = '%s'"), sMedicalNum);

	// 执行SQL语句
	CSmartLock lock(m_dbLock);
	try {
		rs.Open(AFX_DB_USE_DEFAULT_TYPE, sql);
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CMedicalRecordListDlg::UpdateDetialPanel第1个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		return;
	}

	//将内容填入右侧控件
	try {
		rs.MoveFirst();
	}
	catch (CDBException* pe) {
		_TCHAR err[255];
		pe->GetErrorMessage(err, 255);
		theApp.WriteLog(_T("异常位于：CMedicalRecordListDlg::UpdateDetialPanel第2个catch\n"));
		theApp.WriteLog(err);
		theApp.WriteLog(_T("\n"));
		pe->Delete();
		return;
	}
	if (!rs.IsEOF()) {
		try {
			CString sValue;
			CDBVariant var;

			rs.GetFieldValue(_T("medical_num"), sValue);
			m_edtMedicalNum.SetWindowText(sValue);

			rs.GetFieldValue(_T("patient_name"), sValue);
			m_edtPatientName.SetWindowText(sValue);

			rs.GetFieldValue(_T("patient_type"), var);
			sValue = CFMDict::PatientType_itos(var.m_lVal);
			m_edtPatientType.SetWindowText(sValue);

			rs.GetFieldValue(_T("gender"), var);
			sValue = CFMDict::Gender_itos(var.m_lVal);
			m_edtGender.SetWindowText(sValue);

			rs.GetFieldValue(_T("bloodtype"), var);
			sValue = CFMDict::BloodType_itos(var.m_lVal);
			m_edtBloodtype.SetWindowText(sValue);

			rs.GetFieldValue(_T("height"), var);
			sValue.Format(_T("%d"), var.m_lVal);
			m_edtHeight.SetWindowText(sValue);

			rs.GetFieldValue(_T("weight"), var);
			sValue.Format(_T("%d"), var.m_lVal);
			m_edtWeight.SetWindowText(sValue);

			rs.GetFieldValue(_T("birthday"), var);
			sValue.Format(_T("%04d-%02d-%02d"),
				(var.m_pdate)->year, (var.m_pdate)->month, (var.m_pdate)->day);
			m_edtBirthday.SetWindowText(sValue);

			rs.GetFieldValue(_T("address"), sValue);
			m_edtAddress.SetWindowText(sValue);

			rs.GetFieldValue(_T("phone_number"), sValue);
			m_edtPhoneNumber.SetWindowText(sValue);

			rs.GetFieldValue(_T("clinic"), sValue);
			m_edtClinic.SetWindowText(sValue);

			rs.GetFieldValue(_T("doctor"), sValue);
			m_edtDoctor.SetWindowText(sValue);

			rs.GetFieldValue(_T("admission_date"), var);
			sValue.Format(_T("%04d-%02d-%02d"),
				(var.m_pdate)->year, (var.m_pdate)->month, (var.m_pdate)->day);
			m_edtAdmissionDate.SetWindowText(sValue);

			rs.GetFieldValue(_T("create_date"), var);
			sValue.Format(_T("%04d-%02d-%02d"),
				(var.m_pdate)->year, (var.m_pdate)->month, (var.m_pdate)->day);
			m_edtCreateDate.SetWindowText(sValue);

			rs.GetFieldValue(_T("operator"), sValue);
			m_edtAdminName.SetWindowText(sValue);
		}
		catch (CDBException* pe) {
			_TCHAR err[255];
			pe->GetErrorMessage(err, 255);
			theApp.WriteLog(_T("异常位于：CMedicalRecordListDlg::UpdateDetialPanel第3个catch\n"));
			theApp.WriteLog(err);
			theApp.WriteLog(_T("\n"));
			pe->Delete();
			return;
		}
	}

	if (rs.IsOpen()) {
		rs.Close();
	}
}

void CMedicalRecordListDlg::OnNMDbClickLstRecord(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	*pResult = 0;

	if (TYPE_OP_SELECT == m_nType) {
		// 只有选择模式，双击才有用
		int nIndex = pNMItemActivate->iItem;
		if (nIndex < 0 || nIndex >= m_nListValidCount) {
			m_nListCurrentIndex = -1;
			SetNaviButtonState();
			return;
		}
		m_nListCurrentIndex = nIndex;

		OnBnClickedBtnSelect();
	}
}

void CMedicalRecordListDlg::MakeQueryString(CString& sql, CString& sWhereStatement)
{
	// 没有有效的查询条件，那么就查询全部记录
	if (sWhereStatement.IsEmpty()) {
		sql.Format(
			_T("SELECT medical_num,patient_name,birthday,gender,clinic,doctor,admission_date ")
			_T("FROM medical_record a ")
			_T("WHERE medical_num IN")
			_T("  (SELECT TOP %d medical_num FROM")
			_T("    (SELECT TOP %d medical_num ")
			_T("     FROM medical_record ORDER BY medical_num ASC")
			_T("    ) w ORDER BY w.medical_num DESC")
			_T("  ) ORDER BY a.medical_num ASC"),
			min(MEDICAL_LIST_LINAGE, m_nRecordCount - m_nPageStartIndex),
			m_nPageStartIndex + MEDICAL_LIST_LINAGE);
	}
	else {
		sql.Format(
			_T("SELECT medical_num,patient_name,birthday,gender,clinic,doctor,admission_date ")
			_T("FROM medical_record a ")
			_T("WHERE medical_num IN")
			_T("  (SELECT TOP %d medical_num FROM")
			_T("    (SELECT TOP %d medical_num FROM medical_record ")
			_T("     WHERE %s ORDER BY medical_num ASC")
			_T("    ) w ORDER BY w.medical_num DESC")
			_T("  ) ORDER BY a.medical_num ASC"),
			min(MEDICAL_LIST_LINAGE, m_nRecordCount - m_nPageStartIndex),
			m_nPageStartIndex + MEDICAL_LIST_LINAGE,
			sWhereStatement);
	};
}

void CMedicalRecordListDlg::UpdateRecordList(BOOL bGotoLastPage)
{
	// 执行查询
	CRecordset rs(&m_db);
	CString sWhereStatement;
	CString sql;

	m_lstRecord.DeleteAllItems();
	m_nListValidCount = 0;
	m_nListCurrentIndex = -1;

	MakeWhereStatement(sWhereStatement);
	InitRecordCounter(sWhereStatement);

	if (0 == m_nRecordCount) {
		//没有记录，也就没必要查询了
		goto fini;
	}
	if (bGotoLastPage) {
		//跳转到最后一页
		m_nPageStartIndex = ((m_nRecordCount - 1) / MEDICAL_LIST_LINAGE) * MEDICAL_LIST_LINAGE;
	}
	MakeQueryString(sql, sWhereStatement);
	OutputDebugString(sql + _T("\n"));

	{
		CSmartLock lock(m_dbLock);

		try {
			rs.Open(AFX_DB_USE_DEFAULT_TYPE, sql);
		}
		catch (CDBException* pe) {
			_TCHAR err[255];
			pe->GetErrorMessage(err, 255);
			theApp.WriteLog(_T("异常位于：CMedicalRecordListDlg::UpdateRecordList第1个catch\n"));
			theApp.WriteLog(err);
			theApp.WriteLog(_T("\n"));
			pe->Delete();
			return;
		}

		// 将查询内容写入列表
		m_lstRecord.DeleteAllItems();
		int nCount;
		try {
			while (! rs.IsEOF()) {
				rs.MoveNext();
			}
			nCount = rs.GetRecordCount();
		}
		catch (CDBException* pe) {
			_TCHAR err[255];
			pe->GetErrorMessage(err, 255);
			theApp.WriteLog(_T("异常位于：CMedicalRecordListDlg::UpdateRecordList第2个catch\n"));
			theApp.WriteLog(err);
			theApp.WriteLog(_T("\n"));
			pe->Delete();
			goto fini;
		}

		if (0 == nCount) {
			rs.Close();
			goto fini;
		}

		try {
			rs.MoveFirst();
		}
		catch (CDBException* pe) {
			_TCHAR err[255];
			pe->GetErrorMessage(err, 255);
			theApp.WriteLog(_T("异常位于：CMedicalRecordListDlg::UpdateRecordList第3个catch\n"));
			theApp.WriteLog(err);
			theApp.WriteLog(_T("\n"));
			pe->Delete();
			goto fini;
		}

		int i = 0;
		while (!rs.IsEOF()) {
		
			LVITEM lvi;
			_TCHAR buffer[256];
			lvi.mask = LVIF_TEXT;
			lvi.iItem = i;
			lvi.pszText = buffer;
			lvi.cchTextMax = 255;
			i ++;

			try {
				CString sValue;
				CDBVariant var;

				rs.GetFieldValue(_T("medical_num"), sValue);
				lvi.iSubItem = 0;
				_tcsncpy(lvi.pszText, sValue.GetBuffer(0), 255);
				m_lstRecord.InsertItem(&lvi);

				rs.GetFieldValue(_T("patient_name"), sValue);
				lvi.iSubItem = 1;
				_tcsncpy(lvi.pszText, sValue.GetBuffer(0), 255);
				m_lstRecord.SetItem(&lvi);

				rs.GetFieldValue(_T("birthday"), var);
				sValue.Format(_T("%04d-%02d-%02d"),
					(var.m_pdate)->year, (var.m_pdate)->month, (var.m_pdate)->day);
				lvi.iSubItem = 2;
				_tcsncpy(lvi.pszText, sValue.GetBuffer(0), 255);
				m_lstRecord.SetItem(&lvi);

				rs.GetFieldValue(_T("gender"), var);
				sValue = CFMDict::Gender_itos(var.m_lVal);
				lvi.iSubItem = 3;
				_tcsncpy(lvi.pszText, sValue.GetBuffer(0), 255);
				m_lstRecord.SetItem(&lvi);

				rs.GetFieldValue(_T("clinic"), sValue);
				lvi.iSubItem = 4;
				_tcsncpy(lvi.pszText, sValue.GetBuffer(0), 255);
				m_lstRecord.SetItem(&lvi);

				rs.GetFieldValue(_T("doctor"), sValue);
				lvi.iSubItem = 5;
				_tcsncpy(lvi.pszText, sValue.GetBuffer(0), 255);
				m_lstRecord.SetItem(&lvi);

				rs.GetFieldValue(_T("admission_date"), var);
				sValue.Format(_T("%04d-%02d-%02d %02d:%02d:%02d"),
					(var.m_pdate)->year, (var.m_pdate)->month, (var.m_pdate)->day,
					(var.m_pdate)->hour, (var.m_pdate)->minute, (var.m_pdate)->second);
				lvi.iSubItem = 6;
				_tcsncpy(lvi.pszText, sValue.GetBuffer(0), 255);
				m_lstRecord.SetItem(&lvi);
			}
			catch (CDBException* pe) {
				_TCHAR err[255];
				pe->GetErrorMessage(err, 255);
				theApp.WriteLog(_T("异常位于：CMedicalRecordListDlg::UpdateRecordList第4个catch\n"));
				theApp.WriteLog(err);
				theApp.WriteLog(_T("\n"));
				pe->Delete();
				m_nListValidCount = i;
				goto fini;
			}
			rs.MoveNext();
		}
		//记录列表中有效内容的行数
		m_nListValidCount = i;

		if (rs.IsOpen()) {
			rs.Close();
		}
	}

fini:

	//设置页数和页码
	CString sInfo;
	sInfo.Format(_T("%d条，%d页(%d)"),
		m_nRecordCount, //总条数
		(m_nRecordCount + MEDICAL_LIST_LINAGE - 1) / MEDICAL_LIST_LINAGE, //计算占用的页数
		m_nPageStartIndex / MEDICAL_LIST_LINAGE + 1);//当前页
	m_edtPageInfo.SetWindowText(sInfo);
}

void CMedicalRecordListDlg::OnBnClickedBtnSelect()
{
	//只有选择模式下，这个按钮才起作用
	if (TYPE_OP_SELECT != m_nType) {
		return;
	}
	if (-1 == m_nListCurrentIndex) {
		return;
	}

	m_sSelectedMedicalNum = m_lstRecord.GetItemText(m_nListCurrentIndex, MEDICAL_LIST_INDEX_OF_MEDICAL_NUM);
	CDialogEx::OnOK();
}

void CMedicalRecordListDlg::ClearSearchString()
{
	m_sSearchMedicalNum = _T("");
	m_sSearchPatientName = _T("");
	m_sSearchGender = _T("");
	m_sSearchAddress = _T("");
	m_sSearchPhoneNumber = _T("");
	m_sSearchClinic = _T("");
	m_sSearchDoctor = _T("");
	m_sSearchAdmission = _T("");
}

void CMedicalRecordListDlg::MakeWhereStatement(CString& sWhereStatement)
{
	if (! m_sSearchMedicalNum.IsEmpty()) {
		sWhereStatement += m_sSearchMedicalNum;
	}
	if (! m_sSearchPatientName.IsEmpty()) {
		if (! sWhereStatement.IsEmpty()) {
			sWhereStatement += _T(" AND");
		}
		sWhereStatement += _T(" ");
		sWhereStatement += m_sSearchPatientName;
	}
	if (! m_sSearchPatientType.IsEmpty()) {
		if (! sWhereStatement.IsEmpty()) {
			sWhereStatement += _T(" AND");
		}
		sWhereStatement += _T(" ");
		sWhereStatement += m_sSearchPatientType;
	}
	if (! m_sSearchGender.IsEmpty()) {
		if (! sWhereStatement.IsEmpty()) {
			sWhereStatement += _T(" AND");
		}
		sWhereStatement += _T(" ");
		sWhereStatement += m_sSearchGender;
	}
	if (! m_sSearchGender.IsEmpty()) {
		if (! sWhereStatement.IsEmpty()) {
			sWhereStatement += _T(" AND");
		}
		sWhereStatement += _T(" ");
		sWhereStatement += m_sSearchGender;
	}
	if (! m_sSearchAddress.IsEmpty()) {
		if (! sWhereStatement.IsEmpty()) {
			sWhereStatement += _T(" AND");
		}
		sWhereStatement += _T(" ");
		sWhereStatement += m_sSearchAddress;
	}
	if (! m_sSearchPhoneNumber.IsEmpty()) {
		if (! sWhereStatement.IsEmpty()) {
			sWhereStatement += _T(" AND");
		}
		sWhereStatement += _T(" ");
		sWhereStatement += m_sSearchPhoneNumber;
	}
	if (! m_sSearchClinic.IsEmpty()) {
		if (! sWhereStatement.IsEmpty()) {
			sWhereStatement += _T(" AND");
		}
		sWhereStatement += _T(" ");
		sWhereStatement += m_sSearchClinic;
	}
	if (! m_sSearchDoctor.IsEmpty()) {
		if (! sWhereStatement.IsEmpty()) {
			sWhereStatement += _T(" AND");
		}
		sWhereStatement += _T(" ");
		sWhereStatement += m_sSearchDoctor;
	}
	if (! m_sSearchAdmission.IsEmpty()) {
		if (! sWhereStatement.IsEmpty()) {
			sWhereStatement += _T(" AND");
		}
		sWhereStatement += _T(" ");
		sWhereStatement += m_sSearchAdmission;
	}
}

void CMedicalRecordListDlg::OnBnClickedBtnSearchByMediclNum()
{
	CString sValue;
	ClearSearchString();
	m_edtSearchMedicalNum.GetWindowText(sValue);
	if (sValue.IsEmpty()) {
		m_sSearchMedicalNum = _T("medical_num=''");
	}
	else {
		m_sSearchMedicalNum.Format(_T("medical_num LIKE '%%%s%%'"), sValue);
	}
	m_nPageStartIndex = 0;
	UpdateRecordList();
	SetNaviButtonState();
}

void CMedicalRecordListDlg::OnBnClickedBtnSearchByName()
{
	CString sValue;
	ClearSearchString();
	m_edtSearchPatientName.GetWindowText(sValue);
	if (sValue.IsEmpty()) {
		m_sSearchPatientName = _T("patient_name=''");
	}
	else {
		m_sSearchPatientName.Format(_T("patient_name LIKE '%%%s%%'"), sValue);
	}
	m_nPageStartIndex = 0;
	UpdateRecordList();
	SetNaviButtonState();
}

void CMedicalRecordListDlg::OnBnClickedBtnSearchByType()
{
	int nIndex;
	ClearSearchString();
	nIndex = m_cmbSearchPatientType.GetCurSel();
	m_sSearchPatientType.Format(_T("gender='%d'"), nIndex);
	m_nPageStartIndex = 0;
	UpdateRecordList();
	SetNaviButtonState();
}

void CMedicalRecordListDlg::OnBnClickedBtnSearchByGender()
{
	int nIndex;
	ClearSearchString();
	nIndex = m_cmbSearchGender.GetCurSel();
	m_sSearchGender.Format(_T("gender='%d'"), nIndex);
	m_nPageStartIndex = 0;
	UpdateRecordList();
	SetNaviButtonState();
}

void CMedicalRecordListDlg::OnBnClickedBtnSearchByAddress()
{
	CString sValue;
	ClearSearchString();
	m_edtSearchAddress.GetWindowText(sValue);
	if (sValue.IsEmpty()) {
		m_sSearchAddress = _T("address=''");
	}
	else {
		m_sSearchAddress.Format(_T("address LIKE '%%%s%%'"), sValue);
	}
	m_nPageStartIndex = 0;
	UpdateRecordList();
	SetNaviButtonState();
}

void CMedicalRecordListDlg::OnBnClickedBtnSearchByPhoneNumber()
{
	CString sValue;
	ClearSearchString();
	m_edtSearchPhoneNumber.GetWindowText(sValue);
	if (sValue.IsEmpty()) {
		m_sSearchPhoneNumber = _T("phone_number=''");
	}
	else {
		m_sSearchPhoneNumber.Format(_T("phone_number LIKE '%%%s%%'"), sValue);
	}
	m_nPageStartIndex = 0;
	UpdateRecordList();
	SetNaviButtonState();
}

void CMedicalRecordListDlg::OnBnClickedBtnSearchByClinic()
{
	CString sValue;
	ClearSearchString();
	m_edtSearchClinic.GetWindowText(sValue);
	if (sValue.IsEmpty()) {
		m_sSearchClinic = _T("clinic=''");
	}
	else {
		m_sSearchClinic.Format(_T("clinic LIKE '%%%s%%'"), sValue);
	}
	m_nPageStartIndex = 0;
	UpdateRecordList();
	SetNaviButtonState();
}

void CMedicalRecordListDlg::OnBnClickedBtnSearchByDoctor()
{
	CString sValue;
	ClearSearchString();
	m_edtSearchDoctor.GetWindowText(sValue);
	if (sValue.IsEmpty()) {
		m_sSearchDoctor = _T("doctor=''");
	}
	else {
		m_sSearchDoctor.Format(_T("doctor LIKE '%%%s%%'"), sValue);
	}
	m_nPageStartIndex = 0;
	UpdateRecordList();
	SetNaviButtonState();
}

void CMedicalRecordListDlg::OnBnClickedBtnSearchByAdmission()
{
	CTime tm;
	CString sTime;
	ClearSearchString();
	m_datSearchAdmission.GetTime(tm);
	sTime = tm.Format(_T("%Y-%m-%d %H:%M:%S"));
	m_sSearchAdmission.Format(_T("birthday >= '%s'"), sTime);
	m_nPageStartIndex = 0;
	UpdateRecordList();
	SetNaviButtonState();
}

void CMedicalRecordListDlg::OnBnClickedBtnSearchAll()
{
	ClearSearchString();
	m_nPageStartIndex = 0;
	UpdateRecordList();
	SetNaviButtonState();
}

void CMedicalRecordListDlg::OnBnClickedCancel()
{
	// TODO: 在此添加控件通知处理程序代码
	CDialogEx::OnCancel();
}
