#pragma once
#include "afxwin.h"
#include "afxcmn.h"


// CSetupNIBPDlg 对话框
class CFMRecordUnit;
class CSetupNIBPDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CSetupNIBPDlg)

private:
	CFMRecordUnit* m_pru;
	int m_nNIBPUnitOpt;

public:
	CSetupNIBPDlg(CWnd* pParent, CFMRecordUnit* pru, int nNIBPUnit);
	virtual ~CSetupNIBPDlg();

	inline int GetNIBPUnitOpt() { return m_nNIBPUnitOpt; };

// 对话框数据
	enum { IDD = IDD_DIALOG_SETUP_NIBP };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CEdit m_edtSysHigh;
	CEdit m_edtSysLow;
	CEdit m_edtMeaHigh;
	CEdit m_edtMeaLow;
	CEdit m_edtDiaHigh;
	CEdit m_edtDiaLow;
	CEdit m_edtPumpInfo;
	CComboBox m_cmbMode;
	CComboBox m_cmbNIBPUnit;
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedPumpStart();
	afx_msg void OnBnClickedPumpStop();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedBtnSend();
	CStatic m_staSysRange1;
	CStatic m_staSysRange2;
	CStatic m_staMeaRange1;
	CStatic m_staMeaRange2;
	CStatic m_staDiaRange1;
	CStatic m_staDiaRange2;
};
