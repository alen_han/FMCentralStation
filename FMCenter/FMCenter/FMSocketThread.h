#pragma once


class CFMRecordThread;
class CFMSocketUnit;
class CFMSocketThread
{
public:
	CFMSocketThread(CFMRecordThread* pRecord);
	virtual ~CFMSocketThread(void);

public:
	SOCKET m_sListenSocket;
	HANDLE m_hAcceptEvent;
	HANDLE m_hShutdownEvent;
	HANDLE m_hIOCompletionPort;
	CFMSocketUnit* m_arUnits[CLIENT_COUNT];
	inline CFMSocketUnit* GetUnit(int n) { return m_arUnits[n]; };

private:
	CFMRecordThread* m_pRecord;
	CCriticalSection m_cs;
	HANDLE m_hListenThread;

	int m_nWorkThreadCount;
	HANDLE* m_arWorkThreadGroup;

	BOOL CreateListenSocket();
	void CloseListenSocket();
	void CleanUp();
	
public:
	BOOL Start();
	void SetSocketUnit(int nBedIndex, BOOL bEnable);
	void CleanSocketUnits();
	void CheckSockets();
};

