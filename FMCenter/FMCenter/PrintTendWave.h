#pragma once
#include "reviewdlg.h"
class CPrintTendWave : public CPrintTool
{
public:
	CPrintTendWave(CReviewDlg* pdlg);
	virtual ~CPrintTendWave(void);

public:
	virtual void InitSetup(int type=0);
	virtual void Print(CDC* pdc,  CPrintInfo* pInfo);

private:
	CPen  m_penStyle1;
	CPen  m_penStyle2;
	CPen  m_penStyle3;
	CFont m_fontInfo;
	CFont* m_pOldFont;
	int  m_nCurPen;

private:
	inline void ResetGetPen() { m_nCurPen = 0; };
	CPen* GetPen();
	void DrawPatientInfo(CDC* pdc, CRect& rc);
	void DrawGrid(CDC* pdc, CRect& rc);
	void DrawTime(CDC* pdc, CRect& rc);
	void DrawTimeRuler(CDC* pdc, int x, int y, CTime tm, int nSeconds);
	void DrawTableInfo(CDC* pdc, CRect& rc);
	void DrawData(CDC* pdc, CRect& rc);
};

