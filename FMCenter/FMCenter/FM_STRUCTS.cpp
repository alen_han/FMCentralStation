// FM_STRUCTS.cpp

#include "StdAfx.h"
#include "StrTools.h"
#include "FM_STRUCTS.h"
#include "FMSocketUnit.h"

BOOL CStructTools::SendData(CFMSocketUnit* pu, BYTE* buffer, int length)
{
#ifdef __NO_SEND_DATA__
	return TRUE;
#else
	return pu->SendData(buffer, length);
#endif
}

//返回值：0表示OK
//正数：“包起始标志”之前存在的残留数据的数量
//-1：表示还没接收完，继续收吧
//-2：出错，后面的也没法处理了
int CStructTools::CheckData(BYTE* buffer, int length, int* data_size, int* expect_size)
{
	int i, count;
	SOCK_PKG* pk;
	(*data_size) = 0;
	(*expect_size) = 0;
	if (length < sizeof(HEAD)) {
		//至少收到完整的包头，才能开始解析
		(*expect_size) = sizeof(HEAD) - length;
		return -1;
	}

	count = length - 4;
	for (i = 0; i < count; i ++) {
		if (CheckFlag(buffer + i)) {
			break;
		}
	}
	//搜到末尾了还没匹配到“包起始标志”
	if (i == count) {
		return -2;
	}
	//存在残留数据，返回残留数据的长度
	if (i > 0) {
		return i;
	}

	pk = (SOCK_PKG*)buffer;
	(*data_size) = sizeof(HEAD) + pk->head.body_size;

	switch (pk->head.pkg_type) {
	case PKG_QUIT:
	case PKG_NEXT:
	case PKG_PATIENTINFO:
	case PKG_CLIENT_DATA:
		break;
	default: //非法类型
		return -2;
	}

	if (length >= (*data_size)) {
		return 0;
	}

	//还没收完，继续接收
	(*expect_size) = (*data_size) - length;
	return -1;
}

int CStructTools::GetClientDataSize(DWORD opt)
{
	int ret = sizeof(DWORD);
	if (opt && OPT_ECG) {
		ret += sizeof(ECG);
	}
	if (opt && OPT_RESP) {
		ret += sizeof(RESP);
	}
	if (opt && OPT_SPO2) {
		ret += sizeof(SPO2);
	}
	if (opt && OPT_NIBP) {
		ret += sizeof(NIBP);
	}
	if (opt && OPT_TEMP) {
		ret += sizeof(TEMP);
	}
	if (opt && OPT_FETUS) {
		ret += sizeof(FETUS);
	}
	return ret;
}

BOOL CStructTools::CheckFlag(BYTE* buffer)
{
	return ((0xA5 == buffer[3]) &&
			(0x55 == buffer[2]) &&
			(0xAA == buffer[1]) &&
			(0x5A == buffer[0]));
}

void CStructTools::InitFlag(HEAD& h)
{
	h.flag[0] = 0x5A;
	h.flag[1] = 0xAA;
	h.flag[2] = 0x55;
	h.flag[3] = 0xA5;
}

void CStructTools::InitHead(HEAD& h, BYTE bed_num, BYTE type, WORD size)
{
	InitFlag(h);
	h.bed_num = bed_num;
	h.pkg_type = type;
	h.body_size = size;
}

BOOL CStructTools::SendQuit(CFMSocketUnit* pu)
{
	HEAD hh;
	InitHead(hh, pu->GetBedNum(), PKG_QUIT, 0);
	return (SendData(pu, (BYTE*)&hh, sizeof(HEAD)));
}

BOOL CStructTools::SendNext(CFMSocketUnit* pu)
{
	HEAD hh;
	InitHead(hh, pu->GetBedNum(), PKG_NEXT, 0);
	return (SendData(pu, (BYTE*)&hh, sizeof(HEAD)));
}

BOOL CStructTools::SendBusy(CFMSocketUnit* pu)
{
	HEAD hh;
	InitHead(hh, pu->GetBedNum(), PKG_BUSY, 0);
	return (SendData(pu, (BYTE*)&hh, sizeof(HEAD)));
}

BOOL CStructTools::SendSetupECG(CFMSocketUnit* pu,
								int ecg_l1,
								int ecg_l2,
								int filter,
								int gain,
								int stop,
								int paceangy,
								int stangy,
								int rhythm,
								int hr_h,
								int hr_l,
								int st1_h,
								int st1_l,
								int st2_h,
								int st2_l,
								int pvc_h,
								int pvc_l)
{
	SOCK_PKG pk;
	InitHead(pk.head, pu->GetBedNum(), PKG_ECG_COMMAND, sizeof(ECGCTRLINFO));
	pk.body.setup_ecg.ecg_lead1    = ecg_l1;
	pk.body.setup_ecg.ecg_lead2    = ecg_l2;
	pk.body.setup_ecg.ecg_filter   = filter;
	pk.body.setup_ecg.ecg_gain     = gain;
	pk.body.setup_ecg.ecg_stop     = stop;
	pk.body.setup_ecg.ecg_paceangy = paceangy;
	pk.body.setup_ecg.ecg_stangy   = stangy;
	pk.body.setup_ecg.ecg_rhythm   = rhythm;
	pk.body.setup_ecg.hr_high      = hr_h;
	pk.body.setup_ecg.hr_low       = hr_l;
	pk.body.setup_ecg.st1_high     = st1_h;
	pk.body.setup_ecg.st1_low      = st1_l;
	pk.body.setup_ecg.st2_high     = st2_h;
	pk.body.setup_ecg.st2_low      = st2_l;
	pk.body.setup_ecg.pvc_high     = pvc_h;
	pk.body.setup_ecg.pvc_low      = pvc_l;
	return SendData(pu, (BYTE*)&pk, sizeof(HEAD) + sizeof(ECGCTRLINFO));
}

BOOL CStructTools::SendSetupNIBP(CFMSocketUnit* pu,
								 int sys_h,
								 int sys_l,
								 int mea_h,
								 int mea_l,
								 int dia_h,
								 int dia_l,
								 int nibp_mode)
{
	SOCK_PKG pk;
	InitHead(pk.head, pu->GetBedNum(), PKG_NIBP_COMMAND, sizeof(NIBPALARMINFO));
	pk.body.setup_nibp.sys_high = sys_h;
	pk.body.setup_nibp.sys_low  = sys_l;
	pk.body.setup_nibp.mea_high = mea_h;
	pk.body.setup_nibp.mea_low  = mea_l;
	pk.body.setup_nibp.dia_high = dia_h;
	pk.body.setup_nibp.dia_low  = dia_l;
	pk.body.setup_nibp.bp_mode  = nibp_mode;
	return SendData(pu, (BYTE*)&pk, sizeof(HEAD) + sizeof(NIBPALARMINFO));
}

BOOL CStructTools::SendSetupRESP(CFMSocketUnit* pu,
								 int rr_h,
								 int rr_l)
{
	SOCK_PKG pk;
	InitHead(pk.head, pu->GetBedNum(), PKG_RESP_COMMAND, sizeof(RESPALARMINFO));
	pk.body.setup_resp.rr_high = rr_h;
	pk.body.setup_resp.rr_low  = rr_l;
	return SendData(pu, (BYTE*)&pk, sizeof(HEAD) + sizeof(RESPALARMINFO));
}

BOOL CStructTools::SendSetupSPO2(CFMSocketUnit* pu,
								 int spo2_h,
								 int spo2_l,
								 int pr_h,
								 int pr_l)
{
	SOCK_PKG pk;
	InitHead(pk.head, pu->GetBedNum(), PKG_SPO2_COMMAND, sizeof(SPO2ALARMINFO));
	pk.body.setup_spo2.spo2_high = spo2_h;
	pk.body.setup_spo2.spo2_low  = spo2_l;
	pk.body.setup_spo2.pr_high   = pr_h;
	pk.body.setup_spo2.pr_low    = pr_l;
	return SendData(pu, (BYTE*)&pk, sizeof(HEAD) + sizeof(SPO2ALARMINFO));
}

BOOL CStructTools::SendSetupTEMP(CFMSocketUnit* pu,
								 int t1_h,
								 int t1_l,
								 int t2_h,
								 int t2_l)
{
	SOCK_PKG pk;
	InitHead(pk.head, pu->GetBedNum(), PKG_TEMP_COMMAND, sizeof(TEMPALARMINFO));
	pk.body.setup_temp.t1_high = t1_h;
	pk.body.setup_temp.t1_low  = t1_l;
	pk.body.setup_temp.t2_high = t2_h;
	pk.body.setup_temp.t2_low  = t2_h;
	return SendData(pu, (BYTE*)&pk, sizeof(HEAD) + sizeof(TEMPALARMINFO));
}

BOOL CStructTools::SendNIBPPumpCmd(CFMSocketUnit* pu, int cmd)
{
	SOCK_PKG pk;
	InitHead(pk.head, pu->GetBedNum(), PKG_NIBPSTART_COMMAND, sizeof(NIBPCTRLINFO));
	pk.body.nibp_pump.status = cmd;
	return SendData(pu, (BYTE*)&pk, sizeof(HEAD) + sizeof(NIBPCTRLINFO));
}

BOOL CStructTools::SendSetupFHR(CFMSocketUnit* pu, int fhr_h, int fhr_l)
{
	SOCK_PKG pk;
	InitHead(pk.head, pu->GetBedNum(), PKG_FHR_COMMAND, sizeof(FHRALARMINFO));
	pk.body.setup_fhr.fhr_high = fhr_h;
	pk.body.setup_fhr.fhr_low = fhr_l;
	return SendData(pu, (BYTE*)&pk, sizeof(HEAD) + sizeof(FHRALARMINFO));
}

BOOL CStructTools::SendPatientInfo(CFMSocketUnit* pu, PATIENTINFO& pi)
{
	SOCK_PKG pk;
	InitHead(pk.head, pu->GetBedNum(), PKG_PATIENTINFO, sizeof(PATIENTINFO));
	memcpy(&pk.body.pi, &pi, sizeof(PATIENTINFO));
	return SendData(pu, (BYTE*)&pk, sizeof(HEAD) + sizeof(PATIENTINFO));
}

BOOL CStructTools::SendClientData(CFMSocketUnit* pu,
								  DWORD opt,
								  ECG& e,
								  RESP& r,
								  SPO2& s,
								  NIBP& n,
								  TEMP& t,
								  FETUS& f)
{
	int i = 0;
	int body_size = GetClientDataSize(opt);
	SOCK_PKG pk;
	InitHead(pk.head, pu->GetBedNum(), PKG_CLIENT_DATA, body_size);

	pk.body.cd.data_opt = opt;
	if (opt && OPT_ECG) {
		memcpy(&(pk.body.cd.data_buf[i]), &e, sizeof(ECG));
		i += sizeof(ECG);
	}
	if (opt && OPT_RESP) {
		memcpy(&(pk.body.cd.data_buf[i]), &r, sizeof(RESP));
		i += sizeof(RESP);
	}
	if (opt && OPT_SPO2) {
		memcpy(&(pk.body.cd.data_buf[i]), &s, sizeof(SPO2));
		i += sizeof(SPO2);
	}
	if (opt && OPT_NIBP) {
		memcpy(&(pk.body.cd.data_buf[i]), &n, sizeof(NIBP));
		i += sizeof(NIBP);
	}
	if (opt && OPT_TEMP) {
		memcpy(&(pk.body.cd.data_buf[i]), &t, sizeof(TEMP));
		i += sizeof(TEMP);
	}
	if (opt && OPT_FETUS) {
		memcpy(&(pk.body.cd.data_buf[i]), &f, sizeof(FETUS));
		i += sizeof(FETUS);
	}

	return SendData(pu, (BYTE*)&pk, sizeof(HEAD) + body_size);
}

void CStructTools::ParseClientData(CLIENT_DATA* pcd,
								   ECG** ppe,
								   RESP** ppr,
								   SPO2** pps,
								   NIBP** ppn,
								   TEMP** ppt, 
								   FETUS** ppf)
{
	int opt = pcd->data_opt;

	int i = 0;
	if (opt && OPT_ECG) {
		(* ppe) = (ECG*)&(pcd->data_buf[i]);
		i += sizeof(ECG);
	}
	else {
		(* ppe) = NULL;
	}

	if (opt && OPT_RESP) {
		(* ppr) = (RESP*)&(pcd->data_buf[i]);
		i += sizeof(RESP);
	}
	else {
		(* ppr) = NULL;
	}

	if (opt && OPT_SPO2) {
		(* pps) = (SPO2*)&(pcd->data_buf[i]);
		i += sizeof(SPO2);
	}
	else {
		(* pps) = NULL;
	}

	if (opt && OPT_NIBP) {
		(* ppn) = (NIBP*)&(pcd->data_buf[i]);
		i += sizeof(NIBP);
	}
	else {
		(* ppn) = NULL;
	}

	if (opt && OPT_TEMP) {
		(* ppt) = (TEMP*)&(pcd->data_buf[i]);
		i += sizeof(TEMP);
	}
	else {
		(* ppt) = NULL;
	}

	if (opt && OPT_FETUS) {
		(* ppf) = (FETUS*)&(pcd->data_buf[i]);
		i += sizeof(FETUS);
	}
	else {
		(* ppf) = NULL;
	}
}

CString CStructTools::DumpHead(SOCK_PKG* pk)
{
	CString info = _T("\n");
	CString s;
	HEAD* ph = &(pk->head);
	s.Format(_T("flag:0x%02X,0x%02X,0x%02X,0x%02X\n"), ph->flag[3], ph->flag[2], ph->flag[1], ph->flag[0]);
	info += s;
	s.Format(_T("body_size:%u\n"), ph->body_size);
	info += s;
	s.Format(_T("bed_num:%u\n"), ph->bed_num);
	info += s;
	s.Format(_T("pkg_type:0x%02X\n"), ph->pkg_type);
	info += s;
	info += _T("\n");
	OutputDebugString(info);
	return info;
}

CString CStructTools::DumpClientData(SOCK_PKG* pk)
{
	CString info = _T("\n");

	info += DumpHead(pk);
	BODY* pb = &(pk->body);
	CLIENT_DATA* pcd = &(pb->cd);
	int opt = pcd->data_opt;
	CString s;
	s.Format(_T("\ndata_opt:0x%08X\n"), opt);
	OutputDebugString(s);

	int i = 0;
	if (opt && OPT_ECG) {
		ECG* pe = (ECG*)&(pcd->data_buf[i]);
		info += DumpECG(pe);
		i += sizeof(ECG);
	}

	if (opt && OPT_RESP) {
		RESP* pr = (RESP*)&(pcd->data_buf[i]);
		info += DumpRESP(pr);
		i += sizeof(RESP);
	}

	if (opt && OPT_SPO2) {
		SPO2* ps = (SPO2*)&(pcd->data_buf[i]);
		info += DumpSPO2(ps);
		i += sizeof(SPO2);
	}

	if (opt && OPT_NIBP) {
		NIBP* pn = (NIBP*)&(pcd->data_buf[i]);
		info += DumpNIBP(pn);
		i += sizeof(NIBP);
	}

	if (opt && OPT_TEMP) {
		TEMP* pt = (TEMP*)&(pcd->data_buf[i]);
		info += DumpTEMP(pt);
		i += sizeof(TEMP);
	}

	if (opt && OPT_FETUS) {
		FETUS* pf = (FETUS*)&(pcd->data_buf[i]);
		info += DumpFETUS(pf);
		i += sizeof(FETUS);
	}

	OutputDebugString(_T("\n"));
	return info;
}

CString CStructTools::DumpPatientInfo(SOCK_PKG* pk)
{
	CString info = _T("");

	info += DumpHead(pk);
	CString s;
	BODY* pd = &(pk->body);
	PATIENTINFO* p = &(pd->pi);

	s.Format(_T("name:%24s\n"), AtoW(p->name, 24));
	info += s;
	s.Format(_T("medno:%24s\n"), AtoW(p->medno, 24));
	info += s;
	s.Format(_T("doctor:%24s\n"), AtoW(p->doctor, 24));
	info += s;
	s.Format(_T("age:%6s\n"), AtoW(p->age, 6));
	info += s;
	s.Format(_T("height:%6s\n"), AtoW(p->height, 6));
	info += s;
	s.Format(_T("weight:%6s\n"), AtoW(p->weight, 6));
	info += s;
	CString sGender = CFMDict::Gender_itos(p->gender);
	s.Format(_T("gender:%s\n"), sGender);
	info += s;
	CString sBloodType = CFMDict::BloodType_itos(p->bloodtype);
	s.Format(_T("bloodtype:%s\n"), sBloodType);
	info += s;
	CString sPatientType = CFMDict::PatientType_itos(p->patienttype);
	s.Format(_T("patienttype:%s\n"), sPatientType);
	info += s;
	s.Format(_T("comment:%24s\n"), AtoW(p->comment, 24));
	info += s;

	OutputDebugString(info);
	OutputDebugString(_T("\n"));
	return info;
}

CString CStructTools::DumpECG(ECG* pe)
{
	CString info = _T("\nDumpECG:\n");
	CString s;
	s.Format(_T("ecg_lead1:%u\n"), pe->ecg_lead1);
	info += s;
	s.Format(_T("ecg_lead2:%u\n"), pe->ecg_lead2);
	info += s;
	s.Format(_T("ecg_filter:%u\n"), pe->ecg_filter);
	info += s;
	s.Format(_T("ecg_speed:%u\n"), pe->ecg_speed);
	info += s;
	s.Format(_T("ecg_gain:%u\n"), pe->ecg_gain);
	info += s;
	s.Format(_T("ecg_stop:%u\n"), pe->ecg_stop);
	info += s;
	s.Format(_T("ecg_paceangy:%u\n"), pe->ecg_paceangy);
	info += s;
	s.Format(_T("ecg_stangy:%u\n"), pe->ecg_stangy);
	info += s;
	s.Format(_T("ecg_rhythm:%u\n"), pe->ecg_rhythm);
	info += s;
	s.Format(_T("hr:%u\n"), pe->hr);
	info += s;
	s.Format(_T("hr_high:%u\n"), pe->hr_high);
	info += s;
	s.Format(_T("hr_low:%u\n"), pe->hr_low);
	info += s;
	s.Format(_T("hr_level:%u\n"), pe->hr_level);
	info += s;
	s.Format(_T("st1:%u\n"), pe->st1);
	info += s;
	s.Format(_T("st1_high:%u\n"), pe->st1_high);
	info += s;
	s.Format(_T("st1_low:%u\n"), pe->st1_low);
	info += s;
	s.Format(_T("st2:%u\n"), pe->st2);
	info += s;
	s.Format(_T("st2_high:%u\n"), pe->st2_high);
	info += s;
	s.Format(_T("st2_low:%u\n"), pe->st2_low);
	info += s;
	s.Format(_T("st_level:%u\n"), pe->st_level);
	info += s;
	s.Format(_T("pvc:%u\n"), pe->pvc);
	info += s;
	s.Format(_T("pvc_high:%u\n"), pe->pvc_high);
	info += s;
	s.Format(_T("pvc_level:%u\n"), pe->pvc_level);
	info += s;
	s.Format(_T("error:%u\n"), pe->error);
	info += s;
	s.Format(_T("hr_source:%u\n"), pe->hr_source);
	info += s;
	s.Format(_T("arr_status:%u\n"), pe->arr_status);
	info += s;
	//波形数据太长了，暂时挂起
	//s.Format(_T("ecg1data - ECG1:\n"));
	//info += s;
	//pb = &(pe->ecg1data[0]);
	//for (i=0; i<ECG_WAVE_DATA_PER_SECOND; i++) {
	//	s.Format(_T("[%d]:%u\n"), i, pb[i]);
	//	info += s;
	//}
	//s.Format(_T("ecg2data - ECG2:\n"));
	//info += s;
	//pb = &(pe->ecg2data[ECG_WAVE_DATA_PER_SECOND]);
	//for (i=0; i<ECG_WAVE_DATA_PER_SECOND; i++) {
	//	s.Format(_T("[%d]:%u\n"), i, pb[i]);
	//	info += s;
	//}
	//s.Format(_T("ecg3data - RR:\n"));
	//info += s;
	//pb = &(pe->ecg3data[ECG_WAVE_DATA_PER_SECOND * 2]);
	//for (i=0; i<ECG_WAVE_DATA_PER_SECOND; i++) {
	//	s.Format(_T("[%d]:%u\n"), i, pb[i]);
	//	info += s;
	//}
	OutputDebugString(info);
	return info;
}

CString CStructTools::DumpRESP(RESP* pr)
{
	CString info = _T("\nDumpRESP:\n");
	CString s;
	s.Format(_T("rr_speed:%u\n"), pr->rr_speed);
	info += s;
	s.Format(_T("rr_gain:%u\n"), pr->rr_gain);
	info += s;
	s.Format(_T("rr:%u\n"), pr->rr);
	info += s;
	s.Format(_T("rr_high:%u\n"), pr->rr_high);
	info += s;
	s.Format(_T("rr_low:%u\n"), pr->rr_low);
	info += s;
	s.Format(_T("rr_level:%u\n"), pr->rr_level);
	info += s;
	s.Format(_T("error:%u\n"), pr->error);
	info += s;
	//波形数据太长了，暂时挂起
	//s.Format(_T("respdata - ECG3:\n"));
	//info += s;
	//for (i=0; i<RESP_WAVE_DATA_PER_SECOND; i++) {
	//	s.Format(_T("[%d]:%u\n"), i, pr->respdata[i]);
	//	info += s;
	//}
	OutputDebugString(info);
	return info;
}

CString CStructTools::DumpSPO2(SPO2* ps)
{
	CString info = _T("\nDumpSPO2:\n");
	CString s;
	s.Format(_T("spo2_speed:%u\n"), ps->spo2_speed);
	info += s;
	s.Format(_T("spo2_gain:%u\n"), ps->spo2_gain);
	info += s;
	s.Format(_T("spo2:%u\n"), ps->spo2);
	info += s;
	s.Format(_T("spo2_high:%u\n"), ps->spo2_high);
	info += s;
	s.Format(_T("spo2_low:%u\n"), ps->spo2_low);
	info += s;
	s.Format(_T("spo2_level:%u\n"), ps->spo2_level);
	info += s;
	s.Format(_T("pr:%u\n"), ps->pr);
	info += s;
	s.Format(_T("pr_high:%u\n"), ps->pr_high);
	info += s;
	s.Format(_T("pr_low:%u\n"), ps->pr_low);
	info += s;
	s.Format(_T("pr_level:%u\n"), ps->pr_level);
	info += s;
	s.Format(_T("error:%u\n"), ps->error);
	info += s;
	s.Format(_T("status:%u\n"), ps->status);
	info += s;
	s.Format(_T("wavedata:\n"));
	info += s;
	//波形数据太长了，暂时挂起
	//for (i=0; i<SPO2_WAVE_DATA_PER_SECOND; i++) {
	//	s.Format(_T("[%d]:%u\n"), i, ps->wavedata[i]);
	//	info += s;
	//}
	OutputDebugString(info);
	return info;
}

CString CStructTools::DumpNIBP(NIBP* pn)
{
	CString info = _T("\nDumpNIBP:\n");
	CString s;
	s.Format(_T("sys:%u\n"), pn->sys);
	info += s;
	s.Format(_T("sys_high:%u\n"), pn->sys_high);
	info += s;
	s.Format(_T("sys_low:%u\n"), pn->sys_low);
	info += s;
	s.Format(_T("mea:%u\n"), pn->mea);
	info += s;
	s.Format(_T("mea_high:%u\n"), pn->mea_high);
	info += s;
	s.Format(_T("mea_low:%u\n"), pn->mea_low);
	info += s;
	s.Format(_T("dia:%u\n"), pn->dia);
	info += s;
	s.Format(_T("dia_high:%u\n"), pn->dia_high);
	info += s;
	s.Format(_T("dia_low:%u\n"), pn->dia_low);
	info += s;
	s.Format(_T("press:%u\n"), pn->press);
	info += s;
	s.Format(_T("measure:%u\n"), pn->measure);
	info += s;
	s.Format(_T("nibp_level:%u\n"), pn->nibp_level);
	info += s;
	s.Format(_T("unit:%u\n"), pn->unit);
	info += s;
	s.Format(_T("error:%u\n"), pn->error);
	info += s;
	s.Format(_T("mode:%u\n"), pn->mode);
	info += s;
	OutputDebugString(info);
	return info;
}

CString CStructTools::DumpTEMP(TEMP* pt)
{
	CString info = _T("\nDumpTEMP:\n");
	CString s;
	s.Format(_T("t1:%u\n"), pt->t1);
	info += s;
	s.Format(_T("t1_high:%u\n"), pt->t1_high);
	info += s;
	s.Format(_T("t1_low:%u\n"), pt->t1_low);
	info += s;
	s.Format(_T("t2:%u\n"), pt->t2);
	info += s;
	s.Format(_T("t2_high:%u\n"), pt->t2_high);
	info += s;
	s.Format(_T("t2_low:%u\n"), pt->t2_low);
	info += s;
	s.Format(_T("t_level:%u\n"), pt->t_level);
	info += s;
	s.Format(_T("unit:%u\n"), pt->unit);
	info += s;
	s.Format(_T("error:%u\n"), pt->error);
	info += s;
	OutputDebugString(info);
	return info;
}

CString CStructTools::DumpFETUS(FETUS* pf)
{
	int i;
	CString info = _T("\nDumpFETUS:\n");
	CString s;
	s.Format(_T("fetusmode:%u\n"), pf->fetusmode);
	info += s;
	s.Format(_T("fetusvolume:%u\n"), pf->fetusvolume);
	info += s;
	s.Format(_T("marked:%u\n"), pf->marked);
	info += s;
	s.Format(_T("ucreset:%u\n"), pf->ucreset);
	info += s;
	s.Format(_T("fhr1:\n"));
	info += s;
	for (i=0; i<4; i++) {
		s.Format(_T("[%d]:%u\n"), i, pf->fhr1[i]);
		info += s;
	}
	s.Format(_T("fhr2:\n"));
	info += s;
	for (i=0; i<4; i++) {
		s.Format(_T("[%d]:%u\n"), i, pf->fhr2[i]);
		info += s;
	}
	s.Format(_T("fhr_high:%u\n"), pf->fhr_high);
	info += s;
	s.Format(_T("fhr_low:%u\n"), pf->fhr_low);
	info += s;
	s.Format(_T("fhr_level:%u\n"), pf->fhr_level);
	info += s;
	s.Format(_T("uc:\n"));
	info += s;
	for (i=0; i<4; i++) {
		s.Format(_T("[%d]:%u\n"), i, pf->uc[i]);
		info += s;
	}
	s.Format(_T("uc_high:%u\n"), pf->uc_high);
	info += s;
	s.Format(_T("uc_low:%u\n"), pf->uc_low);
	info += s;
	s.Format(_T("uc_level:%u\n"), pf->uc_level);
	info += s;
	s.Format(_T("fm:%u\n"), pf->fm);
	info += s;
	s.Format(_T("error:%u\n"), pf->error);
	info += s;
	s.Format(_T("mark_info:%u\n"), pf->mark_info);
	info += s;
	OutputDebugString(info);
	return info;
}

//判断是不是空数据包
BOOL CStructTools::IsNullData(CLIENT_DATA* pcd)
{
	if (! pcd) {
		return TRUE;
	}
	if (! pcd->data_opt) {
		return TRUE;
	}
	
	return FALSE;
}

//抽值插值算法
void CompressData(BYTE *dest, BYTE *src, int destCount, int srcCount, float scaleY, int baseY)
{
	int i;
	BYTE maxVal = max(src[0], src[srcCount-1]);
	BYTE minVal = min(src[0], src[srcCount-1]);

	//为了曲线的连续
	dest[0] = src[0] * scaleY + baseY;
	dest[destCount-1] = (src[srcCount-1] - baseY) * scaleY;
    for (i=1; i<destCount - 1; i++) {
        double j = (double)i * srcCount / destCount;
        int t = (int)j;
        if (t < (srcCount - 1)) {
            dest[i] = (BYTE)(((src[t] * (1 - (j - t)) + src[t + 1] * (j - t)) - baseY) * scaleY);
        }
        else {
            dest[i] = (src[t] - baseY) * scaleY;
        }
    }
    int minPos = -1, maxPos = -1;
    for (i=0; i<srcCount; i++) {
        if (maxVal < src[i]) {
            maxVal = src[i];
            maxPos = i;
        }
        else if (minVal > src[i]) {
            minVal = src[i];
            minPos = i;
        }
    }
    if (minPos != -1) {
        dest[minPos * destCount / srcCount] = (src[minPos] - baseY) * scaleY;
    }
    if (maxPos != -1) {
        dest[maxPos * destCount / srcCount] = (src[maxPos] - baseY) * scaleY;
    }
}

