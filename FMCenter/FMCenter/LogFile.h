#pragma once
class CLogFile
{
private:
	const static int m_nSaveCount = 100; //每100条刷文件一次
	CFile m_fileLog;
	int m_nMsgCounter;

public:
	CLogFile(CString sFileName);
	virtual ~CLogFile(void);
	void Write(CString s);
};

