#include "stdafx.h"
#include "FMCenter.h"
#include "FMMonitorView.h"
#include "FMMonitorUnit.h"
#include "libzip.h"
#include "MonitorViewToolbar.h"


static _TCHAR* l_bmpFileName[MONITOR_BKGD_COUNT] = {
	BMP_A1,
	BMP_A2,
	BMP_A3,
	BMP_A4,
	BMP_A5,
	BMP_F1,
	BMP_F2,
	BMP_F3,
	BMP_F4,
	BMP_F5,
	BMP_FA
};

static _TCHAR* l_iniFileName[MONITOR_BKGD_COUNT] = {
	INI_A1,
	INI_A2,
	INI_A3,
	INI_A4,
	INI_A5,
	INI_F1,
	INI_F2,
	INI_F3,
	INI_F4,
	INI_F5,
	INI_FA
};

static _TCHAR* l_bmpNumName[MONITOR_FONT_COUNT] = {
	BMP_FONT_1,
	BMP_FONT_2,
	BMP_FONT_3,
	BMP_FONT_4,
	BMP_FONT_5,
	BMP_FONT_6,
	BMP_FONT_7,
	BMP_FONT_8,
	BMP_FONT_9,
	BMP_FONT_10,
	BMP_FONT_11,
	BMP_FONT_12,
	BMP_FONT_13,
	BMP_FONT_14,
	BMP_FONT_15,
	BMP_FONT_16,
	BMP_FONT_17,
	BMP_FONT_18,
	BMP_FONT_19,
	BMP_FONT_20,
	BMP_FONT_21,
	BMP_FONT_22
};

static _TCHAR* l_iniNumName[MONITOR_FONT_COUNT] = {
	INI_FONT_1,
	INI_FONT_2,
	INI_FONT_3,
	INI_FONT_4,
	INI_FONT_5,
	INI_FONT_6,
	INI_FONT_7,
	INI_FONT_8,
	INI_FONT_9,
	INI_FONT_10,
	INI_FONT_11,
	INI_FONT_12,
	INI_FONT_13,
	INI_FONT_14,
	INI_FONT_15,
	INI_FONT_16,
	INI_FONT_17,
	INI_FONT_18,
	INI_FONT_19,
	INI_FONT_20,
	INI_FONT_21,
	INI_FONT_22
};

IMPLEMENT_DYNCREATE(CFMMonitorView, CView)

BEGIN_MESSAGE_MAP(CFMMonitorView, CView)
	ON_WM_ERASEBKGND()
	ON_WM_RBUTTONDOWN()
	ON_WM_TIMER()
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONDBLCLK()
	ON_MESSAGE(WM_USER_LAYOUT_CHANGE, &CFMMonitorView::OnLayoutChange)
END_MESSAGE_MAP()

CFMMonitorView::CFMMonitorView(void)
{
	theApp.m_pMonitorView = this;

	m_pToolbar = NULL;
	m_bInitBkDC = FALSE;
	ZeroMemory(m_arBkDC, sizeof(m_arBkDC));
	ZeroMemory(m_arUnits, sizeof(m_arUnits));
	m_pMemDC = NULL;
	m_pDefaultBkDC = NULL;
	m_puFocus = NULL;
	m_nCurLayoutNum = -1;

	// 创建默认字体
	m_fontTimeRuler.CreateFont(16, 0, 0, 0, FW_SEMIBOLD, FALSE, FALSE, 0, ANSI_CHARSET,
		OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_SWISS, _T("宋体"));
	m_fontNumber.CreateFont(24, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET,
		OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_SWISS, _T("微软雅黑"));
	m_fontInfo.CreateFont(14, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, DEFAULT_CHARSET,
		OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_SWISS, _T("宋体"));
	m_pOldFont = NULL;

	m_nDecimusCounter = 0;
}

CFMMonitorView::~CFMMonitorView(void)
{
	int i;

	//释放监视单元
	for(i=0; i<MONITOR_UNIT_COUNT; i++) {
		delete m_arUnits[i];
		m_arUnits[i] = NULL;
	}

	//释放格子背景DC
	for (i=0; i<MONITOR_BKGD_COUNT; i++) {
		delete m_arBkDC[i];
		m_arBkDC[i] = NULL;
		delete m_arBkIni[i];
		m_arBkIni[i] = NULL;
	}

	//释放数字图片DC
	for (i=0; i<MONITOR_FONT_COUNT; i++) {
		delete m_arNumDC[i];
		m_arNumDC[i] = NULL;
		delete m_arNumIni[i];
		m_arNumIni[i] = NULL;
	}

	//释放MemDC
	if (m_pMemDC) {
		delete m_pMemDC;
		m_pMemDC = NULL;
	}
	if (m_pDefaultBkDC) {
		delete m_pDefaultBkDC;
		m_pDefaultBkDC = NULL;
	}

	if (m_pToolbar) {
		delete m_pToolbar;
		m_pToolbar = NULL;
	}

	m_pOldFont = NULL;
}

void CFMMonitorView::SelectTimeRulerFont()
{
	m_pOldFont = m_pMemDC->SelectObject(&m_fontTimeRuler);
}

void CFMMonitorView::SelectNumberFont()
{
	m_pOldFont = m_pMemDC->SelectObject(&m_fontNumber);
}

void CFMMonitorView::SelectInfoFont()
{
	m_pOldFont = m_pMemDC->SelectObject(&m_fontInfo);
}

void CFMMonitorView::ResetFont()
{
	if (m_pOldFont) {
		m_pMemDC->SelectObject(m_pOldFont);
	}
}

void CFMMonitorView::InitBkDCandMemDC(CDC* pDC)
{
	int i;
	//初始化存放资源的DC
	for (i=0; i<MONITOR_BKGD_COUNT; i++) {
		CDC* pBkImgDC = new CDC();
		pBkImgDC->CreateCompatibleDC(pDC);
		m_arBkDC[i] = pBkImgDC;
	}
	for (i=0; i<MONITOR_FONT_COUNT; i++) {
		CDC* pNumberDC = new CDC();
		pNumberDC->CreateCompatibleDC(pDC);
		m_arNumDC[i] = pNumberDC;
	}

	//初始化MemDC
	m_pMemDC = new CDC();
	m_pMemDC->CreateCompatibleDC(pDC);
	CBitmap bmpMemDC;
	bmpMemDC.CreateCompatibleBitmap(pDC,
		MAIN_AREA_W, MAIN_AREA_H);
	::DeleteObject(m_pMemDC->SelectObject(&bmpMemDC));

	//初始化所需资源图片
	LoadResource();

	//初始化默认背景图
	m_pDefaultBkDC = new CDC();
	m_pDefaultBkDC->CreateCompatibleDC(pDC);
	CBitmap bmpBkDC;
	bmpBkDC.CreateCompatibleBitmap(pDC,
		MAIN_AREA_W, MAIN_AREA_H);
	::DeleteObject (m_pDefaultBkDC->SelectObject(&bmpBkDC));
	m_pDefaultBkDC->BitBlt(0, 0, MAIN_AREA_W, MAIN_AREA_H, m_arBkDC[INDEX_FA], 0, 0, SRCCOPY);

	CBitmap bmpLogo;
	bmpLogo.LoadBitmap(IDB_SPLASH);
    CDC dcImage;
    dcImage.CreateCompatibleDC(pDC);
    BITMAP bm;
    bmpLogo.GetBitmap(&bm);
    CBitmap* pOldBitmap = dcImage.SelectObject(&bmpLogo);
	int logo_x = (MAIN_AREA_W - bm.bmWidth) / 2;
	int logo_y = (MAIN_AREA_H - bm.bmHeight) / 2;
    m_pDefaultBkDC->BitBlt(logo_x, logo_y, bm.bmWidth, bm.bmHeight, &dcImage, 0, 0, SRCCOPY);
	dcImage.SelectObject(pOldBitmap);
	//把整体颜色稍微调暗，否则看上去不舒服
	int j;
	for (j=0; j<MAIN_AREA_H; j++) {
		for (i=0; i<MAIN_AREA_W; i++) {
			COLORREF cr = m_pDefaultBkDC->GetPixel(i, j);
			BYTE r = GetRValue(cr) / 2;
			BYTE g = GetGValue(cr) / 2;
			BYTE b = GetBValue(cr) / 2;
			m_pDefaultBkDC->SetPixel(i, j, RGB(r, g, b));
		}
	}
}

void CFMMonitorView::LoadResource()
{
	int i;

	//加载背景图片资源文件
	LPWSTR szBkgroundName = MAKEINTRESOURCE(LOWORD(IDR_BKGROUND));
	LPWSTR szBkgroundType = _T("Binary");
	HRSRC hRes = FindResource(NULL, szBkgroundName, szBkgroundType);
	HGLOBAL hResData;
	if (! hRes || !(hResData = ::LoadResource(NULL, hRes))) {
		MessageBox(_T("背景图片资源 加载失败！"));
		return;
	}

	int len = SizeofResource(NULL, hRes);
	PBYTE pSrc = (PBYTE)LockResource(hResData);
	if (! pSrc) {
		MessageBox(_T("背景图片资源 锁定失败！"));
		return;
	}

	BYTE* buffer = new BYTE[len];
	CopyMemory(buffer, pSrc, len);
	FreeResource(hResData);

	libzip zip;
	if (! zip.load(buffer, len)) {
		MessageBox(_T("背景图片资源 解压缩失败！"));
		return;
	}

	//初始化背景图片
	for (i=0; i<MONITOR_BKGD_COUNT; i++) {

		int size = zip.getItemSize(l_bmpFileName[i]);

		LPVOID pvData = NULL;
		HGLOBAL hGlobal = GlobalAlloc(GMEM_MOVEABLE, size);
		pvData = GlobalLock(hGlobal);
		zip.loadItem(l_bmpFileName[i], (BYTE*)pvData, size);

		LPSTREAM pstm = NULL;
		HRESULT hr = CreateStreamOnHGlobal(hGlobal, TRUE, &pstm);

		LPPICTURE gpPicture;
		hr = ::OleLoadPicture(pstm, size, FALSE, IID_IPicture, (LPVOID *)&gpPicture);
		pstm->Release();

		OLE_HANDLE m_picHandle;
		gpPicture->get_Handle(&m_picHandle);
		CGdiObject gobj;
		gobj.Attach((HGDIOBJ)m_picHandle);

		GlobalUnlock(hGlobal);
		GlobalFree(hGlobal);
		::DeleteObject (m_arBkDC[i]->SelectObject(&gobj));
	}

	//初始化配置信息
	for (i=0; i<MONITOR_BKGD_COUNT; i++) {
		int size = zip.getItemSize(l_iniFileName[i]);

		BYTE* pbData = new BYTE[size + 2];
		zip.loadItem(l_iniFileName[i], pbData, size + 2);
		pbData[size] = 0x0;
		pbData[size + 1] = 0x0;

		CString sIniContent = (_TCHAR*)(pbData + 2);//跳过文件开头的FF FE
		m_arBkIni[i] = new CIniInt(sIniContent);

		delete[] pbData;
	}

	//初始化数字图片
	for (i=0; i<MONITOR_FONT_COUNT; i++) {

		int size = zip.getItemSize(l_bmpNumName[i]);

		LPVOID pvData = NULL;
		HGLOBAL hGlobal = GlobalAlloc(GMEM_MOVEABLE, size);
		pvData = GlobalLock(hGlobal);
		zip.loadItem(l_bmpNumName[i], (BYTE*)pvData, size);

		LPSTREAM pstm = NULL;
		HRESULT hr = CreateStreamOnHGlobal(hGlobal, TRUE, &pstm);

		LPPICTURE gpPicture;
		hr = ::OleLoadPicture(pstm, size, FALSE, IID_IPicture, (LPVOID *)&gpPicture);
		pstm->Release();

		OLE_HANDLE m_picHandle;
		gpPicture->get_Handle(&m_picHandle);
		CGdiObject gobj;
		gobj.Attach((HGDIOBJ)m_picHandle);

		GlobalUnlock(hGlobal);
		GlobalFree(hGlobal);
		::DeleteObject (m_arNumDC[i]->SelectObject(&gobj));
	}

	//初始化字体配置信息
	for (i=0; i<MONITOR_FONT_COUNT; i++) {
		int size = zip.getItemSize(l_iniNumName[i]);

		BYTE* pbData = new BYTE[size + 2];
		zip.loadItem(l_iniNumName[i], pbData, size + 2);
		pbData[size] = 0x0;
		pbData[size + 1] = 0x0;

		CString sIniContent = (_TCHAR*)(pbData + 2);//跳过文件开头的FF FE
		m_arNumIni[i] = new CIniInt(sIniContent);

		delete[] pbData;
	}
}

void CFMMonitorView::DrawNumber(CDC* pDC, int x, int y, CString sValue, int nType, int nSubType)
{
	int i, priW, priH, subW, subH, length, n;
	BOOL bSub = FALSE;
	if (nType <= 0) {
		return;
	}
	if (nSubType <= 0) {//不指定SubType，则两部分采用同一字体绘制
		nSubType = nType;
	}
	nType -= 1;
	nSubType -= 1;//把type转化为index

	if (nType >= MONITOR_FONT_COUNT ||
		nSubType >= MONITOR_FONT_COUNT) {
		theApp.WriteLog(_T("CFMMonitorView::DrawNumber参数nType或nSubType无效！\n"));
		CString info;
		info.Format(_T("nType=%d; nSubType=%d\n"), nType, nSubType);
		theApp.WriteLog(info);
		return;
	}

	CDC* pNumDC = m_arNumDC[nType];
	CDC* pSubDC = m_arNumDC[nSubType];
	priW = m_arNumIni[nType]->Get(_T("W"));
	priH = m_arNumIni[nType]->Get(_T("H"));
	subW = m_arNumIni[nSubType]->Get(_T("W"));
	subH = m_arNumIni[nSubType]->Get(_T("H"));

	length = sValue.GetLength();
	for (i=0; i<length; i++) {
		_TCHAR tc = sValue.GetAt(i);
		switch (tc) {
		case _T(' '): n = -1; break;//' '是占位符，绘制纯黑色块
		case _T('0'): n =  0; break;
		case _T('1'): n =  1; break;
		case _T('2'): n =  2; break;
		case _T('3'): n =  3; break;
		case _T('4'): n =  4; break;
		case _T('5'): n =  5; break;
		case _T('6'): n =  6; break;
		case _T('7'): n =  7; break;
		case _T('8'): n =  8; break;
		case _T('9'): n =  9; break;
		case _T('-'): n = 10; break;
		case _T('.'): n = 11; bSub = TRUE; y += (priH - subH); break;
		default:
			CString err;
			err.Format(_T("CFMMonitorView::DrawNumber遇到不支持的参数:%s！\n"), sValue);
			theApp.WriteLog(err);
			return;
		}

		if (bSub) {
			//绘制nSubType样式的数字
			if (-1 == n) {
				//占位符，绘制纯黑色块
				pDC->BitBlt(x, y, subW, subH, pSubDC, 0, 0, BLACKNESS);
				x += subW;
			}
			else if(11 == n) {
				//小数点宽度减半
				pDC->BitBlt(x, y, subW / 2, subH, pSubDC, subW * n, 0, SRCCOPY);
				x += (subW / 2);
			}
			else {
				pDC->BitBlt(x, y, subW, subH, pSubDC, subW * n, 0, SRCCOPY);
				x += subW;
			}
		}
		else {
			//绘制nType样式的数字
			if (-1 == n) {
				//占位符，绘制纯黑色块
				pDC->BitBlt(x, y, priW, priH, pNumDC, 0, 0, BLACKNESS);
				x += priW;
			}
			else if(11 == n) {
				//小数点宽度减半
				pDC->BitBlt(x, y, priW / 2, priH, pNumDC, priW * n, 0, SRCCOPY);
				x += (priW / 2);
			}
			else {
				pDC->BitBlt(x, y, priW, priH, pNumDC, priW * n, 0, SRCCOPY);
				x += priW;
			}
		}
	}
}

void CFMMonitorView::CloseTimer()
{
	KillTimer(REDRAW_TIMER_ID);
}

void CFMMonitorView::OnDraw(CDC* pDC)
{
	if (!m_bInitBkDC) {
		InitBkDCandMemDC(pDC);
		InitMonitorUnit();
		SetTimer(REDRAW_TIMER_ID, REDRAW_TIMER_SPAN, NULL);
		m_bInitBkDC = TRUE;
		//资源初始化完成之后，把窗口挪动到最终位置
		//（初始化阶段，窗口可能被用户移动）
		theApp.PostInitBkMessage();
	}
	else {
		if (0 == m_nCurLayoutNum) {
			//绘制一床都没有情况下的默认背景图
			DrawDefaultBk(pDC);
		}
		else {
			//设置剪裁区，目的是避免小Toolbar闪烁。
			CRect rc;
			GetWindowRect(&rc);
			ScreenToClient(&rc);
			CRgn rgnClient;
			rgnClient.CreateRectRgnIndirect(&rc);
			rgnClient.CombineRgn(&rgnClient, &m_rgnToolBarMask, RGN_DIFF);
			pDC->SelectClipRgn(&rgnClient);

			//绘制缓冲区的内容
			int i;
			BOOL bFocus = FALSE;
			for (i=0; i<MONITOR_UNIT_COUNT; i++) {
				if (m_puFocus == m_arUnits[i]) {
					bFocus = TRUE;
					break;
				}
			}

			if (bFocus) {
				pDC->BitBlt(FRAME_WIDTH_FOCUS, FRAME_WIDTH_FOCUS,
					MAIN_AREA_W - 2 * FRAME_WIDTH_FOCUS, MAIN_AREA_H - 2 * FRAME_WIDTH_FOCUS,
					m_pMemDC, FRAME_WIDTH_FOCUS, FRAME_WIDTH_FOCUS, SRCCOPY);
				m_arUnits[i]->DrawFocus(pDC);
			}
			else {
				pDC->BitBlt(FRAME_WIDTH_NORMAL, FRAME_WIDTH_NORMAL,
					MAIN_AREA_W - 2 * FRAME_WIDTH_NORMAL, MAIN_AREA_H - 2 * FRAME_WIDTH_NORMAL,
					m_pMemDC, FRAME_WIDTH_NORMAL, FRAME_WIDTH_NORMAL, SRCCOPY);
				m_arUnits[i]->DrawFrame(pDC);
			}
		}
	}
}

BOOL CFMMonitorView::OnEraseBkgnd(CDC* pDC)
{
	return FALSE;
}

void CFMMonitorView::DrawDefaultBk(CDC* pDC)
{
	pDC->BitBlt(0, 0, MAIN_AREA_W, MAIN_AREA_H, m_pDefaultBkDC, 0, 0, SRCCOPY);
}

void CFMMonitorView::InitMonitorUnit()
{
	int i;
	for(i=0; i<MONITOR_UNIT_COUNT; i++) {
		if (! m_arUnits[i]) {
			m_arUnits[i] = new CFMMonitorUnit(this);
			m_arUnits[i]->SetType(MONITOR_TYPE_FETUS);
		}
	}

	Layout_0();
}

void CFMMonitorView::OnRButtonDown(UINT nFlags, CPoint point)
{
	if (! m_bInitBkDC) {
		return;
	}

/*
	CPoint pt(point);
	CMenu muTemp;
	CMenu* pmuPop;
	if (!muTemp.LoadMenu(IDR_MENU_POPUP)) {
		CString info;
		info.Format(_T("！！！创建右键菜单失败！"));
		OutputDebugString(info);
		goto fin;
	}
	pmuPop = muTemp.GetSubMenu(0);
	if (pmuPop) {
		this->ClientToScreen(&pt);
		pmuPop->TrackPopupMenu(TPM_LEFTALIGN, pt.x, pt.y, this);   
	}

fin:
*/
	CView::OnRButtonDown(nFlags, point);
}

void CFMMonitorView::OnLButtonDown(UINT nFlags, CPoint point)
{
	if (! m_bInitBkDC) {
		return;
	}

	int i;
	for(i=0; i<MONITOR_UNIT_COUNT; i++) {
		if (m_arUnits[i]->PtInArea(point)) {
			if (m_puFocus) {
				if (m_puFocus == m_arUnits[i]) {
					break;
				}
				m_puFocus->OnLoseFocus();
				theApp.SetFocusIndex(m_arUnits[i]->GetAliveIndex());
			}
			m_puFocus = m_arUnits[i];
			m_puFocus->OnGetFocus();
			break;
		}
	}

	CView::OnLButtonDown(nFlags, point);
}

void CFMMonitorView::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	int i;
	for(i=0; i<MONITOR_UNIT_COUNT; i++) {
		if (m_arUnits[i]->OnDbClick(point)) {
			break;
		}
	}

	CView::OnLButtonDblClk(nFlags, point);
}

void CFMMonitorView::Layout_0(void)
{
	if (0 == m_nCurLayoutNum) {
		return;
	}
	m_nCurLayoutNum = 0;
	int i;
	for (i=0; i<MONITOR_UNIT_COUNT; i++) {
		m_arUnits[i]->m_bVisible = FALSE;
	}
	m_pMemDC->BitBlt(0, 0, MAIN_AREA_W, MAIN_AREA_H, m_pDefaultBkDC, 0, 0, SRCCOPY);
	m_pToolbar->ShowWindow(SW_HIDE);
}

void CFMMonitorView::Layout_1(void)
{
	if (1 == m_nCurLayoutNum) {
		return;
	}
	m_nCurLayoutNum = 1;
	//|---------------|
	//|               |
	//|               |
	//|               |
	//|               |
	//|               |
	//|---------------|
	m_arUnits[0]->SetPos(0, 0, MAIN_AREA_W, MAIN_AREA_H, MONITOR_AREA_1);
	m_arUnits[1]->m_bVisible = FALSE;
	m_arUnits[2]->m_bVisible = FALSE;
	m_arUnits[3]->m_bVisible = FALSE;
	m_arUnits[4]->m_bVisible = FALSE;
	m_arUnits[5]->m_bVisible = FALSE;
	m_arUnits[6]->m_bVisible = FALSE;
	m_arUnits[7]->m_bVisible = FALSE;
}

void CFMMonitorView::Layout_2(void)
{
	if (2 == m_nCurLayoutNum) {
		return;
	}
	m_nCurLayoutNum = 2;
	//|-------|-------|
	//|       |       |
	//|       |       |
	//|       |       |
	//|       |       |
	//|       |       |
	//|-------|-------|
	int nHalfW = MAIN_AREA_W / 2;
	m_arUnits[0]->SetPos(0, 0, nHalfW, MAIN_AREA_H, MONITOR_AREA_2);
	m_arUnits[1]->SetPos(nHalfW, 0, nHalfW, MAIN_AREA_H, MONITOR_AREA_2);
	m_arUnits[2]->m_bVisible = FALSE;
	m_arUnits[3]->m_bVisible = FALSE;
	m_arUnits[4]->m_bVisible = FALSE;
	m_arUnits[5]->m_bVisible = FALSE;
	m_arUnits[6]->m_bVisible = FALSE;
	m_arUnits[7]->m_bVisible = FALSE;
}

void CFMMonitorView::Layout_3(void)
{
	if (3 == m_nCurLayoutNum) {
		return;
	}
	m_nCurLayoutNum = 3;
	//|-------|-------|
	//|       |       |
	//|       |       |
	//|-------|       |
	//|       |       |
	//|       |       |
	//|-------|-------|
	int nHalfW = MAIN_AREA_W / 2;
	int nHalfH = MAIN_AREA_H / 2;
	m_arUnits[0]->SetPos(0, 0, nHalfW, nHalfH, MONITOR_AREA_3);
	m_arUnits[1]->SetPos(0, nHalfH, nHalfW, nHalfH, MONITOR_AREA_3);
	m_arUnits[2]->SetPos(nHalfW, 0, nHalfW, MAIN_AREA_H, MONITOR_AREA_2);
	m_arUnits[3]->m_bVisible = FALSE;
	m_arUnits[4]->m_bVisible = FALSE;
	m_arUnits[5]->m_bVisible = FALSE;
	m_arUnits[6]->m_bVisible = FALSE;
	m_arUnits[7]->m_bVisible = FALSE;
}

void CFMMonitorView::Layout_4(void)
{
	if (4 == m_nCurLayoutNum) {
		return;
	}
	m_nCurLayoutNum = 4;
	//|-------|-------|
	//|       |       |
	//|       |       |
	//|-------|-------|
	//|       |       |
	//|       |       |
	//|-------|-------|
	int nHalfW = MAIN_AREA_W / 2;
	int nHalfH = MAIN_AREA_H / 2;
	m_arUnits[0]->SetPos(0, 0, nHalfW, nHalfH, MONITOR_AREA_3);
	m_arUnits[1]->SetPos(0, nHalfH, nHalfW, nHalfH, MONITOR_AREA_3);
	m_arUnits[2]->SetPos(nHalfW, 0, nHalfW, nHalfH, MONITOR_AREA_3);
	m_arUnits[3]->SetPos(nHalfW, nHalfH, nHalfW, nHalfH, MONITOR_AREA_3);
	m_arUnits[4]->m_bVisible = FALSE;
	m_arUnits[5]->m_bVisible = FALSE;
	m_arUnits[6]->m_bVisible = FALSE;
	m_arUnits[7]->m_bVisible = FALSE;
}

void CFMMonitorView::Layout_5(void)
{
	if (5 == m_nCurLayoutNum) {
		return;
	}
	m_nCurLayoutNum = 5;
	//|-------|-------|
	//|       |       |
	//|-------|       |
	//|       |-------|
	//|-------|       |
	//|       |       |
	//|-------|-------|
	int nHalfW = MAIN_AREA_W / 2;
	int nHalfH = MAIN_AREA_H / 2;
	int nTripH = MAIN_AREA_H / 3;
	m_arUnits[0]->SetPos(0, 0, nHalfW, nTripH, MONITOR_AREA_4);
	m_arUnits[1]->SetPos(0, nTripH, nHalfW, nTripH, MONITOR_AREA_4);
	m_arUnits[2]->SetPos(0, nTripH*2, nHalfW, nTripH, MONITOR_AREA_4);
	m_arUnits[3]->SetPos(nHalfW, 0, nHalfW, nHalfH, MONITOR_AREA_3);
	m_arUnits[4]->SetPos(nHalfW, nHalfH, nHalfW, nHalfH, MONITOR_AREA_3);
	m_arUnits[5]->m_bVisible = FALSE;
	m_arUnits[6]->m_bVisible = FALSE;
	m_arUnits[7]->m_bVisible = FALSE;
}

void CFMMonitorView::Layout_6(void)
{
	if (6 == m_nCurLayoutNum) {
		return;
	}
	m_nCurLayoutNum = 6;
	//|-------|-------|
	//|       |       |
	//|-------|-------|
	//|       |       |
	//|-------|-------|
	//|       |       |
	//|-------|-------|
	int nHalfW = MAIN_AREA_W / 2;
	int nTripH = MAIN_AREA_H / 3;
	m_arUnits[0]->SetPos(0, 0, nHalfW, nTripH, MONITOR_AREA_4);
	m_arUnits[1]->SetPos(0, nTripH, nHalfW, nTripH, MONITOR_AREA_4);
	m_arUnits[2]->SetPos(0, nTripH*2, nHalfW, nTripH, MONITOR_AREA_4);
	m_arUnits[3]->SetPos(nHalfW, 0, nHalfW, nTripH, MONITOR_AREA_4);
	m_arUnits[4]->SetPos(nHalfW, nTripH, nHalfW, nTripH, MONITOR_AREA_4);
	m_arUnits[5]->SetPos(nHalfW, nTripH*2, nHalfW, nTripH, MONITOR_AREA_4);
	m_arUnits[6]->m_bVisible = FALSE;
	m_arUnits[7]->m_bVisible = FALSE;
}

void CFMMonitorView::Layout_7(void)
{
	if (7 == m_nCurLayoutNum) {
		return;
	}
	m_nCurLayoutNum = 7;
	//|-------|-------|
	//|       |       |
	//|-------|       |
	//|       |-------|
	//|-------|       |
	//|       |-------|
	//|-------|       |
	//|       |       |
	//|-------|-------|
	int nHalfW = MAIN_AREA_W / 2;
	int nTripH = MAIN_AREA_H / 3;
	int nQuarterH = MAIN_AREA_H / 4;
	m_arUnits[0]->SetPos(0, 0, nHalfW, nQuarterH, MONITOR_AREA_5);
	m_arUnits[1]->SetPos(0, nQuarterH, nHalfW, nQuarterH, MONITOR_AREA_5);
	m_arUnits[2]->SetPos(0, nQuarterH*2, nHalfW, nQuarterH, MONITOR_AREA_5);
	m_arUnits[3]->SetPos(0, nQuarterH*3, nHalfW, nQuarterH, MONITOR_AREA_5);
	m_arUnits[4]->SetPos(nHalfW, 0, nHalfW, nTripH, MONITOR_AREA_4);
	m_arUnits[5]->SetPos(nHalfW, nTripH, nHalfW, nTripH, MONITOR_AREA_4);
	m_arUnits[6]->SetPos(nHalfW, nTripH*2, nHalfW, nTripH, MONITOR_AREA_4);
	m_arUnits[7]->m_bVisible = FALSE;
}

void CFMMonitorView::Layout_8(void)
{
	if (8 == m_nCurLayoutNum) {
		return;
	}
	m_nCurLayoutNum = 8;
	//|-------|-------|
	//|       |       |
	//|-------|-------|
	//|       |       |
	//|-------|-------|
	//|       |       |
	//|-------|-------|
	//|       |       |
	//|-------|-------|
	int nHalfW = MAIN_AREA_W / 2;
	int nQuarterH = MAIN_AREA_H / 4;
	m_arUnits[0]->SetPos(0, 0, nHalfW, nQuarterH, MONITOR_AREA_5);
	m_arUnits[1]->SetPos(0, nQuarterH, nHalfW, nQuarterH, MONITOR_AREA_5);
	m_arUnits[2]->SetPos(0, nQuarterH*2, nHalfW, nQuarterH, MONITOR_AREA_5);
	m_arUnits[3]->SetPos(0, nQuarterH*3, nHalfW, nQuarterH, MONITOR_AREA_5);
	m_arUnits[4]->SetPos(nHalfW, 0, nHalfW, nQuarterH, MONITOR_AREA_5);
	m_arUnits[5]->SetPos(nHalfW, nQuarterH, nHalfW, nQuarterH, MONITOR_AREA_5);
	m_arUnits[6]->SetPos(nHalfW, nQuarterH*2, nHalfW, nQuarterH, MONITOR_AREA_5);
	m_arUnits[7]->SetPos(nHalfW, nQuarterH*3, nHalfW, nQuarterH, MONITOR_AREA_5);
}

void CFMMonitorView::OnTimer(UINT_PTR nIDEvent)
{
	int i;
	for (i=0; i<MONITOR_UNIT_COUNT; i++) {

		if (0 == m_nDecimusCounter) {
			try {
				m_arUnits[i]->RenewData(m_pMemDC);
			}
			catch (CException* pe) {
				_TCHAR err[256];
				pe->GetErrorMessage(err, 255);
				CString s;
				s.Format(_T("CFMMonitorView::OnTimer中 RenewData 发生了异常：%s\n"), err);
				theApp.WriteLog(s);
				pe->Delete();
			}
		}

#ifdef DECIMUS_VERSION
		try {
			m_arUnits[i]->DrawWaveDecimus(m_pMemDC, m_nDecimusCounter);
		}
		catch (CException* pe) {
			_TCHAR err[256];
			pe->GetErrorMessage(err, 255);
			CString s;
			s.Format(_T("CFMMonitorView::OnTimer中 DrawWaveDecimus 发生了异常：%s\n"), err);
			theApp.WriteLog(s);
			pe->Delete();
		}
#endif
	}


#ifdef DECIMUS_VERSION
	m_nDecimusCounter ++;
	if (10 == m_nDecimusCounter) {
		m_nDecimusCounter = 0;
	}
#endif


	this->Invalidate();
	CView::OnTimer(nIDEvent);
}

int CFMMonitorView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;

	m_pToolbar = new CMonitorViewToolbar(this);
	m_pToolbar->ShowWindow(SW_HIDE);
	return 0;
}

void CFMMonitorView::OnDestroy()
{
	CView::OnDestroy();
	m_pToolbar->DestroyWindow();
}

LRESULT CFMMonitorView::OnLayoutChange(WPARAM wp, LPARAM lp)
{
	CUIntArray ar;
	int nCount = theApp.GetAliveIndexList(ar);
	int nCurIndex = theApp.GetFocusIndex();

	//断开界面与RecordUnit之间的联系
	int i;
	for (i=0; i<MONITOR_UNIT_COUNT; i++) {
		m_arUnits[i]->SetRecordUnit(-1);
		m_arUnits[i]->SetAliveIndex(-1);
	}

	//改变布局
	if (0 == nCount) {
		Layout_0();
		return 0L;
	}
	int nStartIndex = nCurIndex / MONITOR_UNIT_COUNT * MONITOR_UNIT_COUNT; //按页取整
	int count = min(8, nCount - nStartIndex);
	switch (count) {
	case 1: Layout_1(); break;
	case 2: Layout_2(); break;
	case 3: Layout_3(); break;
	case 4: Layout_4(); break;
	case 5: Layout_5(); break;
	case 6: Layout_6(); break;
	case 7: Layout_7(); break;
	case 8: Layout_8(); break;
	}

	//重新关联各个ViewUnit和RecordUnit之间的关系
	for (i=0; i<count; i++) {
		m_arUnits[i]->SetRecordUnit(ar[nStartIndex + i]);
		m_arUnits[i]->SetAliveIndex(nStartIndex + i);
	}

	//设置焦点
	CFMMonitorUnit* pNewFocus = m_arUnits[nCurIndex % 8];
	if (m_puFocus) {
		m_puFocus->OnLoseFocus();
	}
	m_puFocus = pNewFocus;
	m_puFocus->OnGetFocus();

	//刷新显示
	for (i=0; i<count; i++) {
		m_arUnits[i]->RenewBkConfig();
	}
	return 0L;
}

void CFMMonitorView::SetToolbarStatus(int x, int y, CString type)
{
	if (m_pToolbar) {
		m_pToolbar->SetStatus(x, y, type);
		CRect rc;
		m_pToolbar->GetWindowRect(&rc);
		ScreenToClient(&rc);
		m_rgnToolBarMask.DeleteObject();
		m_rgnToolBarMask.CreateRectRgnIndirect(&rc);
	}
}

void CFMMonitorView::OnSwitchType()
{
	if (m_puFocus) {
		m_puFocus->OnSwitchType();
		m_puFocus->RenewBkConfig();
	}
}

void CFMMonitorView::OnMedicalRecord()
{
	if (m_puFocus) {
		m_puFocus->OnMedicalRecord();
	}
}

