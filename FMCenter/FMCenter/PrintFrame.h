#pragma once
#include "afxwin.h"
#include "GuiTools.h"

class CPrintWrapperView;
class CPrintFrame :	public CFrameWnd
{
public:
	CPrintFrame(DRAWFUN pDraw, CWnd*pOldW, CWnd* pCallW, BOOL bDirect, LPCTSTR stTitle=NULL);
	virtual ~CPrintFrame(void);
	CWnd*   pOldWnd;
	CWnd*   pCallWnd;
	DRAWFUN Draw;
	BOOL    bDirectPrint;

protected:
	DECLARE_DYNAMIC(CPrintFrame)

public:
	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);
	//}}AFX_VIRTUAL
	// Implementation

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members
	CStatusBar  m_wndStatusBar;
	CToolBar    m_wndToolBar;
	CPrintWrapperView* m_pView;

// Generated message map functions
protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);   
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnClose();
};

