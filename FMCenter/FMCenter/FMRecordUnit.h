#pragma once

//存盘缓冲，只要Buffer Size 比Save Size大就可以了。
//Save Size决定了每个记录文件的最大尺寸。
#define RECORD_RELEASE_SIZE            (1200)
#define UNIT_DATA_INCREASE             (600)
#define RECORD_SAVE_SIZE               (600)

#include "FMDisplayDataBuffer.h"
class CFMDisplayDataBuffer;
class CFMRecordUnit
{
private:
	int m_nIndex;
	CDatabase& m_db;
	CCriticalSection& m_dbLock;
	
	BOOL m_bAlive;
	BOOL m_bFirst;
	CString m_sRecordID;

	//存盘事件
	HANDLE m_hSaveEvent;
	BOOL m_bWaitForSave;
	int m_nSavePos;             //保存位置相对于缓冲区的位置
	int m_nSaveCount;
	int m_nStartOffset;         //保存位置相对记录开始的位置，存盘用
	int m_nFileCount;           //已经保存了多少个文件
	//存盘缓冲
	CLIENT_DATA m_pcdSaveBuffer[RECORD_SAVE_SIZE];

	//接收数据锁
	CCriticalSection m_csRecvBufLock;
	//接收数据缓冲
	CArray<CLIENT_DATA, CLIENT_DATA&> m_arRecvBuf;
	CTime m_tmStart;

	//显示用的数据组
	CFMDisplayDataBuffer* m_pDisplayBuffer;

	//调试使用的接口
	CTime m_tmRecordStart;

	//病历号（默认为空字符串，病历关联操作之后被设置）
	CString m_sMedicalNum;

	//PatientInfo信息（从病例库中查出的信息）
	CString m_sPatientName;
	int m_nPatientGender;
	int m_nPatientType;
	int m_nPatientAge;

	CString m_sPregnantWeek; //孕周是监护记录的字段

public:
	int m_type;
	//调试使用的接口
	CLogFile* m_pLogFile;

	//与调试相关的函数
	void WriteLogString(CString sFunction, CString sContent);

private:
	void Reset();
	void ReleaseOutdatedBuffer();
	void GetNewRecordID();

public:
	CFMRecordUnit(int nIndex, HANDLE hEvent);
	virtual ~CFMRecordUnit(void);

	inline BOOL IsFirstUse() { if (m_bFirst) { m_bFirst = FALSE; return TRUE; } return FALSE; };
	inline BOOL IsAlive() { return m_bAlive; };
	inline int GetIndex() { return m_nIndex; };
	inline int GetBedNum() { return m_nIndex + 1; };
	inline CTime GetStartTime() { return m_tmStart; };
	inline CString GetRecordID() { return m_sRecordID; };
	BOOL SetMedicalNum(CWnd* pWnd, CString& sMedNum);

	void UpdatePatientInfo(CString sMedicalNum);
	inline void SetPregnantWeek(CString sPregnantWeek) { m_sPregnantWeek = sPregnantWeek; }
	inline BOOL GetPatientInfo(CString& name, int& gender, int& type, int& age) {
		if (m_sMedicalNum.IsEmpty()) {
			return FALSE;
		}
		name = m_sPatientName;
		gender = m_nPatientGender;
		type = m_nPatientType;
		age = m_nPatientAge;
		return TRUE;
	};
	inline CString GetPregnantWeek(void) {
		return m_sPregnantWeek;
	}

	BOOL Start();
	void Stop();

	BOOL SocketAdd(CLIENT_DATA& cd);

	//触发保存过程，并进行必要的处理
	BOOL TriggerSaveProcess();
	//执行将数据保存为文件的操作
	void SaveUnitProcess();

	int GetDisplayLenOfSecond();
	//获取指定时间段内的显示数据
	DISPLAY_DATA* GetNumbData(int nTimeSpan);
	DISPLAY_DATA* GetLatestNumbData();
	void Autodiagnostics(void);
	int GetWaveData(int nTimeSpan,
		BYTE** ppECG1, BYTE** ppECG2, BYTE** ppECGv, BYTE** ppSPO2, BYTE** ppRESP);
	int GetFHRWaveData(int nTimeSpan,
		WORD** ppFHR1, WORD** ppFHR2, WORD** ppTOCO, BYTE** ppFMMARK, BYTE** ppTOCOReset, DISPLAY_FLAG** ppDiagno1, DISPLAY_FLAG** ppDiagno2);
};
