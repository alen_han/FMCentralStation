
// MainFrm.h : CMainFrame 类的接口
//

#pragma once

class CFMSplitterWnd : public CSplitterWnd
{
   DECLARE_DYNCREATE(CFMSplitterWnd)
protected:
    CSplitterWnd    m_wndSplitter;
public:
	CFMSplitterWnd() {};

protected:
	afx_msg LRESULT OnNcHitTest(CPoint point) { return HTNOWHERE; };

public:
	virtual ~CFMSplitterWnd() {};

    DECLARE_MESSAGE_MAP()
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
};

class CMainFrame : public CMDIFrameWnd
{
	DECLARE_DYNAMIC(CMainFrame)
public:
	CMainFrame();
	void Close();

// 特性
protected:
	CFMSplitterWnd m_wndSplitter;
public:

// 操作
public:

// 重写
public:
	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

// 实现
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif


// 生成的消息映射函数
protected:
	DECLARE_MESSAGE_MAP()

public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg LRESULT OnSocketChange(WPARAM wp, LPARAM lp);
	afx_msg LRESULT OnInitBkFinish(WPARAM wp, LPARAM lp);
	afx_msg LRESULT OnNcHitTest(CPoint point);
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnClose();
};


