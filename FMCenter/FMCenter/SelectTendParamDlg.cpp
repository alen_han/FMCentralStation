// SelectTendParamDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "FMCenter.h"
#include "SelectTendParamDlg.h"
#include "afxdialogex.h"
#include "ReviewDlg.h"

// CSelectTendParamDlg 对话框

IMPLEMENT_DYNAMIC(CSelectTendParamDlg, CDialogEx)

CSelectTendParamDlg::CSelectTendParamDlg(CReviewDlg* pDlg, CRect rc)
	: CDialogEx(CSelectTendParamDlg::IDD, pDlg)
	, m_pReviewDlg(pDlg)
	, m_rcButton(rc)
{
}

CSelectTendParamDlg::~CSelectTendParamDlg()
{
}

void CSelectTendParamDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CHK_TEND_PRINT_HR, m_chkHR);
	DDX_Control(pDX, IDC_CHK_TEND_PRINT_SYS, m_chkSYS);
	DDX_Control(pDX, IDC_CHK_TEND_PRINT_MEA, m_chkMEA);
	DDX_Control(pDX, IDC_CHK_TEND_PRINT_DIA, m_chkDIA);
	DDX_Control(pDX, IDC_CHK_TEND_PRINT_RR, m_chkRR);
	DDX_Control(pDX, IDC_CHK_TEND_PRINT_SPO2, m_chkSPO2);
	DDX_Control(pDX, IDC_CHK_TEND_PRINT_T1, m_chkT1);
	DDX_Control(pDX, IDC_CHK_TEND_PRINT_T2, m_chkT2);
}

BEGIN_MESSAGE_MAP(CSelectTendParamDlg, CDialogEx)
	ON_BN_CLICKED(IDCANCEL, &CSelectTendParamDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDOK, &CSelectTendParamDlg::OnBnClickedOk)
END_MESSAGE_MAP()

BOOL CSelectTendParamDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_chkHR.SetCheck(m_pReviewDlg->m_bPrintHR);
	m_chkSYS.SetCheck(m_pReviewDlg->m_bPrintSYS);
	m_chkMEA.SetCheck(m_pReviewDlg->m_bPrintMEA);
	m_chkDIA.SetCheck(m_pReviewDlg->m_bPrintDIA);
	m_chkRR.SetCheck(m_pReviewDlg->m_bPrintRR);
	m_chkSPO2.SetCheck(m_pReviewDlg->m_bPrintSPO2);
	m_chkT1.SetCheck(m_pReviewDlg->m_bPrintT1);
	m_chkT2.SetCheck(m_pReviewDlg->m_bPrintT2);

	CRect rc;
	GetWindowRect(&rc);
	CRect rcNew(m_rcButton.left,
		m_rcButton.top - rc.Height(),
		m_rcButton.left + rc.Width(),
		m_rcButton.top);
	MoveWindow(&rcNew);
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

// CSelectTendParamDlg 消息处理程序
void CSelectTendParamDlg::OnBnClickedCancel()
{
	CDialogEx::OnCancel();
}

void CSelectTendParamDlg::OnBnClickedOk()
{
	m_pReviewDlg->m_bPrintHR   = m_chkHR.GetCheck();
	m_pReviewDlg->m_bPrintSYS  = m_chkSYS.GetCheck();
	m_pReviewDlg->m_bPrintMEA  = m_chkMEA.GetCheck();
	m_pReviewDlg->m_bPrintDIA  = m_chkDIA.GetCheck();
	m_pReviewDlg->m_bPrintRR   = m_chkRR.GetCheck();
	m_pReviewDlg->m_bPrintSPO2 = m_chkSPO2.GetCheck();
	m_pReviewDlg->m_bPrintT1   = m_chkT1.GetCheck();
	m_pReviewDlg->m_bPrintT2   = m_chkT2.GetCheck();
	CDialogEx::OnOK();
}
