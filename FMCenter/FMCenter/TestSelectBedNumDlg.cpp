// TestSelectBedNumDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "FMCenter.h"
#include "TestSelectBedNumDlg.h"
#include "TestSysMetricsDlg.h"
#include "FMToolbarForm.h"
#include "afxdialogex.h"


// CTestSelectBedNumDlg 对话框

IMPLEMENT_DYNAMIC(CTestSelectBedNumDlg, CDialogEx)

CTestSelectBedNumDlg::CTestSelectBedNumDlg(CFMToolbarForm* pParent, int nType)
	: CDialogEx(CTestSelectBedNumDlg::IDD, pParent)
	, m_pParent(pParent)
	, m_nType(nType)
{

}

CTestSelectBedNumDlg::~CTestSelectBedNumDlg()
{
}

void CTestSelectBedNumDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO_BED_NUM, m_cmbBedNum);
}


BEGIN_MESSAGE_MAP(CTestSelectBedNumDlg, CDialogEx)
	ON_BN_CLICKED(IDCANCEL, &CTestSelectBedNumDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDOK, &CTestSelectBedNumDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CTestSelectBedNumDlg 消息处理程序
void CTestSelectBedNumDlg::OnBnClickedCancel()
{
	// TODO: 在此添加控件通知处理程序代码
	CDialogEx::OnCancel();
}

BOOL CTestSelectBedNumDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	int i, count;
	CUIntArray ar;

	switch (m_nType) {

	case TYPE_OP_SIM_ADD:
		SetWindowText(_T("添加床旁机"));
		count = m_pParent->GetSleepIndexList(ar);
		m_cmbBedNum.ResetContent();
		for (i=0 ;i<count; i++) {
			int nBedNum = ar[i];
			CString sBedNum;
			sBedNum.Format(_T("%02d"), nBedNum + 1);
			int index = m_cmbBedNum.AddString(sBedNum);
			m_cmbBedNum.SetItemData(index, (DWORD_PTR)nBedNum);
		}
		break;

	case TYPE_OP_SIM_DEL:
		SetWindowText(_T("删除床旁机"));
		count = m_pParent->GetAliveIndexList(ar);
		m_cmbBedNum.ResetContent();
		for (i=0 ;i<count; i++) {
			int nBedNum = ar[i];
			CString sBedNum;
			sBedNum.Format(_T("%02d"), nBedNum + 1);
			int index = m_cmbBedNum.AddString(sBedNum);
			m_cmbBedNum.SetItemData(index, (DWORD_PTR)nBedNum);
		}
		break;
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void CTestSelectBedNumDlg::OnBnClickedOk()
{
	int nSel = m_cmbBedNum.GetCurSel();
	if (CB_ERR == nSel) {
		MessageBox(_T("请选择床号！"));
		return;
	}

	m_nSelectedBedIndex = (int)m_cmbBedNum.GetItemData(nSel);
	CDialogEx::OnOK();
}


