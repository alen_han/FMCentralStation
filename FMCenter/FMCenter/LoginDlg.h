#pragma once
#include "afxwin.h"


// CLoginDlg 对话框
typedef struct _ADMIN_INFO {
	int id;
	int nType;
	CString sName;
	CString sPass;
} ADMIN_INFO;

class CLoginDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CLoginDlg)

public:
	CLoginDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CLoginDlg();

// 对话框数据
	enum { IDD = IDD_LOGIN };

private:
	CDatabase& m_db;
	CCriticalSection& m_dbLock;
	int m_nAdminCount;
	ADMIN_INFO* m_arAdminGroup;

private:
	void QueryAdminInfo();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	CEdit m_edtLoginName;
	CEdit m_edtLoginPass;
};
