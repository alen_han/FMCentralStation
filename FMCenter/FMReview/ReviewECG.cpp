#include "stdafx.h"
#include "ReviewECG.h"
#include "ReviewDlg.h"

static const COLORREF cr_info = RGB(255,255,225);
static const COLORREF cr_ecg1 = RGB(0,255,0);
static const COLORREF cr_ecg2 = RGB(0,255,255);
static const COLORREF cr_bkgd = RGB(32,18,0);
static const COLORREF cr_dark_gray = RGB(32,32,32);
static const COLORREF cr_light_gray = RGB(64,64,64);

CReviewECG::CReviewECG(CReviewDlg* pdlg)
	: CReviewTool(pdlg)
{
	m_fontEcgInfo.CreateFont(12, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, DEFAULT_CHARSET,
		OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_SWISS, _T("宋体"));
	m_pOldFont = NULL;
}

CReviewECG::~CReviewECG(void)
{
	m_pOldFont = NULL;
}

void CReviewECG::SelectEcgInfoFont(CDC* pdc)
{
	m_pOldFont = pdc->SelectObject(&m_fontEcgInfo);
}

void CReviewECG::ResetFont(CDC* pdc)
{
	if (m_pOldFont) {
		pdc->SelectObject(m_pOldFont);
	}
}

int CReviewECG::GetPageSpan()
{
	CRect rc = m_pReviewDlg->GetTableRect();
	switch (m_pReviewDlg->GetZoomScale()) {
	case REVIEW_ZOOM_SCALE100: return (rc.Width() / 250);
	case REVIEW_ZOOM_SCALE50:  return (rc.Width() / 125);
	case REVIEW_ZOOM_SCALE20:  return (rc.Width() / 50);
	case REVIEW_ZOOM_SCALE10:  return (rc.Width() / 25);
	}
	return 1;
}

void CReviewECG::EnterReview()
{
	m_pReviewDlg->m_cmbSelectChannel.ResetContent();
	m_pReviewDlg->m_cmbSelectChannel.AddString(_T("通道1"));
	m_pReviewDlg->m_cmbSelectChannel.AddString(_T("通道2"));
	m_pReviewDlg->m_cmbSelectChannel.SetCurSel(0);
	m_pReviewDlg->m_cmbSelectChannel.ShowWindow(SW_SHOW);
	m_pReviewDlg->m_btnZoomIn.ShowWindow(SW_SHOW);
	m_pReviewDlg->m_btnZoomOut.ShowWindow(SW_SHOW);
	m_pReviewDlg->m_edtZoomInfo.ShowWindow(SW_SHOW);
}

void CReviewECG::LeaveReview()
{
	m_pReviewDlg->m_cmbSelectChannel.ShowWindow(SW_HIDE);
	m_pReviewDlg->m_btnZoomIn.ShowWindow(SW_HIDE);
	m_pReviewDlg->m_btnZoomOut.ShowWindow(SW_HIDE);
	m_pReviewDlg->m_edtZoomInfo.ShowWindow(SW_HIDE);
}

void CReviewECG::Draw(CDC* pdc)
{
	m_pReviewDlg->InitBkgd(pdc, cr_bkgd);
	CRect rc = m_pReviewDlg->GetTableRect();
	rc.OffsetRect(-rc.left, -rc.top);
	switch (m_pReviewDlg->GetZoomScale()) {
	case REVIEW_ZOOM_SCALE100:
		DrawGrid100(pdc, rc);
		break;
	case REVIEW_ZOOM_SCALE50:
		DrawGrid50(pdc, rc);
		break;
	case REVIEW_ZOOM_SCALE20:
	case REVIEW_ZOOM_SCALE10:
		//太密集，不画格子了
		break;
	}

	//绘制两个导连之间的分隔条
	CBrush br((COLORREF)0);
	CRect rc2(rc.left, rc.top + rc.Height() / 2 - 1 , rc.right, rc.top + rc.Height() / 2 + 2);
	pdc->FillRect(&rc2, &br);

	DrawTime(pdc, rc);
	DrawWave(pdc, rc);
}

void CReviewECG::DrawGrid100(CDC* pdc, CRect& rc)
{
	const int grid_gap = 4;

	CPen darkGray(PS_SOLID, 1, cr_dark_gray);
	CPen lightGray(PS_SOLID, 1, cr_light_gray);
	CPen* pOldPen = pdc->SelectObject(&darkGray);

	int x, y;
	for (x=rc.left; x<rc.right; x+= grid_gap) {
		if (4 != (x % 5)) {
			pdc->MoveTo(x, rc.top);
			pdc->LineTo(x, rc.bottom);
		}
	}
	for (y=rc.top; y<rc.bottom; y+= grid_gap) {
		if (4 != (y % 5)) {
			pdc->MoveTo(rc.left, y);
			pdc->LineTo(rc.right, y);
		}
	}

	pdc->SelectObject(&lightGray);
	for (x=rc.left; x<rc.right; x+= grid_gap) {
		if (4 == (x % 5)) {
			pdc->MoveTo(x, rc.top);
			pdc->LineTo(x, rc.bottom);
		}
	}
	for (y=rc.top; y<rc.bottom; y+= grid_gap) {
		if (4 == (y % 5)) {
			pdc->MoveTo(rc.left, y);
			pdc->LineTo(rc.right, y);
		}
	}

	pdc->SelectObject(pOldPen);
}

void CReviewECG::DrawGrid50(CDC* pdc, CRect& rc)
{
	const int grid_gap = 2;

	CPen darkGray(PS_SOLID, 1, cr_dark_gray);
	CPen* pOldPen = pdc->SelectObject(&darkGray);

	int x, y;
	/*
	for (x=rc.left; x<rc.right; x+= grid_gap) {
		for (y=rc.top; y<rc.bottom; y+= grid_gap) {
			if (4 != (x % 5) && 4 != (y % 5)) {
				pdc->SetPixel(x, y, darkGray);
			}
		}
	}
	*/

	for (x=rc.left; x<rc.right; x+= grid_gap) {
		if (4 == (x % 5)) {
			pdc->MoveTo(x, rc.top);
			pdc->LineTo(x, rc.bottom);
		}
	}
	for (y=rc.top; y<rc.bottom; y+= grid_gap) {
		if (4 == (y % 5)) {
			pdc->MoveTo(rc.left, y);
			pdc->LineTo(rc.right, y);
		}
	}

	pdc->SelectObject(pOldPen);
}

void CReviewECG::DrawTime(CDC* pdc, CRect& rc)
{
	int nValidLen;
	CTime tmTableStart = m_pReviewDlg->GetCurrentDataTime(&nValidLen);
	if (tmTableStart == 0) {
		return;
	}

	CString sTime = tmTableStart.Format(_T("%Y-%m-%d %H:%M:%S"));
	CString sInfo;
	sInfo.Format(_T("记录时间：%s"), sTime);
	SelectEcgInfoFont(pdc);
	pdc->SetBkMode(TRANSPARENT);
	pdc->SetTextColor(cr_info);
	pdc->TextOut(rc.left + 10, rc.bottom - 16, sInfo);
	ResetFont(pdc);
}

void CReviewECG::DrawWave(CDC* pdc, CRect& rc)
{
	int nZoomScale = m_pReviewDlg->GetZoomScale();
	int nValidLen;
	CLIENT_DATA* pData = m_pReviewDlg->GetCurrentBufferPointer(&nValidLen);
	if (! pData) {
		return;
	}

	int limitH = rc.Height() / 4;
	int nDataPerSecond;
	switch (nZoomScale) {
	case REVIEW_ZOOM_SCALE100: nDataPerSecond = ECG_WAVE_DATA_PER_SECOND; break;
	case REVIEW_ZOOM_SCALE50:  nDataPerSecond = ECG_WAVE_DATA_PER_SECOND / 2; break;
	case REVIEW_ZOOM_SCALE20:  nDataPerSecond = ECG_WAVE_DATA_PER_SECOND / 5; break;
	case REVIEW_ZOOM_SCALE10:  nDataPerSecond = ECG_WAVE_DATA_PER_SECOND / 10; break;
	}
	int nDispSeconds = (rc.Width() + nDataPerSecond) / nDataPerSecond;
	int nDispCount = min(nValidLen, nDispSeconds);
	int i;
	BYTE data1[ECG_WAVE_DATA_PER_SECOND + 1];
	BYTE data2[ECG_WAVE_DATA_PER_SECOND + 1];
	WORD data1Lead;
	WORD data2Lead;
	for (i=0; i<nDispCount; i++) {
		ECG* pe;
		RESP* pr;
		SPO2* ps;
		NIBP* pn;
		TEMP* pt;
		FETUS* pf;
		CStructTools::ParseClientData(&(pData[i]), &pe, &pr, &ps, &pn, &pt, &pf);

		int x;
		switch (nZoomScale) {
		case REVIEW_ZOOM_SCALE100: x = rc.left + ECG_WAVE_DATA_PER_SECOND * i; break;
		case REVIEW_ZOOM_SCALE50:  x = rc.left + ECG_WAVE_DATA_PER_SECOND / 2 * i; break;
		case REVIEW_ZOOM_SCALE20:  x = rc.left + ECG_WAVE_DATA_PER_SECOND / 5 * i; break;
		case REVIEW_ZOOM_SCALE10:  x = rc.left + ECG_WAVE_DATA_PER_SECOND / 10 * i; break;
		}
		int y1 = rc.top + rc.Height() / 4;
		int y2 = rc.top + rc.Height() / 4 * 3;
		int limitW = rc.right - x;

		if (0 == i) {
			data1[0] = pe->ecg1data[0];
			data2[0] = pe->ecg2data[0];
			data1Lead = pe->ecg_lead1;
			data2Lead = pe->ecg_lead2;
			DrawLeadInfo(pdc, x, rc.top, rc.Height() / 2, data1Lead);
			DrawLeadInfo(pdc, x, rc.top + rc.Height() / 2, rc.Height() / 2, data2Lead);
		}
		else {
			data1[0] = data1[ECG_WAVE_DATA_PER_SECOND];
			data2[0] = data2[ECG_WAVE_DATA_PER_SECOND];
			if (data1Lead != pe->ecg_lead1) {
				data1Lead = pe->ecg_lead1;
				DrawLeadInfo(pdc, x, rc.top, rc.Height() / 2, data1Lead);
			}
			if (data2Lead != pe->ecg_lead2) {
				data2Lead = pe->ecg_lead2;
				DrawLeadInfo(pdc, x, rc.top + rc.Height() / 2, rc.Height() / 2, data2Lead);
			}
		}
		memcpy(&(data1[1]), pe->ecg1data, ECG_WAVE_DATA_PER_SECOND);
		memcpy(&(data2[1]), pe->ecg2data, ECG_WAVE_DATA_PER_SECOND);
		switch (nZoomScale) {
		case REVIEW_ZOOM_SCALE100:
			DrawECGWave(pdc, cr_ecg1, x, y1, data1, limitW, limitH);
			DrawECGWave(pdc, cr_ecg2, x, y2, data2, limitW, limitH);
			break;
		case REVIEW_ZOOM_SCALE50:
			DrawECGWave50(pdc, cr_ecg1, x, y1, data1, limitW, limitH);
			DrawECGWave50(pdc, cr_ecg2, x, y2, data2, limitW, limitH);
			break;
		case REVIEW_ZOOM_SCALE20:
			DrawECGWave20(pdc, cr_ecg1, x, y1, data1, limitW, limitH);
			DrawECGWave20(pdc, cr_ecg2, x, y2, data2, limitW, limitH);
			break;
		case REVIEW_ZOOM_SCALE10:
			DrawECGWave10(pdc, cr_ecg1, x, y1, data1, limitW, limitH);
			DrawECGWave10(pdc, cr_ecg2, x, y2, data2, limitW, limitH);
			break;
		}
	}
}

void CReviewECG::DrawLeadInfo(CDC* pdc, int x, int y, int h, int lead)
{
	CPen linePen(PS_SOLID, 1, cr_info);
	CPen* pOldPen = pdc->SelectObject(&linePen);
	pdc->MoveTo(x + 1, y + 10);
	pdc->LineTo(x + 1, y + h / 2 + 10);
	pdc->SelectObject(pOldPen);

	SelectEcgInfoFont(pdc);
	pdc->SetBkMode(TRANSPARENT);
	pdc->SetTextColor(cr_info);
	pdc->TextOut(x + 5, y + 10, CFMDict::ECGLead_itos(lead));
	ResetFont(pdc);
}