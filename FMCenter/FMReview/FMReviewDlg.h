
// FMReviewDlg.h : 头文件
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"

class CRecordGroup
{
private:
	CString m_sFilePrefix;
	CString m_sDatetime;
public:
	int m_nBedNum;
	CStringArray m_saFullPathGroup;
	inline BOOL SetFilePrefix(CString sFileName) {
		if (sFileName.GetLength() < 19) {
			return FALSE;
		}

		m_sFilePrefix = sFileName.Left(19);

		CString s;
		int nYear, nMonth, nDate, nHour, nMin, nSec;
		_stscanf(m_sFilePrefix, _T("%04d%02d%02d-%02d%02d%02d(%02d-"), 
			&nYear, &nMonth, &nDate, &nHour, &nMin, &nSec, &m_nBedNum);
		m_sDatetime.Format(_T("%04d/%02d/%02d-%02d:%02d:%02d"), 
			nYear, nMonth, nDate, nHour, nMin, nSec);
		return TRUE;
	};
	inline CString GetDatetimeString() { return m_sDatetime; };
	inline BOOL CheckFilePrefix(CString sFileName) {
		CString sPrefix = sFileName.Left(19);
		return (m_sFilePrefix == sPrefix);
	}
};

// CFMReviewDlg 对话框
class CFMReviewDlg : public CDialogEx
{
// 构造
public:
	CFMReviewDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_FMREVIEW_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持

// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CEdit m_edtFolderPath;
	CListBox m_lstFileList;
	CListCtrl m_lstRecordList;
	afx_msg void OnBnClickedBtnSelectFolder();
	afx_msg void OnBnClickedBtnScanFolder();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();

private:
	CRecordGroup* m_pCurrentRecordGroup;
	CString m_sFileFolder;
	void ResetRecordList(void);
	void AddToRecordList(CString sFileName, CString& sFullPath);
	void ScanFolder(void);

public:
	CStringArray* GetPathGroup() {
		if (! m_pCurrentRecordGroup) {
			return NULL;
		}
		return &(m_pCurrentRecordGroup->m_saFullPathGroup);
	};
	virtual BOOL DestroyWindow();
};
