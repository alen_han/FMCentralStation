#include "stdafx.h"
#include "FMReview.h"
#include "PrintPreView.h"
#include "PrintWrapperView.h"


BOOL CALLBACK _AfxMyPreviewCloseProc(CFrameWnd* pFrameWnd);

/////////////////////////////////////////////////////////////////////////////
// CPrintWrapperView
IMPLEMENT_DYNCREATE(CPrintWrapperView, CScrollView)

CPrintWrapperView::CPrintWrapperView()
{
	m_pFrameWnd = NULL;
	m_nMapMode = MM_LOMETRIC; //修改为0.1mm为单位的模式
}

CPrintWrapperView::~CPrintWrapperView()
{
}

BEGIN_MESSAGE_MAP(CPrintWrapperView, CScrollView)
	//{{AFX_MSG_MAP(CPrintWrapperView)
	// NOTE - the ClassWizard will add and remove mapping macros here.    
	ON_COMMAND(ID_FILE_PRINT, OnFilePrint)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CPrintWrapperView::OnDraw(CDC *pDC)
{
}

void CPrintWrapperView::OnPrint(CDC* pDC, CPrintInfo* pInfo)
{
	if (pDC) {
		CPrintFrame* pf = (CPrintFrame*)theApp.m_pMainWnd;
		(*pf->Draw)(pDC, pInfo, (void*)pf->pCallWnd);
	}
}

/////////////////////////////////////////////////////////////////////////////
// CPrintWrapperView diagnostics

#ifdef _DEBUG
void CPrintWrapperView::AssertValid() const
{
	CView::AssertValid();
}

void CPrintWrapperView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CPrintWrapperView message handlers

BOOL CALLBACK _AfxMyPreviewCloseProc(CFrameWnd* pFrameWnd)
{
	ASSERT_VALID(pFrameWnd);

	CPrintPreviewView* pView = (CPrintPreviewView*) pFrameWnd->GetDlgItem(AFX_IDW_PANE_FIRST);
	ASSERT_KINDOF(CPrintPreviewView, pView);

	pView->OnPreviewClose();
	return FALSE;
}

BOOL CPrintWrapperView::OnPreparePrinting(CPrintInfo* pInfo) 
{
	//设置纸张为横向打印
	LPDEVMODE lpDevMode;
	PRINTDLG pd = pInfo->m_pPD->m_pd;
	if (AfxGetApp()->GetPrinterDeviceDefaults(&pd)) {
		lpDevMode = (LPDEVMODE)GlobalLock(pd.hDevMode);
		if (lpDevMode) {
			lpDevMode->dmPaperSize = DMPAPER_A4;
			lpDevMode->dmOrientation = DMORIENT_LANDSCAPE;//横向打印
		}
		GlobalUnlock(pd.hDevMode);
	}

	// default preparation
	return DoPreparePrinting(pInfo);
}

void CPrintWrapperView::OnFilePrint()
{
	CPrintFrame* pf = (CPrintFrame*)theApp.m_pMainWnd;
	if (pf->bDirectPrint) {
		pf->ShowWindow(SW_HIDE);
	}
	CView::OnFilePrint();    
	if (pf->bDirectPrint) {
		theApp.m_pMainWnd = pf->pOldWnd;
		pf->DestroyWindow();    
	}
}

void CPrintWrapperView::OnFilePrintPreview(CFrameWnd *pFrame)
{    
	m_pFrameWnd = pFrame;

	CPrintPreviewState* pState = new CPrintPreviewState;
	pState->lpfnCloseProc = _AfxMyPreviewCloseProc;

	if (! DoPrintPreview(AFX_IDD_PREVIEW_TOOLBAR,
		this, RUNTIME_CLASS(CPrintPreviewView), pState)) {        

		TRACE0("Error: DoPrintPreview failed.\n");
		AfxMessageBox(AFX_IDP_COMMAND_FAILURE);
		delete pState;
	}
}

BOOL CPrintWrapperView::DoPrintPreview(UINT nIDResource, CView* pPrintView, CRuntimeClass* pPreviewViewClass, CPrintPreviewState* pState)
{
	ASSERT_VALID_IDR(nIDResource);
	ASSERT_VALID(pPrintView);
	ASSERT(pPreviewViewClass != NULL);
	ASSERT(pPreviewViewClass->IsDerivedFrom(RUNTIME_CLASS(CScrollView)));
	ASSERT(pState != NULL);

	ASSERT(m_pFrameWnd != NULL);
	CFrameWnd* pParent = m_pFrameWnd; //STATIC_DOWNCAST(CFrameWnd, AfxGetMainWnd()); //
	ASSERT_VALID(pParent);

	CCreateContext context;
	context.m_pCurrentFrame = pParent;
	context.m_pCurrentDoc = GetDocument();
	context.m_pLastView = this;

	// Create the preview view object
	CPrintPreviewView* pView = (CPrintPreviewView*)pPreviewViewClass->CreateObject();
	if (pView == NULL) {
		TRACE0("Error: Failed to create preview view.\n");
		return FALSE;
	}
	ASSERT_KINDOF(CScrollView, pView);

	pView->m_pPreviewState = pState;        // save pointer

	pParent->OnSetPreviewMode(TRUE, pState);    // Take over Frame Window

	//Create the toolbar from the dialog resource
	pView->m_pToolBar = new CDialogBar;
	if (!pView->m_pToolBar->Create(pParent, MAKEINTRESOURCE(nIDResource),
		CBRS_TOP, AFX_IDW_PREVIEW_BAR)) {

		TRACE0("Error: Preview could not create toolbar dialog.\n");
		pParent->OnSetPreviewMode(FALSE, pState);   // restore Frame Window
		delete pView->m_pToolBar;       // not autodestruct yet
		pView->m_pToolBar = NULL;
		pView->m_pPreviewState = NULL;  // do not delete state structure
		delete pView;
		return FALSE;
	}
	pView->m_pToolBar->m_bAutoDelete = TRUE;    // automatic cleanup

	if (!pView->Create(NULL, NULL, AFX_WS_DEFAULT_VIEW,
		CRect(0,0,0,0), pParent, AFX_IDW_PANE_FIRST, &context)) {

		TRACE0("Error: couldn't create preview view for frame.\n");
		pParent->OnSetPreviewMode(FALSE, pState);   // restore Frame Window
		pView->m_pPreviewState = NULL;  // do not delete state structure
		delete pView;
		return FALSE;
	}

	pState->pViewActiveOld = pParent->GetActiveView();
	CPrintWrapperView* pActiveView = (CPrintWrapperView*)pParent->GetActiveFrame()->GetActiveView();
	if (pActiveView != NULL) {
		pActiveView->OnActivateView(FALSE, pActiveView, pActiveView);
	}

	if (!pView->SetPrintView(pPrintView)) {
		pView->OnPreviewClose();
		return TRUE;            // signal that OnEndPrintPreview was called
	}

	pParent->SetActiveView(pView);  // set active view - even for MDI

	// update toolbar and redraw everything
	pView->m_pToolBar->SendMessage(WM_IDLEUPDATECMDUI, (WPARAM)TRUE);
	pParent->RecalcLayout();            // position and size everything
	pParent->UpdateWindow();

	return TRUE;
}

// OnEndPrintPreview is here for swap tuning reasons
//  (see viewprev.cpp for complete preview mode implementation)
void CPrintWrapperView::OnEndPrintPreview(CDC* pDC, CPrintInfo* pInfo,POINT, CPrintPreviewView* pView)
{
	ASSERT_VALID(pDC);
	ASSERT_VALID(pView);
	ASSERT(m_pFrameWnd != NULL);

	if (pView->m_pPrintView != NULL) {
		OnEndPrinting(pDC, pInfo);
	}

	CFrameWnd* pParent = m_pFrameWnd;
	ASSERT_VALID(pParent);

	// restore the old main window
	pParent->OnSetPreviewMode(FALSE, pView->m_pPreviewState);

	// Force active view back to old one
	pParent->SetActiveView(pView->m_pPreviewState->pViewActiveOld);
	if (pParent != GetParentFrame()) {
		OnActivateView(TRUE, this, this);   // re-activate view in real frame
	}
	pView->DestroyWindow();     // destroy preview view
	// C++ object will be deleted in PostNcDestroy

	// restore main frame layout and idle message
	pParent->RecalcLayout();
	pParent->SendMessage(WM_SETMESSAGESTRING, (WPARAM)AFX_IDS_IDLEMESSAGE, 0L);
	pParent->UpdateWindow();
}

void CPrintWrapperView::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	CView::OnEndPrinting(pDC, pInfo);
}

/////////////////////////////////////////////////////////////////////

