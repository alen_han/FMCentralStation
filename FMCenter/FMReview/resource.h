//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 FMReview.rc 使用
//
#define ID_BTN_ECG_WAVEFORM             3
#define ID_BTN_FHR_WAVEFORM             4
#define ID_BTN_TEND_TABLE               5
#define ID_BTN_TEND_WAVEFORM            6
#define ID_BTN_PRINT                    7
#define ID_BTN_SELECT_PARAM             8
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_FMREVIEW_DIALOG             102
#define IDR_MAINFRAME                   128
#define IDD_DIALOG_REVIEW               336
#define IDD_SELEDT_TEND_PARAM           337
#define IDC_BTN_SELECT_FOLDER           1000
#define IDC_EDT_FOLDER                  1001
#define IDC_BTN_SCAN_FOLDER             1002
#define IDC_LST_FILES                   1003
#define IDC_LST_RECORDS                 1004
#define IDC_EDIT_START_TIME             1005
#define IDC_EDIT_TIME_SPAN              1010
#define IDC_EDIT_PATIENT_NAME           1016
#define IDC_EDIT_ZOOM_INFO              1020
#define IDC_COMBO_SELECT_FORMAT         1032
#define IDC_COMBO_DOCTOR_DIAGNO         1033
#define IDC_COMBO_SELECT_NIBP_UNIT      1036
#define IDC_COMBO_SELECT_TEMP_UNIT      1039
#define IDC_COMBO_SELECT_CHANNAL        1042
#define IDC_CHK_TEND_PRINT_HR           1048
#define IDC_CHK_TEND_PRINT_SYS          1053
#define IDC_CHK_TEND_PRINT_MEA          1058
#define IDC_CHK_TEND_PRINT_DIA          1061
#define IDC_CHK_TEND_PRINT_RR           1065
#define IDC_CHK_TEND_PRINT_SPO2         1068
#define IDC_CHK_TEND_PRINT_T1           1071
#define IDC_CHK_TEND_PRINT_T2           1073
#define IDC_BTN_ZOOM_OUT                1079
#define IDC_BTN_ZOOM_IN                 1084
#define IDC_SCROLLBAR                   1216
#define IDC_STATIC_TABLE_AREA           1218
#define IDC_STATIC_SELECT_FORMAT        1219
#define IDC_STATIC_SELECT_NIBP_UNIT     1220
#define IDC_STATIC_SELECT_TEMP_UNIT     1221
#define IDC_STATIC_DOCTOR_DIAGNO        1222

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1005
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
