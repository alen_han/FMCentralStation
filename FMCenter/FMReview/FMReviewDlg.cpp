
// FMReviewDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "FMReview.h"
#include "FMReviewDlg.h"
#include "afxdialogex.h"
#include "ReviewDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define MEDICAL_LIST_COLUMN_COUNT                (3)

//column上面显示的文字
static _TCHAR* l_arColumnLabel[MEDICAL_LIST_COLUMN_COUNT] =
{
	_T("床号"),
	_T("监护时间"),
	_T("文件数")
};
//column上文字的显示方式（靠左）
static int l_arColumnFmt[MEDICAL_LIST_COLUMN_COUNT] = 
{
	LVCFMT_LEFT,
	LVCFMT_LEFT,
	LVCFMT_LEFT
};
//column的宽度
static int l_arColumnWidth[MEDICAL_LIST_COLUMN_COUNT] = 
{
	40,
	140,
	60
};

// 用于应用程序“关于”菜单项的 CAboutDlg 对话框
class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CFMReviewDlg 对话框
CFMReviewDlg::CFMReviewDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CFMReviewDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_pCurrentRecordGroup = NULL;
}

void CFMReviewDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDT_FOLDER, m_edtFolderPath);
	DDX_Control(pDX, IDC_LST_FILES, m_lstFileList);
	DDX_Control(pDX, IDC_LST_RECORDS, m_lstRecordList);
}

BEGIN_MESSAGE_MAP(CFMReviewDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BTN_SELECT_FOLDER, &CFMReviewDlg::OnBnClickedBtnSelectFolder)
	ON_BN_CLICKED(IDC_BTN_SCAN_FOLDER, &CFMReviewDlg::OnBnClickedBtnScanFolder)
	ON_BN_CLICKED(IDOK, &CFMReviewDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CFMReviewDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


// CFMReviewDlg 消息处理程序

BOOL CFMReviewDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// 初始化LIST的表头
	int i;
	m_lstRecordList.RemoveAllGroups();
	m_lstRecordList.ModifyStyle(0, LVS_REPORT | LVS_SHOWSELALWAYS, 0);
	m_lstRecordList.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	//设定一个用于存取column的结构lvc
	LVCOLUMN lvc;
	//设定存取模式
	lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
	//用InSertColumn函数向窗口中插入柱
	for (i=0 ; i<MEDICAL_LIST_COLUMN_COUNT; i++) {
		lvc.iSubItem = i;
		lvc.pszText = l_arColumnLabel[i];
		lvc.cx = l_arColumnWidth[i];
		lvc.fmt = l_arColumnFmt[i];
		m_lstRecordList.InsertColumn(i,&lvc);
	}

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CFMReviewDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CFMReviewDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标显示。
HCURSOR CFMReviewDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CFMReviewDlg::OnBnClickedBtnSelectFolder()
{
	TCHAR szPath[MAX_PATH];     //存放选择的目录路径   
	ZeroMemory(szPath, sizeof(szPath));

	BROWSEINFO bi;
	bi.hwndOwner = m_hWnd;
	bi.pidlRoot = NULL;
	bi.pszDisplayName = szPath;
	bi.lpszTitle = _T("请选择数据目录：");
	bi.ulFlags = 0;
	bi.lpfn = NULL;
	bi.lParam = 0;
	bi.iImage = 0; 
	LPITEMIDLIST lp = SHBrowseForFolder(&bi);     

	if (lp && SHGetPathFromIDList(lp, szPath)) {
		//CString str;
		//str.Format(_T("选择的目录为 %s"), szPath);
		//MessageBox(str);
		m_sFileFolder.Format(_T("%s"), szPath);
		m_edtFolderPath.SetWindowText(m_sFileFolder);
		OnBnClickedBtnScanFolder();
	}
	else {
		CString str;
		str.Format(_T("无效的目录，请重新选择！"));
		MessageBox(str);
		m_sFileFolder = _T("");
	}
}

void CFMReviewDlg::OnBnClickedBtnScanFolder()
{
	m_edtFolderPath.GetWindowText(m_sFileFolder);
	ScanFolder();
}

void CFMReviewDlg::ResetRecordList(void)
{
	int i;
	int count = m_lstRecordList.GetItemCount();
	for (i=0; i<count; i++) {
		DWORD_PTR pd = m_lstRecordList.GetItemData(i);
		delete (CRecordGroup*)pd;
	}
	m_lstRecordList.DeleteAllItems();
}

void CFMReviewDlg::AddToRecordList(CString sFileName, CString& sFullPath)
{
	int i;
	int count = m_lstRecordList.GetItemCount();
	for (i=0; i<count; i++) {
		CRecordGroup* pd = (CRecordGroup*)m_lstRecordList.GetItemData(i);
		if (pd->CheckFilePrefix(sFileName)) {
			//匹配，加进去
			pd->m_saFullPathGroup.Add(sFullPath);
			CString sFileCount;
			sFileCount.Format(_T("%d"), pd->m_saFullPathGroup.GetCount());
			m_lstRecordList.SetItemText(i, 2, sFileCount);
			return;
		}
	}
	//没有匹配的，新建，加入列表
	CRecordGroup* prg = new CRecordGroup();
	prg->SetFilePrefix(sFileName);
	prg->m_saFullPathGroup.Add(sFullPath);

	LVITEM lvi;
	_TCHAR buffer[256];
	lvi.mask = LVIF_TEXT;
	lvi.iItem = i;
	lvi.pszText = buffer;
	lvi.cchTextMax = 255;

	CString sBedNum;
	sBedNum.Format(_T("%02d"), prg->m_nBedNum);
	CString sDatetime = prg->GetDatetimeString();

	lvi.iSubItem = 0;
	_tcsncpy(lvi.pszText, sBedNum.GetBuffer(0), sBedNum.GetLength() + 1);
	m_lstRecordList.InsertItem(&lvi);

	lvi.iSubItem = 1;
	_tcsncpy(lvi.pszText, sDatetime.GetBuffer(0), sDatetime.GetLength() + 1);
	m_lstRecordList.SetItem(&lvi);

	lvi.iSubItem = 2;
	_tcsncpy(lvi.pszText, _T("1"), 2);
	m_lstRecordList.SetItem(&lvi);

	m_lstRecordList.SetItemData(lvi.iItem, (DWORD_PTR)prg);
}

void CFMReviewDlg::ScanFolder(void)
{
	ResetRecordList();
	m_lstFileList.ResetContent();
	CString sFileFilter = m_sFileFolder + _T("\\*.rcd");
	CFileFind finder;
	BOOL bWorking = finder.FindFile(sFileFilter);
	while (bWorking) {
	   bWorking = finder.FindNextFile();
	   CString sFileName = finder.GetFileName();
	   m_lstFileList.AddString(sFileName);
	   CString sFullPath = m_sFileFolder + _T("\\") + sFileName;
	   AddToRecordList(sFileName, sFullPath);
	}
}

void CFMReviewDlg::OnBnClickedOk()
{
	int nSelectedItem = m_lstRecordList.GetSelectionMark();
	if (-1 == nSelectedItem) {
		return;
	}
	m_pCurrentRecordGroup = (CRecordGroup*)m_lstRecordList.GetItemData(nSelectedItem);

	CString sMRID;
	int nType = REVIEW_ECG_WAVEFORM;
	CTime tmNow = CTime::GetCurrentTime();
	CString sStartTime = tmNow.Format(_T("%Y-%m-%d %H:%M:%S"));
	CString sBedNum;
	CString sMedicalNum;
	CString sParentName;
	CString sPatientGender;
	CString sPatientType;
	CString sPatientAge;
	CString sPregnantWeek;
	CReviewDlg dlg(this, sMRID, nType, sStartTime, sBedNum, sMedicalNum,
		sParentName, sPatientGender, sPatientType, sPatientAge, sPregnantWeek);

	dlg.DoModal();
}

void CFMReviewDlg::OnBnClickedCancel()
{
	CDialogEx::OnCancel();
}


BOOL CFMReviewDlg::DestroyWindow()
{
	ResetRecordList();
	return CDialogEx::DestroyWindow();
}
