#include "stdafx.h"
#include "FMReview.h"
#include "PrintFHR.h"
#include "FHRAutodiagnostics.h"

static const int lft_margin = 200;
static const int top_margin = 150;

static const int table_top = 40;
static const int grid_w = 40;    //1格10秒钟
static const int grid_cols = 60; //10分钟
static const float time_scale = 4.0f;

static const int fhr_grid_line = 21;
static const int f20_grid_h = 20;
static const int f10_grid_h = 40;
static const float f20_scale = 2.0f;
static const float f10_scale = 4.0f;
static const int f20_table_h = f20_grid_h * fhr_grid_line;
static const int f10_table_h = f10_grid_h * fhr_grid_line;
static const int fhr_max = 240;
static const int fhr_min = 30;

static const int toco_grid_line = 10;
static const int t20_grid_h = 20;
static const int t10_grid_h = 40;
static const float t20_scale = 2.0f;
static const float t10_scale = 4.0f;
static const int t20_table_h = t20_grid_h * toco_grid_line;
static const int t10_table_h = t10_grid_h * toco_grid_line;
static const int toco_max = 100;

static const int separator_h = 40;
static const int diagnosis_h = 300;
static const int time_f10_h = 60;
static const int time_f20_h = 40;
static const int text_scale_h = 30;
static const int half_scale_h = (text_scale_h / 2);
static const int table_w = grid_w * grid_cols;
static const int table_h = (f20_table_h + time_f20_h + t20_table_h + separator_h) * 2;
static const int scale_w = 60;
static const int f10_mark_h = 100;
static const int f20_mark_h = 50;

static const COLORREF cr_fhr_normal = RGB(192,255,225);
static const COLORREF cr_grid_light = RGB(192,192,255);
static const COLORREF cr_grid_dark = RGB(128,128,255);
static const COLORREF cr_minite = RGB(220,92,128);
static const COLORREF cr_scale = RGB(0,0,128);
static const COLORREF cr_info = RGB(0,0,0);
static const COLORREF cr_wave = RGB(0,0,0);
static const COLORREF cr_mark = RGB(128,0,0);

#define DATA_PAPER_LEN       (600)

CPrintFHR::CPrintFHR(CReviewDlg* pdlg)
	: CPrintTool(pdlg)
{
	m_pAuto = new CFHRAutodiagnostics();
	m_fontTableScale.CreateFont(32, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, DEFAULT_CHARSET,
		OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_SWISS, _T("宋体"));
	m_fontFhrInfo.CreateFont(36, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, DEFAULT_CHARSET,
		OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_SWISS, _T("宋体"));
	m_fontDoctorSign.CreateFont(36, 0, 0, 0, FW_BOLD, FALSE, FALSE, 0, DEFAULT_CHARSET,
		OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_SWISS, _T("宋体"));
	m_pOldFont = NULL;
	m_type = 0;
}

CPrintFHR::~CPrintFHR(void)
{
	m_pOldFont = NULL;
	if (m_pAuto) {
		delete m_pAuto;
		m_pAuto = NULL;
	}
}

void CPrintFHR::SelectScaleFont(CDC* pdc)
{
	m_pOldFont = pdc->SelectObject(&m_fontTableScale);
}

void CPrintFHR::SelectInfoFont(CDC* pdc)
{
	m_pOldFont = pdc->SelectObject(&m_fontFhrInfo);
}

void CPrintFHR::SelectDoctorSignFont(CDC* pdc)
{
	m_pOldFont = pdc->SelectObject(&m_fontDoctorSign);
}

void CPrintFHR::ResetFont(CDC* pdc)
{
	if (m_pOldFont) {
		pdc->SelectObject(m_pOldFont);
	}
}

void CPrintFHR::InitSetup(int type)
{
	m_type = type;
	//获得自动诊断结果
	Autodiagnostics();
	//获得并保存医生诊断信息
	m_nDoctorDiagno = theApp.m_nDoctorDiagno;
}

void CPrintFHR::Print(CDC* pdc,  CPrintInfo* pInfo)
{
	//在MM_LOMETRIC映射模式下，Y坐标是反向的
	CRect rc(lft_margin, - top_margin,
		lft_margin + scale_w + table_w,
		- top_margin - table_top - table_h - diagnosis_h);
	
	m_nMoveCount = 0;
	pdc->SetBkMode(TRANSPARENT);
	pdc->SetTextColor((COLORREF)0);
	SelectScaleFont(pdc);

	if (0 == m_type) {
		DrawGridF10(pdc, rc);
		DrawTimeF10(pdc, rc);
		DrawWaveF10(pdc, rc);
	}
	else {
		DrawGridF20(pdc, rc);
		DrawTimeF20(pdc, rc);
		DrawWaveF20(pdc, rc);
	}
	SelectInfoFont(pdc);
	DrawInfo(pdc, rc);
	DrawReport(pdc, rc);
	ResetFont(pdc);
}

void CPrintFHR::DrawGridF10(CDC* pdc, CRect& rc)
{
	int i;
	CBrush brNormalArea(cr_fhr_normal);
	CRect rcNormalArea(rc.left + scale_w, rc.top - table_top - f10_grid_h * 8,
		rc.left + scale_w + table_w, rc.top - table_top - f10_grid_h * 12);
	pdc->FillRect(rcNormalArea, &brNormalArea);

	CPen penRed(PS_SOLID, 1, cr_minite);
	CPen penDark(PS_SOLID, 1, cr_grid_dark);
	CPen penLight(PS_SOLID, 1, cr_grid_light);
	CPen* pOldPen = pdc->SelectObject(&penLight);

	int nNibpUnit = m_pReviewDlg->GetNIBPUnitOpt();
	int nTempUnit = m_pReviewDlg->GetTEMPUnitOpt();
	int nChannel = m_pReviewDlg->m_cmbSelectChannel.GetCurSel();

	//胎心率表格
	int x = rc.left + scale_w;
	int y = rc.top - table_top;
	int w = table_w;
	int h = f10_table_h;
	pdc->SetTextColor(cr_scale);
	for (i=0; i<=w; i+= grid_w) {
		if (0 == i % (grid_w * 6)) {
			pdc->SelectObject(&penRed);
		}
		else {
			pdc->SelectObject(&penLight);
		}
		pdc->MoveTo(x + i, y);
		pdc->LineTo(x + i, y - h);
	}
	for (i=0; i<=h; i+= f10_grid_h) {
		if (0 == i % (f10_grid_h * 3)) {
			CString s;
			s.Format(_T("%3d"), (int)(fhr_max - i / f10_scale));
			pdc->TextOut(x - scale_w, y - i + half_scale_h, s);
			pdc->SelectObject(&penDark);
		}
		else {
			pdc->SelectObject(&penLight);
		}
		pdc->MoveTo(x, y - i);
		pdc->LineTo(x + table_w, y - i);
	}
	pdc->SetTextColor(cr_info);
	pdc->TextOut(x + 20, y - half_scale_h, (0 == nChannel) ? _T("FHR 1") : _T("FHR 2"));

	//宫缩压表格
	y = y - h - time_f10_h;
	h = t10_table_h;
	pdc->SetTextColor(cr_scale);
	for (i=0; i<=w; i+= grid_w) {
		if (0 == i % (grid_w * 6)) {
			pdc->SelectObject(&penRed);
		}
		else {
			pdc->SelectObject(&penLight);
		}
		pdc->MoveTo(x + i, y);
		pdc->LineTo(x + i, y - h);
	}
	for (i=0; i<=h; i+= t10_grid_h) {
		if (0 == i % (t10_grid_h * 2)) {
			CString s = itoNibpString(nNibpUnit, (int)(toco_max - i / t10_scale));
			pdc->TextOut(x - scale_w, y - i + half_scale_h, s);
			pdc->SelectObject(&penDark);
		}
		else {
			pdc->SelectObject(&penLight);
		}
		pdc->MoveTo(x, y - i);
		pdc->LineTo(x + table_w, y - i);
	}
}

void CPrintFHR::DrawGridF20(CDC* pdc, CRect& rc)
{
	int i;
	CBrush brNormalArea(cr_fhr_normal);
	CRect rcNormalArea(rc.left + scale_w, rc.top - table_top - f20_grid_h * 8,
		rc.left + scale_w + table_w, rc.top - table_top - f20_grid_h * 12);
	pdc->FillRect(rcNormalArea, &brNormalArea);
	rcNormalArea.OffsetRect(0, - f20_table_h - time_f20_h - t20_table_h - separator_h);
	pdc->FillRect(rcNormalArea, &brNormalArea);

	CPen penRed(PS_SOLID, 1, cr_minite);
	CPen penDark(PS_SOLID, 1, cr_grid_dark);
	CPen penLight(PS_SOLID, 1, cr_grid_light);
	CPen* pOldPen = pdc->SelectObject(&penLight);

	int nNibpUnit = m_pReviewDlg->GetNIBPUnitOpt();
	int nTempUnit = m_pReviewDlg->GetTEMPUnitOpt();
	int nChannel = m_pReviewDlg->m_cmbSelectChannel.GetCurSel();

	//胎心率表格
	int x = rc.left + scale_w;
	int y = rc.top - table_top;
	int w = table_w;
	int h = f20_table_h;
	pdc->SetTextColor(cr_scale);
	for (i=0; i<=w; i+= grid_w) {
		if (0 == i % (grid_w * 6)) {
			pdc->SelectObject(&penRed);
		}
		else {
			pdc->SelectObject(&penLight);
		}
		pdc->MoveTo(x + i, y);
		pdc->LineTo(x + i, y - h);
	}
	for (i=0; i<=h; i+= f20_grid_h) {
		if (0 == i % (f20_grid_h * 3)) {
			CString s;
			s.Format(_T("%3d"), (int)(fhr_max - i / f20_scale));
			pdc->TextOut(x - scale_w, y - i + half_scale_h, s);
			pdc->SelectObject(&penDark);
		}
		else {
			pdc->SelectObject(&penLight);
		}
		pdc->MoveTo(x, y - i);
		pdc->LineTo(x + table_w, y - i);
	}
	pdc->SetTextColor(cr_info);
	pdc->TextOut(x + 20, y - half_scale_h, (0 == nChannel) ? _T("FHR 1") : _T("FHR 2"));

	//宫缩压表格
	y = y - h - time_f20_h;
	h = t20_table_h;
	pdc->SetTextColor(cr_scale);
	for (i=0; i<=w; i+= grid_w) {
		if (0 == i % (grid_w * 6)) {
			pdc->SelectObject(&penRed);
		}
		else {
			pdc->SelectObject(&penLight);
		}
		pdc->MoveTo(x + i, y);
		pdc->LineTo(x + i, y - h);
	}
	for (i=0; i<=h; i+= t20_grid_h) {
		if (0 == i % (t20_grid_h * 2)) {
			CString s = itoNibpString(nNibpUnit, (int)(toco_max - i / t20_scale));
			pdc->TextOut(x - scale_w, y - i + half_scale_h, s);
			pdc->SelectObject(&penDark);
		}
		else {
			pdc->SelectObject(&penLight);
		}
		pdc->MoveTo(x, y - i);
		pdc->LineTo(x + table_w, y - i);
	}

	//胎心率表格
	y = y - h - separator_h;
	h = f20_table_h;
	pdc->SetTextColor(cr_scale);
	for (i=0; i<=w; i+= grid_w) {
		if (0 == i % (grid_w * 6)) {
			pdc->SelectObject(&penRed);
		}
		else {
			pdc->SelectObject(&penLight);
		}
		pdc->MoveTo(x + i, y);
		pdc->LineTo(x + i, y - h);
	}
	for (i=0; i<=h; i+= f20_grid_h) {
		if (0 == i % (f20_grid_h * 3)) {
			CString s;
			s.Format(_T("%3d"), (int)(fhr_max - i / f20_scale));
			pdc->TextOut(x - scale_w, y - i + half_scale_h, s);
			pdc->SelectObject(&penDark);
		}
		else {
			pdc->SelectObject(&penLight);
		}
		pdc->MoveTo(x, y - i);
		pdc->LineTo(x + table_w, y - i);
	}
	pdc->SetTextColor(cr_info);
	pdc->TextOut(x + 20, y - half_scale_h, (0 == nChannel) ? _T("FHR 1") : _T("FHR 2"));

	//宫缩压表格
	y = y - h - time_f20_h;
	h = t20_table_h;
	pdc->SetTextColor(cr_scale);
	for (i=0; i<=w; i+= grid_w) {
		if (0 == i % (grid_w * 6)) {
			pdc->SelectObject(&penRed);
		}
		else {
			pdc->SelectObject(&penLight);
		}
		pdc->MoveTo(x + i, y);
		pdc->LineTo(x + i, y - h);
	}
	for (i=0; i<=h; i+= t20_grid_h) {
		if (0 == i % (t20_grid_h * 2)) {
			CString s = itoNibpString(nNibpUnit, (int)(toco_max - i / t20_scale));
			pdc->TextOut(x - scale_w, y - i + half_scale_h, s);
			pdc->SelectObject(&penDark);
		}
		else {
			pdc->SelectObject(&penLight);
		}
		pdc->MoveTo(x, y - i);
		pdc->LineTo(x + table_w, y - i);
	}
}

void CPrintFHR::DrawTimeF10(CDC* pdc, CRect& rc)
{
	int nValidLen;
	CTime tmTableStart = m_pReviewDlg->GetCurrentDataTime(&nValidLen);
	if (tmTableStart == 0) {
		return;
	}

	CString sTime = tmTableStart.Format(_T("%Y-%m-%d %H:%M:%S"));
	int nDrawLen = min(nValidLen, DATA_PAPER_LEN-1); //10分钟
	pdc->SetTextColor(cr_info);

	int x = rc.left + scale_w;
	int y = rc.top - table_top - f10_table_h;
	DrawTimeRuler(pdc, x, y, tmTableStart, time_f10_h, nDrawLen);
}

void CPrintFHR::DrawTimeF20(CDC* pdc, CRect& rc)
{
	int nValidLen;
	CTime tmTableStart = m_pReviewDlg->GetCurrentDataTime(&nValidLen);
	if (tmTableStart == 0) {
		return;
	}

	CString sTime = tmTableStart.Format(_T("%Y-%m-%d %H:%M:%S"));
	int nDrawLen = min(nValidLen, DATA_PAPER_LEN-1); //10分钟
	pdc->SetTextColor(cr_info);

	int x = rc.left + scale_w;
	int y = rc.top - table_top - f20_table_h;
	DrawTimeRuler(pdc, x, y, tmTableStart, time_f20_h, nDrawLen);

	nDrawLen = min(nValidLen - DATA_PAPER_LEN, DATA_PAPER_LEN-1);
	CTimeSpan ts(0, 0, 0, DATA_PAPER_LEN);
	tmTableStart += ts; //增加10分钟
	y = y - time_f20_h - t20_table_h - separator_h - f20_table_h;
	DrawTimeRuler(pdc, x, y, tmTableStart, time_f20_h, nDrawLen);
}

void CPrintFHR::DrawWaveF10(CDC* pdc, CRect& rc)
{
	int nValidLen;
	CLIENT_DATA* pData = m_pReviewDlg->GetCurrentBufferPointer(&nValidLen);
	if (! pData) {
		return;
	}

	CPen penBlack(PS_SOLID, 3, cr_wave);
	CPen* pOldPen = pdc->SelectObject(&penBlack);

	//用户选择的通道
	int nChannel = m_pReviewDlg->m_cmbSelectChannel.GetCurSel();

	int nDispCount = min(nValidLen, DATA_PAPER_LEN);//图纸长度为600秒波形
	//绘制时注意，1秒钟2个数据点
	int i;
	BYTE fhr[3];
	BYTE toco[3];
	BYTE fm_mark;
	int x = rc.left + scale_w;
	int fhr_y = rc.top - table_top - f10_table_h;
	int toco_y = fhr_y - time_f10_h - t10_table_h;
	int mark_y = fhr_y - time_f10_h;
	for (i=0; i<nDispCount; i++) {
		ECG* pe;
		RESP* pr;
		SPO2* ps;
		NIBP* pn;
		TEMP* pt;
		FETUS* pf;
		CStructTools::ParseClientData(&(pData[i]), &pe, &pr, &ps, &pn, &pt, &pf);

		switch (nChannel) {
		case 0:
			if (0 == i) {
				fhr[0] = pf->fhr1[1];
			}
			else {
				fhr[0] = fhr[2];
			}
			fhr[1] = pf->fhr1[1];
			fhr[2] = pf->fhr1[3];
			break;
		case 1:
			if (0 == i) {
				fhr[0] = pf->fhr2[1];
			}
			else {
				fhr[0] = fhr[2];
			}
			fhr[1] = pf->fhr2[1];
			fhr[2] = pf->fhr2[3];
			break;
		}
		if (0 == i) {
			toco[0] = pf->uc[1];
		}
		else {
			toco[0] = toco[2];
		}
		toco[1] = pf->uc[1];
		toco[2] = pf->uc[3];
		fm_mark = pf->marked;

		//绘制FHR曲线
		int x0 = x + i * time_scale;
		int x1 = x0 + (time_scale / 2);
		int x2 = x0 + time_scale;
		if (0 != fhr[0] && 0 != fhr[1]) {
			pdc->MoveTo(x0, fhr_y + (fhr[0] - 30) * f10_scale);
			pdc->LineTo(x0, fhr_y + (fhr[1] - 30) * f10_scale);
			pdc->LineTo(x1, fhr_y + (fhr[1] - 30) * f10_scale);
		}
		if (0 != fhr[1] && 0 != fhr[2]) {
			pdc->MoveTo(x1, fhr_y + (fhr[1] - 30) * f10_scale);
			pdc->LineTo(x1, fhr_y + (fhr[2] - 30) * f10_scale);
			pdc->LineTo(x2, fhr_y + (fhr[2] - 30) * f10_scale);
		}
		//绘制TOCO曲线
		pdc->MoveTo(x0, toco_y + toco[0] * f10_scale);
		pdc->LineTo(x0, toco_y + toco[1] * f10_scale);
		pdc->LineTo(x1, toco_y + toco[1] * f10_scale);
		pdc->LineTo(x1, toco_y + toco[2] * f10_scale);
		pdc->LineTo(x2, toco_y + toco[2] * f10_scale);

		//绘制胎动标记
		if (fm_mark) {
			m_nMoveCount ++;
			DrawFMMark(pdc, cr_mark, x1, mark_y, f10_mark_h);
		}

		//绘制自动诊断信息
		if (m_pAuto->Success()) {
			DIAGNOSIS_FLAG* pf = m_pAuto->FindFlag(i*2, 2);
			if (pf) {
				int flag_y = fhr_y + (pf->nY - 30) * f10_scale;
				DrawDiagnosisFlag(pdc, x0, flag_y, pf->nType);
			}
		}
	}

	pdc->SelectObject(pOldPen);
}

void CPrintFHR::DrawWaveF20(CDC* pdc, CRect& rc)
{
	int nValidLen;
	CLIENT_DATA* pData = m_pReviewDlg->GetCurrentBufferPointer(&nValidLen);
	if (! pData) {
		return;
	}

	CPen penBlack(PS_SOLID, 3, cr_wave);
	CPen* pOldPen = pdc->SelectObject(&penBlack);

	//用户选择的通道
	int nChannel = m_pReviewDlg->m_cmbSelectChannel.GetCurSel();

	int nDispCount = min(nValidLen, DATA_PAPER_LEN);//图纸长度为600秒波形
	//绘制时注意，1秒钟2个数据点
	int i;
	BYTE fhr[3];
	BYTE toco[3];
	BYTE fm_mark;
	int x = rc.left + scale_w;
	int fhr_y  = rc.top - table_top - f20_table_h;
	int toco_y = fhr_y - time_f20_h - t20_table_h;
	int mark_y = fhr_y - time_f20_h;
	for (i=0; i<nDispCount; i++) {
		ECG* pe;
		RESP* pr;
		SPO2* ps;
		NIBP* pn;
		TEMP* pt;
		FETUS* pf;
		CStructTools::ParseClientData(&(pData[i]), &pe, &pr, &ps, &pn, &pt, &pf);

		switch (nChannel) {
		case 0:
			if (0 == i) {
				fhr[0] = pf->fhr1[1];
			}
			else {
				fhr[0] = fhr[2];
			}
			fhr[1] = pf->fhr1[1];
			fhr[2] = pf->fhr1[3];
			break;
		case 1:
			if (0 == i) {
				fhr[0] = pf->fhr2[1];
			}
			else {
				fhr[0] = fhr[2];
			}
			fhr[1] = pf->fhr2[1];
			fhr[2] = pf->fhr2[3];
			break;
		}
		if (0 == i) {
			toco[0] = pf->uc[1];
		}
		else {
			toco[0] = toco[2];
		}
		toco[1] = pf->uc[1];
		toco[2] = pf->uc[3];
		fm_mark = pf->marked;

		//绘制FHR曲线
		int x0 = x + i * time_scale;
		int x1 = x0 + (time_scale / 2);
		int x2 = x0 + time_scale;

		if (0 != fhr[0] && 0 != fhr[1]) {
			pdc->MoveTo(x0, fhr_y + (fhr[0] - 30) * f20_scale);
			pdc->LineTo(x0, fhr_y + (fhr[1] - 30) * f20_scale);
			pdc->LineTo(x1, fhr_y + (fhr[1] - 30) * f20_scale);
		}
		if (0 != fhr[1] && 0 != fhr[2]) {
			pdc->MoveTo(x1, fhr_y + (fhr[1] - 30) * f20_scale);
			pdc->LineTo(x1, fhr_y + (fhr[2] - 30) * f20_scale);
			pdc->LineTo(x2, fhr_y + (fhr[2] - 30) * f20_scale);
		}

		//绘制TOCO曲线
		pdc->MoveTo(x0, toco_y + toco[0] * f20_scale);
		pdc->LineTo(x0, toco_y + toco[1] * f20_scale);
		pdc->LineTo(x1, toco_y + toco[1] * f20_scale);
		pdc->LineTo(x1, toco_y + toco[2] * f20_scale);
		pdc->LineTo(x2, toco_y + toco[2] * f20_scale);

		//绘制胎动标记
		if (fm_mark) {
			DrawFMMark(pdc, cr_mark, x1, mark_y, f20_mark_h);
		}

		//绘制自动诊断信息
		if (m_pAuto->Success()) {
			DIAGNOSIS_FLAG* pf = m_pAuto->FindFlag(i*2, 2);
			if (pf) {
				int flag_y = fhr_y + (pf->nY - 30) * f20_scale;
				DrawDiagnosisFlag(pdc, x0, flag_y, pf->nType);
			}
		}
	}

	nDispCount = min(nValidLen - DATA_PAPER_LEN, DATA_PAPER_LEN);//图纸长度为600秒波形
	fhr_y  = rc.top - table_top - f20_table_h - time_f20_h - t20_table_h - separator_h - f20_table_h;
	toco_y = fhr_y - time_f20_h - t20_table_h;
	mark_y = fhr_y - time_f20_h;
	for (i=0; i<nDispCount; i++) {
		ECG* pe;
		RESP* pr;
		SPO2* ps;
		NIBP* pn;
		TEMP* pt;
		FETUS* pf;
		CStructTools::ParseClientData(&(pData[DATA_PAPER_LEN+i]), &pe, &pr, &ps, &pn, &pt, &pf);

		switch (nChannel) {
		case 0:
			fhr[0] = fhr[2];
			fhr[1] = pf->fhr1[1];
			fhr[2] = pf->fhr1[3];
			break;
		case 1:
			fhr[0] = fhr[2];
			fhr[1] = pf->fhr2[1];
			fhr[2] = pf->fhr2[3];
			break;
		}
		toco[0] = toco[2];
		toco[1] = pf->uc[1];
		toco[2] = pf->uc[3];
		fm_mark = pf->marked;

		//绘制FHR曲线
		int x0 = x + i * time_scale;
		int x1 = x0 + (time_scale / 2);
		int x2 = x0 + time_scale;

		if (0 != fhr[0] && 0 != fhr[1]) {
			pdc->MoveTo(x0, fhr_y + (fhr[0] - 30) * f20_scale);
			pdc->LineTo(x0, fhr_y + (fhr[1] - 30) * f20_scale);
			pdc->LineTo(x1, fhr_y + (fhr[1] - 30) * f20_scale);
		}
		if (0 != fhr[1] && 0 != fhr[2]) {
			pdc->MoveTo(x1, fhr_y + (fhr[1] - 30) * f20_scale);
			pdc->LineTo(x1, fhr_y + (fhr[2] - 30) * f20_scale);
			pdc->LineTo(x2, fhr_y + (fhr[2] - 30) * f20_scale);
		}

		//绘制TOCO曲线
		pdc->MoveTo(x0, toco_y + toco[0] * f20_scale);
		pdc->LineTo(x0, toco_y + toco[1] * f20_scale);
		pdc->LineTo(x1, toco_y + toco[1] * f20_scale);
		pdc->LineTo(x1, toco_y + toco[2] * f20_scale);
		pdc->LineTo(x2, toco_y + toco[2] * f20_scale);

		//绘制胎动标记
		if (fm_mark) {
			DrawFMMark(pdc, cr_mark, x1, mark_y, f20_mark_h);
		}

		//绘制自动诊断信息
		if (m_pAuto->Success()) {
			DIAGNOSIS_FLAG* pf = m_pAuto->FindFlag(i*2, 2);
			if (pf) {
				int flag_y = fhr_y + (pf->nY - 30) * f20_scale;
				DrawDiagnosisFlag(pdc, x0, flag_y, pf->nType);
			}
		}
	}

	pdc->SelectObject(pOldPen);
}

void CPrintFHR::DrawInfo(CDC* pdc, CRect& rc)
{
	CString sPatientInfo;
	CString sWaveformInfo;

	CString sBedNum;
	CString sMedNum;
	CString sPatName;
	CString sPregnantWeek;
	CString sPatAge;

	sBedNum.Format(_T("床位：%02s"), m_pReviewDlg->m_sBedNum.IsEmpty() ? _T("----") : m_pReviewDlg->m_sBedNum);
	sMedNum.Format(_T("病历号：%s"), m_pReviewDlg->m_sMedicalNum.IsEmpty() ? _T("----") : m_pReviewDlg->m_sMedicalNum);
	sPatName.Format(_T("姓名：%s"), m_pReviewDlg->m_sPatientName.IsEmpty() ? _T("----") : m_pReviewDlg->m_sPatientName);
	sPregnantWeek.Format(_T("孕周：%s"), m_pReviewDlg->m_sPregnantWeek .IsEmpty() ? _T("----") : m_pReviewDlg->m_sPregnantWeek);
	sPatAge.Format(_T("年龄：%s"), m_pReviewDlg->m_sPatientType.IsEmpty() ? _T("--") : m_pReviewDlg->m_sPatientAge);

	sPatientInfo.Format(_T("%5s%20s%40s%20s%20s"), sBedNum, sMedNum, sPatName, sPregnantWeek, sPatAge);
	pdc->SetTextColor(cr_info);
	pdc->TextOut(rc.left, rc.top + 36, sPatientInfo);

	CString sStartTime;
	CString sWaveformLen;
	CString sPrintTime;
	int nValidLen;
	CTime tmTableStart = m_pReviewDlg->GetCurrentDataTime(&nValidLen);
	sStartTime = tmTableStart.Format(_T("开始时间：%Y-%m-%d %H:%M:%S"));
	int nWaveformLen = min(60, nValidLen);
	sWaveformLen.Format(_T("波形长度：%d秒"), nWaveformLen);
	CTime tmNow = CTime::GetCurrentTime();
	sPrintTime = tmNow.Format(_T("打印时间：%Y-%m-%d %H:%M:%S"));

	sWaveformInfo.Format(_T("%20s%30s%55s"), sStartTime, sWaveformLen, sPrintTime);
	pdc->TextOut(rc.left, rc.bottom - 6, sWaveformInfo);
	
	if (! theApp.m_sHospitalName.IsEmpty()) {
		pdc->TextOut(rc.left, rc.bottom - 44, theApp.m_sHospitalName);
	}
}

void CPrintFHR::DrawReport(CDC* pdc, CRect& rc)
{
	int i;
	CString sBaseline     = _T("");
	CString sBaselineMark = _T("");
	CString sLTV_AB_Mark  = _T("");
	CString sLTV_T_Mark   = _T("");
	CString sAccMark      = _T("");
	CString sDecMark      = _T("");
	CString sFischerMark  = _T("");
	CString sAccCount     = _T("");
	CString sFM           = _T("");
	CString s_wuciji_test = _T("");
	CString s_gongsuo_test = _T("");

	switch (m_nDoctorDiagno) {
	case 1: s_wuciji_test = _T("反应型");   break;
	case 2: s_wuciji_test = _T("无反应型"); break;
	case 3: s_wuciji_test = _T("混合型");   break;
	case 4: s_gongsuo_test = _T("阴性");    break;
	case 5: s_gongsuo_test = _T("阳性");    break;
	case 6: s_gongsuo_test = _T("可疑");    break;
	case 7: s_gongsuo_test = _T("不成功");  break;
	}

	//胎动次数
	sFM.Format(_T("  %d次"), m_nMoveCount);

	//获得自动诊断数据
	if (m_pAuto->Success()) {
		int nBaseline = m_pAuto->GetFhrBaseline();
		if (0 == nBaseline) {
			sBaseline = _T("  ----");
		}
		else {
			sBaseline.Format(_T("  %d(bpm)"), nBaseline);
		}
		sBaselineMark.Format(_T("%d分"), m_pAuto->GetBaselineMark());
		sLTV_AB_Mark.Format(_T("%d分"), m_pAuto->GetLTV_AB_Mark());
		sLTV_T_Mark.Format(_T("%d分"), m_pAuto->GetLTV_T_Mark());
		sAccMark.Format(_T("%d分"), m_pAuto->GetAccMark());
		sDecMark.Format(_T("%d分"), m_pAuto->GetDecMark());
		sFischerMark.Format(_T("%d分"), m_pAuto->GetFischerMark());
		sAccCount.Format(_T("  %d次"), m_pAuto->GetAccCount());
	}

	//绘制方框
	int x = rc.left;
	int y = rc.top - table_h - 10;
	int x1 = x;
	int x2 = x + 90;
	int x3 = x2 + 300;
	int x4 = x2 + 300 * 2;
	int x5 = x2 + 300 * 3;
	int x6 = x2 + 300 * 4;
	int x7 = x6 + 360;
	int x8 = x7 + 200;
	int x9 = x7 + 200 * 2;
	int x10 = x7 + 200 * 3;
	int y1 = y;
	int y2 = y - 36;
	int y3 = y - 36 * 2;
	int y4 = y - 36 * 3;
	int y5 = y - 36 * 4;
	int y6 = y - 36 * 5 - 20;
	int y7 = y - 36 * 6 - 20;
	CBrush br(cr_grid_light);
	CRect rc1(x2, y5, x2 + 200, y5 - 40);
	for (i=0; i<5; i++) {
		pdc->FillRect(&rc1, &br);
		rc1.OffsetRect(300, 0);
	}
	CRect rc2(x8, y1, x8 + 200, y1 - 40);
	for (i=0; i<3; i++) {
		pdc->FillRect(&rc2, &br);
		rc2.OffsetRect(0, -36 * 2);
	}
	CRect rc3(x10, y1, x10 + 200, y1 - 40);
	for (i=0; i<3; i++) {
		pdc->FillRect(&rc3, &br);
		rc3.OffsetRect(0, -36 * 2);
	}

	SelectInfoFont(pdc);
	pdc->SetTextColor(cr_info);
	pdc->TextOut(x1, y1, _T(""));
	pdc->TextOut(x2, y1, _T("胎心率基线(bpm)"));
	pdc->TextOut(x3, y1, _T("振幅变异(bpm)"));
	pdc->TextOut(x4, y1, _T("周期变异(cpm)"));
	pdc->TextOut(x5, y1, _T("加速"));
	pdc->TextOut(x6, y1, _T("减速"));
	pdc->TextOut(x7, y1, _T("Fischer评分"));
	pdc->TextOut(x8, y1, sFischerMark);
	pdc->TextOut(x9, y1, _T("胎心率基线"));
	pdc->TextOut(x10, y1, sBaseline);

	pdc->TextOut(x1, y2, _T("0分"));
	pdc->TextOut(x2, y2, _T("<100, >180"));
	pdc->TextOut(x3, y2, _T("<3"));
	pdc->TextOut(x4, y2, _T("<2"));
	pdc->TextOut(x5, y2, _T("无"));
	pdc->TextOut(x6, y2, _T("LD、PD、重VD"));
	pdc->TextOut(x7, y2, _T(""));
	pdc->TextOut(x8, y2, _T(""));
	pdc->TextOut(x9, y2, _T(""));
	pdc->TextOut(x10, y2, _T(""));

	pdc->TextOut(x1, y3, _T("1分"));
	pdc->TextOut(x2, y3, _T("100~120,160~180"));
	pdc->TextOut(x3, y3, _T("3~5，>25"));
	pdc->TextOut(x4, y3, _T("2~5"));
	pdc->TextOut(x5, y3, _T("1~2，小加速≥3"));
	pdc->TextOut(x6, y3, _T("轻度VD≥3或ED≥3"));
	pdc->TextOut(x7, y3, _T("无刺激 试验"));
	pdc->TextOut(x8, y3, s_wuciji_test);
	pdc->TextOut(x9, y3, _T("胎动次数"));
	pdc->TextOut(x10, y3, sFM);

	pdc->TextOut(x1, y4, _T("2分"));
	pdc->TextOut(x2, y4, _T("120~160"));
	pdc->TextOut(x3, y4, _T("6~25"));
	pdc->TextOut(x4, y4, _T(">5"));
	pdc->TextOut(x5, y4, _T(">2"));
	pdc->TextOut(x6, y4, _T("无"));
	pdc->TextOut(x7, y4, _T(""));
	pdc->TextOut(x8, y4, _T(""));
	pdc->TextOut(x9, y4, _T(""));
	pdc->TextOut(x10, y4, _T(""));
	
	pdc->TextOut(x1, y5, _T(""));
	pdc->TextOut(x2, y5, sBaselineMark);
	pdc->TextOut(x3, y5, sLTV_AB_Mark);
	pdc->TextOut(x4, y5, sLTV_T_Mark);
	pdc->TextOut(x5, y5, sAccMark);
	pdc->TextOut(x6, y5, sDecMark);
	pdc->TextOut(x7, y5, _T("宫 缩  试验"));
	pdc->TextOut(x8, y5, s_gongsuo_test);
	pdc->TextOut(x9, y5, _T("加速次数"));
	pdc->TextOut(x10, y5, sAccCount);

	pdc->TextOut(x1, y6, _T("说明：1、电脑分析结果仅供参考，尚需医生根据多种临床因素分析而定。"));
	pdc->TextOut(x1, y7, _T("      2、胎儿状况具有明显动态变化，应按时复诊或孕妇自感异常随时复诊。"));
	SelectDoctorSignFont(pdc);
	pdc->TextOut(x + 1500, y6, _T("医生签名："));
	ResetFont(pdc);
}

void CPrintFHR::DrawTimeRuler(CDC* pdc, int x, int y, CTime tm, int h, int nSeconds)
{
	int j;
	int count = nSeconds / 60; //两个时间标之间间距1分钟
	
	for (j = 0; j <= count; j ++) {
		pdc->MoveTo(x, y - 4);
		pdc->LineTo(x, y - h + 4);

		CTimeSpan ts(0, 0, 0, j * 60);
		CTime tmMark = tm + ts;
		pdc->TextOut(x + 2, y - (h - text_scale_h) / 2, tmMark.Format(_T("%H:%M:%S")));
		x += (int)(60 * time_scale);
	}
}

void CPrintFHR::DrawFMMark(CDC* pdc, COLORREF cr, int x, int y, int h)
{
	const int nMarkWidth = 5;
	const int nArrawSize = 24;
	int i;
	CPen penBlack(PS_SOLID, 2, cr);
	CPen* pOldPen = pdc->SelectObject(&penBlack);
	for (i = 0; i < nArrawSize; i ++) {
		pdc->MoveTo(x + nMarkWidth - i / 2, y - i - 1);
		pdc->LineTo(x + nMarkWidth, y - i - 1);
	}
	pdc->SelectObject(pOldPen);
	CBrush br(cr);
	CRect rc(x, y - nArrawSize - 1, x + nMarkWidth + 1, y - h);
	pdc->FillRect(&rc, &br);
}

void CPrintFHR::Autodiagnostics(void)
{
	int i;
	int nValidLen;
	CLIENT_DATA* pData = m_pReviewDlg->GetCurrentBufferPointer(&nValidLen);
	if (! pData) {
		return;
	}

	//用户选择的通道
	int nChannel = m_pReviewDlg->m_cmbSelectChannel.GetCurSel();
	int nProcCount = min(nValidLen, (0 == m_type) ? DATA_PAPER_LEN : DATA_PAPER_LEN*2);//自动诊断处理最长10/20分钟的数据

	BYTE fhr_buf[2400];
	BYTE toco_buf[2400];
	for (i=0; i<nProcCount; i++) {
		ECG* pe;
		RESP* pr;
		SPO2* ps;
		NIBP* pn;
		TEMP* pt;
		FETUS* pf;
		CStructTools::ParseClientData(&(pData[i]), &pe, &pr, &ps, &pn, &pt, &pf);

		int nCurIndex = i * 2;
		switch (nChannel) {
		case 0:
			fhr_buf[nCurIndex] = pf->fhr1[1];
			fhr_buf[nCurIndex+1] = pf->fhr1[3];
			break;
		case 1:
			fhr_buf[nCurIndex] = pf->fhr2[1];
			fhr_buf[nCurIndex+1] = pf->fhr2[3];
			break;
		}
		toco_buf[nCurIndex] = pf->uc[1];
		toco_buf[nCurIndex+1] = pf->uc[3];
	}

	m_pAuto->SetBuffer(fhr_buf, toco_buf, nProcCount * 2);
	m_pAuto->StartAutodiagno();
}

void CPrintFHR::DrawDiagnosisFlag(CDC* pdc, int x, int y, int type)
{
	if (0 == type) {
		return;
	}
	pdc->SetTextColor(cr_info);
	if (type > 0) {
		pdc->TextOut(x, y + text_scale_h * 1.5, CFMDict::FHRAutodiagnoAccel_itos(type));
	}
	else {
		pdc->TextOut(x, y - text_scale_h * 0.5, CFMDict::FHRAutodiagnoDecel_itos(-type));
	}
}
