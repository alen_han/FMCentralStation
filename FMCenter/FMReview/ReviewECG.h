#pragma once
#include "reviewdlg.h"

class CReviewECG : public CReviewTool
{
private:
	CFont m_fontEcgInfo;
	CFont* m_pOldFont;

public:
	CReviewECG(CReviewDlg* pdlg);
	virtual ~CReviewECG(void);

private:
	virtual int  GetPageSpan();
	virtual void EnterReview();
	virtual void LeaveReview();
	virtual void Draw(CDC* pdc);

private:
	void SelectEcgInfoFont(CDC* pdc);
	void ResetFont(CDC* pdc);
	void DrawGrid100(CDC* pdc, CRect& rc);
	void DrawGrid50(CDC* pdc, CRect& rc);
	void DrawTime(CDC* pdc, CRect& rc);
	void DrawWave(CDC* pdc, CRect& rc);
	void DrawLeadInfo(CDC* pdc, int x, int y, int h, int lead);
};

